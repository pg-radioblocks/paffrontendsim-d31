# PAF Frontend Simulator

Frontend simulator of a PAF (Phased Array Feed) as part of the Radioblocks project.

This project is divided into two parts:
1. PAFFrontendSim.py, which is a reference implementation of a script to simulates a PAF receiver
2. Libs folder containing individual libraries for the PAF simulator


The PAF Frontend Simulator is able to interface with the [PAF Backend Simulator](https://gitlab.mpcdf.mpg.de/nesser/pafsim), a closely related project.

## Libraries
- Simulation:
    A collection of modules to simulate radio-astronomical receivers from horn antenna output to sky performance. This includes a physical and geometrical raytracer and a complete noise model.
- FileIO:
    A rudimentary library for file access (writing and reading). This is pretty much in build up and will be a collection of read-write functions for a generic ascii-data file creation.

## Installation
Make sure a Python 3 interpreter is installed. The standard [CPython interpreter](https://www.python.org/downloads/) (`python3`) is recommended.

The Python libraries `numpy`, `scipy` and `matplotlib` are required.
On most platforms these packages are installed via `pip`.

An installation of `gcc` with the standard C-libraries is also required.

Currently `matplotlib` expects `Tk` to be installed, though this might change in the future.

`git` is not required though highly recommended to aid collaboration.
You will need a local copy of this git repository.

It can be obtained using the command `git clone https://gitlab.mpcdf.mpg.de/pg-radioblocks/paffrontendsim-d31` or alternatively using the Download button in the GitLab interface.

This simulator has only been tested on Linux, though all required software is also available on other platforms.

You may use any recent versions of the software packages.
However please make sure that your code is backwards compatible with the following releases:

    python = 3.9
    numpy = 1.21
    scipy = 1.8
    matplotlib = 3.5
    Tk = 8.6

CSTDataClass.py optionally uses the official CST Python library in order to access CST project files.
If you wish to use this feature then be aware that it only supports specific versions of Python.
At the time of writing, the 2023 CST Python library only runs when using Python 3.6 through 3.9.

## Usage
Executing PAFFrontendSim.py using Python 3 will start a simulation with default parameters.

Running a simulation can take a few seconds to a few days, depending on the testing routine, parameters and hardware.

The results are stored in a dedicated folder. The default is `paffrontendsim-d31/simData/OutputFolder/defaultOutput/`.
If a new simulation starts and its output folder already contains data, said folder is automatically cleared to make space for new data. Beware of data loss.

Various parameters can be passed to PAFFrontendSim.py like the type of telescope, the frequency range, the size of the PAF and many more.
The following command is an example of its usage:

```bash
$ ./src/Apps/PAFFrontendSim/PAFFrontendSim.py --TelescopeName MRO --SourcePol 45 --TestRFI

```

## Documentation
Code documentation is based on docstrings in the source code.
It is automatically generated using Sphinx and can be found [here](https://gitlab.mpcdf.mpg.de/pg-radioblocks/paffrontendsim-d31/-/jobs/artifacts/main/file/public/index.html?job=pages).

## Contributing
Please refrain from committing directly to the main branch.

If you wish to make changes, please create a branch, make changes there and then request to have your changes merged back into the main branch.

Please include docstrings in your code in the following (Sphinx-parsable) format:

```python
def someFunction(self, someObject, someVariable = True):
    """
    A one-sentence description of someFunction.

    Optionally a lengthier description explaining more detail.

    Parameters:
        someObject (someClass): a brief description of what this is and maybe how it is used
        someVariable (boolean): another brief description of what this is

    Returns:
        anotherVariable (float): a brief description of what this is and what it means
    """
```

## License

This project is licensed under the terms of the GNU General Public License v3.0.
See the [LICENSE](./LICENSE) file for details.

## Contact

[Yanik Yiannakis](mailto:yiannakis@mpifr-bonn.mpg.de) (Max-Planck-Institute for Radio Astronomy)

## Further Reading

The following links provide context:

 - [Phased Arrays](https://www.cambridge.org/9781108423922) book by Karl F. Warnick
 - [Python](https://docs.python.org) documentation
 - [Numpy and Scipy](https://docs.scipy.org/doc) documentation
 - [Matplotlib](http://matplotlib.org/contents.html) documentation
 - [Git](https://git-scm.com/docs) documentation
