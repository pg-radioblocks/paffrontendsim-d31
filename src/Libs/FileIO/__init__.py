#!/bin/python3
"""
This file is part of the PAFFrontendSim.

The PAFFrontendSim is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License v3 as published by the Free Software Foundation.

The PAFFrontendSim is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with the PAFFrontendSim. If not, see https://www.gnu.org/licenses/.
"""
# Package providing various File-IO rutines
#
"""
# TODO

"""
#############################################################################
# import internal libs
#############################################################################

#############################################################################
# common variables
#############################################################################

#############################################################################
# shortcut to main classes
#############################################################################
from . import constants 
from . import FileDataClass


