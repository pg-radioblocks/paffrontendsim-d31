#!!/usr/bin/env python3
"""
This file is part of the PAFFrontendSim.

The PAFFrontendSim is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License v3 as published by the Free Software Foundation.

The PAFFrontendSim is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with the PAFFrontendSim. If not, see https://www.gnu.org/licenses/.
"""
#############################################################################
# import libs
#############################################################################

#############################################################################
# constants
#############################################################################
_CLASSNAME       = "Constants"

# default values 
_COMMENT         = '#'                             # comment string as used in internal files
_HEADERINDICATOR = _COMMENT * 80                   # for readability: start of the header
_KEYINDICATOR    = ':'

#############################################################################
# common header profiles
#############################################################################
#
#
# all keys starting with '_' are used internally only they are not parsed!
# Return data types are derived from parsing the list of parameters for a given key-word. Data need to be seperated by white-space
#
# pre-defined internal keys and their function (with example):
# _NAME                    :   'UFO Data file header',      # special entry, name of the header profile
# _COMMENT                 :   '#',                         # special line, indicating comment sign
# _KEYINDICATOR            :   ':',                         # special line, indicating the character for ending keys 
# _HEADERINDICATOR         :   '#' * 80,                    # special line, indicating header start / stop-line
# _EMPTY                   :   2,                           # special line, telling how many empty lines are added between HEADERINDICATOR and first Key-word
# _UPPERCASE               :   True,                        # special line, telling to use all keywords in uppercase only (True)
# _ADDUNKNOWN              :   False,                       # special line, telling to add unknown keywords to dictionary (and therewith write them also)
# _DELMITTER               :   " ",                         # special line, indicating string used as delmitter inside the data array
# _ACCURACY                :   12,                          # special line, indicating how many float pointing numbers are printed (int)
# _LENGTH                  :   15,                          # special line, indicating how characters per entry are printed in minimum (int)
# _SCIENTIFIC              :   '',                          # special line, indicating if present that floats are wiriten in scientific notation in the data section
# _ReadFunction            :   readUFO,                     # special line, containing the translation function from column-list to data-line entry
# _WriteFunction           :   writeUFO,                    # special line, containing the translation function from data-line entry to column-list
#
#
# configuration dictionary consists of Keyword and list of parameters
# (
#  Mandatory [0/1] (unused so far),
#  String with return data types,
#  dictionary-Keyword (if empty, keyword is used), numberafter ':' of the keyword indicates field number sorting (first field goes to first index, second field to second index ....
#  comment-string
# )
# Return data types:
# f     : float number
# F     : convert all available line parts (white-space seperated) to floats
# d     : integer number
# D     : convert all available line parts (white-space seperated) to integer
# s     : string
# S     : return all available line parts (white-space seperated) as strings
# U     : unified string (one string covering the rest of the line up to the first comment sign after the key-word, can contain white spaces!)
#

_DefaulHeader = {
                    # config
                    "_NAME"                    :   'Default File Header profile',  # special entry, name of the header profile (str)
                    "_COMMENT"                 :   '#',                            # special line, indicating comment sign (char)
                    "_KEYINDICATOR"            :   ':',                            # special line, indicating the character for ending keys (char) 
                    "_HEADERINDICATOR"         :   '#' * 80,                       # special line, indicating header start / stop-line (str)
                    "_EMPTY"                   :   2,                              # special line, telling how many empty lines are added between HEADERINDICATOR and first Key-word (int)
                    "_UPPERCASE"               :   True,                           # special line, telling to use all keywords in uppercase only (True) (bool)
                    "_ADDUNKNOWN"              :   False,                          # special line, telling to add unknown keywords to dictionary (and therewith write them also) (bool)
                    "_DELMITTER"               :   " ",                            # special line, indicating string used as delmitter inside the data array (str)
                    "_ACCURACY"                :    5,                             # special line, indicating how many float pointing numbers are printed (int)
                    "_LENGTH"                  :   10,                             # special line, indicating how characters per entry are printed in minimum (int)
                    "_SCIENTIFIC"              :   '',                             # special line, indicating if present that floats are writen in scientific notation in the data section (char)
                    "_ReadFunction"            :   None,                           # special line, containing the translation function from column-list to data-line entry
                    "_WriteFunction"           :   None,                           # special line, containing the translation function from data-line entry to column-list
                     # internal configuration as example
                    "_CONFIG_TYPE"             :   'TEMPERATURES',                 # internal line with further confis-statements

                    # list of key-words
                    "_Comment1"                : "File description:",         # special comment line in written header
                    "Version"                  :   [1, 'U',    "", "File Version"],
                    "Operator"                 :   [0, 'U',    "", "Operator name / ID"],
                    "Date"                     :   [0, 'S',    "", "Date of data taking"],
                    "_Comment2"                : "Data descriptionn:",         # special comment line in written header
                    "Device"                   :   [1, 'U',    "", "Device name / ID"],
                    "DUT"                      :   [0, 'U',    "", "Device under test"]
                }
