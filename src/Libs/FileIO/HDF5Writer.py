#!/usr/bin/env python3
"""
This file is part of the PAFFrontendSim.

The PAFFrontendSim is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License v3 as published by the Free Software Foundation.

The PAFFrontendSim is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with the PAFFrontendSim. If not, see https://www.gnu.org/licenses/.
"""
import numpy as np
import h5py    as h5
import subprocess
from pathlib import Path
import matplotlib.pyplot as plt
from copy  import deepcopy
import datetime

import LogClass

_CLASSNAME   = "HDF5Writer"


_acmDescDict = {
                "allLoss"    : "telescope + atmosphere",
                "atmosphere" : "atmospheric noise",
                "horn"       : "antenna noise",
                "lna"        : "lna noise",
                "signal"     : "astronomical signal",
                "sky"        : "isotropic thermal noise of environment",
                "sys"        : "t + lna",
                "t"          : "sky + allLoss + horn",
                "telescope"  : "telescope spill-over noise",
                "rfi"        : "signal and RFI contributions"
                }


def plotACM(data, name = "", figname = "", show = False ) :
	"""
	Plot an ACM via matplotlib.

	Parameters:
        data (np.array):  ACM data as 2D numpy array.
		name (string):    Name of the ACM (shown as the title of the plot).
		figname (string): Filename of the figure.
		show (boolean):   Flag to show plot on screen even when being written to disk (True).
	"""
	fig, ax = plt.subplots()

	mat = np.absolute(data)
	mat = mat / np.max(mat)

	cf = ax.matshow(mat, cmap = plt.get_cmap('rainbow'), vmin = 0.0, vmax = 1.0)

	ax.set_xlabel("Element number")
	ax.set_ylabel("Element number")
	ax.set_title ("%s ACM (relative amplitude)" % name)
	fig.colorbar(cf)
	ax.set_aspect('equal')

	if figname != "" :
		fig.savefig(figname)
		if show :
			plt.show()
	else :
		plt.show()

	plt.close()


class HDF5Writer :
    """
    Module to write ACMs in a structured HDF5-file.
    The ACMs (array covariance matrices) are the most important data output of the simulator.
    Storing them in a file acts as an interface with the backend processor of a PAF system.
    """
    def __init__(self, outputFile, log = None):
        """
        Initialize HDF5-file, write some metadata.

        Parameters:
            outputFile (string): Path of the hdf5-file.
            log (LogClass):      Logging class to be used.
        """
        self.fileName = outputFile

        if log == None:
            self.log = LogClass.LogClass(_CLASSNAME)
        else:
            self.log = deepcopy(log)             # logging class used for all in and output messages
            self.log.addLabel(self)


    def setMetadata(self, antenna_matching, antenna_return_loss, element_ids, element_positions, element_polarisations, geometry, radius, spacing, elevation, name):
        """
        Sets metadata of the PAF in arrayGroup and of the telescope in telescopeGroup.
        The current git commit hash is also stored in the file to represent the software version.

        The metadata adds simulation parameters to the hdf5 file.
        This context is needed to make sense of the ACMs.

        Parameters:
            antenna_matching (float):       Impedance.
            antenna_return_loss (float):    Loss in front of the LNA.
            element_ids (list):             IDs of the array elements.
            element_positions (list):       XY positions of the array elements.
            element_polarisations (list):   Lists of the polarizations of the signal chains of each array element. Usually just one per element.
            geometry (string):              The geometry of the array (square, hexagonal, random, etc.).
            radius (float):                 Radius of the PAF.
            spacing (float):                Spacing between the antennas.
            elevation (float):              Elevation of the telescope relative to the ground.
            name (string:                   Name of the telescope.
        """
        try:
            result = subprocess.run(['git', 'rev-parse', 'HEAD'], stdout=subprocess.PIPE, stderr=subprocess.PIPE, text=True, check=True)
            commit_hash = result.stdout.strip()
        except subprocess.CalledProcessError as e:
            self.log.printOut(e, self.log.ERROR_TYPE)
            commit_hash = "could not determine commit"

        with h5.File(self.fileName, "w-") as file:
            file.attrs["commit"] = commit_hash
            file.attrs["creation_date_utc"] = datetime.datetime.utcnow().isoformat()

            arrayGroup = file.create_group("/array")
            arrayGroup["antenna_matching"] = antenna_matching
            arrayGroup["antenna_matching"].attrs["unit"] = "ohm"
            arrayGroup["antenna_return_loss"] = antenna_return_loss
            arrayGroup["antenna_return_loss"].attrs["unit"] = "ohm"
            arrayGroup["element_ids"] = element_ids
            arrayGroup["element_polarisations"] = element_polarisations
            arrayGroup["element_polarisations"].attrs["unit"] = "degree"
            arrayGroup["element_positions"] = element_positions
            arrayGroup["element_positions"].attrs["unit"] = "meters"
            arrayGroup["geometry"] = geometry
            arrayGroup["radius"] = radius
            arrayGroup["radius"].attrs["unit"] = "meters"
            arrayGroup["spacing"] = spacing
            arrayGroup["spacing"].attrs["unit"] = "meters"

            telescopeGroup = file.create_group("/telescope")
            telescopeGroup["elevation"] = elevation
            telescopeGroup["elevation"].attrs["unit"] = "degree"
            telescopeGroup["name"] = name


    def writeACM(self, acmKey, acmValue, frequency, coordinates = None, ppa = None):
        """
        Writes an ACM to the hdf5-file.

        Parameters:
            acmKey (string):     Name of the acm.
            acmValue (np.array): The acm as a numpy array.
            frequency (float):   Frequency at which this ACM was generated.
            coordinates (list):  Position on sky relative to boresight (0,0).
            ppa (float):         Source polarization.
        """
        with h5.File(self.fileName, "a") as file:
            acmID = 0
            if acmKey not in ("signal", "rfi"):
                h5targetPath = "/acms/noise/0/"+acmKey
            else:
                h5targetPath = "/acms/"+acmKey+"/0"

                while h5targetPath in file and (coordinates != file[h5targetPath]["coordinates"]).all():
                    acmID += 1
                    h5targetPath = "/acms/"+acmKey+"/"+str(acmID)

            if h5targetPath not in file:    # if the group doesn't exist yet in the file
                targetGroup = file.create_group(h5targetPath)
                targetGroup.attrs["name"] = acmKey
                targetGroup.attrs["description"] = _acmDescDict[acmKey]

                targetGroup.create_dataset("frequencies", shape = (0), maxshape=(100), dtype = frequency.dtype)
                targetGroup["frequencies"].attrs["unit"] = "GHz"

                noElements = len(acmValue[1])
                targetGroup.create_dataset("acm", shape = (0, noElements, noElements), maxshape = (100, noElements, noElements), dtype = acmValue.dtype)

                if acmKey in ("signal", "rfi"):
                    targetGroup["coordinates"] = coordinates
                    targetGroup["coordinates"].attrs["units"] = "degrees"

                    if acmKey == "signal":
                        targetGroup["ppa"] = ppa
                        targetGroup["ppa"].attrs["unit"] = "degree"

            else:
                targetGroup = file[h5targetPath]

            targetGroup["frequencies"].resize(targetGroup["frequencies"].shape[0]+1, axis=0)
            targetGroup["frequencies"][-1] = frequency # frequency of acm

            targetGroup["acm"].resize(targetGroup["acm"].shape[0]+1, axis=0)
            targetGroup["acm"][-1] = acmValue  # this is the acm data

        plotName = self.fileName.with_name(self.fileName.stem+"_"+str(frequency)+'GHz_ID_'+str(acmID)+"_"+acmKey+".png")
        plotACM(acmValue, h5targetPath, plotName)
