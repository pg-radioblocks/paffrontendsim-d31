#!!/usr/bin/env python3
"""
This file is part of the PAFFrontendSim.

The PAFFrontendSim is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License v3 as published by the Free Software Foundation.

The PAFFrontendSim is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with the PAFFrontendSim. If not, see https://www.gnu.org/licenses/.
"""
#############################################################################
# import libs
#############################################################################

#############################################################################
# constants
#############################################################################
_CLASSNAME       = "Constants"

#############################################################################
# import environment variables
#############################################################################

#############################################################################
# common physical constants
#############################################################################

#############################################################################
# Simulation constants
#############################################################################
# Contour
_WaistContour   = 4                           # float, means gives beam contour (diameter) in geometric tracing in units of the waist 

# Default values for geometric trace
_Rings          = _WaistContour / 2 * 15      # number of rings -- nominal 15 per waist (radius)
_Rays           = 70                          # number of rays per ring at radius 1 w ( ray density per ring is proportional to e^(-2*radius^2 / w0^2) )
#############################################################################
# End
#############################################################################
