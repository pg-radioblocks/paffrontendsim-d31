/*
This file is part of the PAFFrontendSim.

The PAFFrontendSim is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License v3 as published by the Free Software Foundation.

The PAFFrontendSim is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with the PAFFrontendSim. If not, see https://www.gnu.org/licenses/.
*/

/* ------------------------------------------------------------------------*\
 * | c-Library to simulate electromagnetic field propagation in free-space     |
 * | or within a dielectric.                                                   |
 * |                                                                           |
 * |                                                                           |
 * \* ------------------------------------------------------------------------*/
#include <omp.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

/* ------------------------------------------------------------------------*\
 * | Constants                                                                 |
 * \* ------------------------------------------------------------------------*/
/* Program constants      */
#define OUTPUT                1000            /* debug-statement every so often (calculated points in between */
#define CHUNK                 1000            /* minimum chunk length of the parallel calcuated parts  */
#define maxCores                16            /* maximum number of cores to be used */

/* Physical constants */
#define ETA                   376.73031366857 /* impedance of the free-space */

/* ------------------------------------------------------------------------*\
 * | Type-definitions                                                         |
 * \* ------------------------------------------------------------------------*/
typedef struct       /* definition of a 3d-vector */
{
    double x;
    double y;
    double z;
} VECTOR;

typedef struct       /* definition of a 3x3 matrix from 3d-vectors */
{
    VECTOR vx;
    VECTOR vy;
    VECTOR vz;
} MATRIX;

typedef struct        /* Definition of a data point includin position vector, normal vector, E-field, surface current, area element size and intensity */
{
    VECTOR v_;          /* Position vector of the surface point                        */
    VECTOR n_;          /* Normal vevtor of the surface point                          */
    VECTOR Jr_;         /* Real-part of the surface current vector                     */
    VECTOR Ji_;         /* Imaginary part of the surface current vector (reflection)   */
    VECTOR Er_;         /* Real part of the E-field at the surface point (reflection)  */
    VECTOR Ei_;         /* Imaginary part of the E-field at the surface point          */
    VECTOR Jtr_;        /* Real-part of the surface current vector (transmission)      */
    VECTOR Jti_;        /* Imaginary part of the surface current vector (transmission) */
    double dF;          /* Aerea element size                                          */
    double I;           /* Intensity of the E-field ( E_*E_ )                          */
}SURFACE;

VECTOR equal_(double x, double y, double z)                  /* assign a 3d vector by three float numbers */
{
    VECTOR v;

    v.x = x;
    v.y = y;
    v.z = z;
    return v;
}

VECTOR plus_(VECTOR v1, VECTOR v2)                          /* add two 3d vectors */
{
    VECTOR v;
    v.x = v1.x + v2.x;
    v.y = v1.y + v2.y;
    v.z = v1.z + v2.z;
    return v;
}

VECTOR minus_(VECTOR v1, VECTOR v2)                         /* substract two 3d-vectors */
{
    VECTOR v;

    v.x = v1.x - v2.x;
    v.y = v1.y - v2.y;
    v.z = v1.z - v2.z;
    return v;
}

double mult(VECTOR v1, VECTOR v2)                          /* dot product of two 3d vectors */
{
    double skalar;

    skalar =          v1.x * v2.x;
    skalar = skalar + v1.y * v2.y;
    skalar = skalar + v1.z * v2.z;
    return skalar;
}

VECTOR skalar_(double skalar, VECTOR v1)                   /* scalar multiplication of a float number with a 3d vector */
{
    VECTOR v;

    v.x = v1.x * skalar;
    v.y = v1.y * skalar;
    v.z = v1.z * skalar;
    return v;
}

VECTOR operator_(MATRIX m1, VECTOR v1)                     /* right sided product from matrix and 3d vector */
{
    VECTOR v;

    v.x = mult(m1.vx, v1);
    v.y = mult(m1.vy, v1);
    v.z = mult(m1.vz, v1);
    return v;
}

/* Modify positions (faster in c-code than in python) */
void move            ( SURFACE *object, int size, VECTOR move)
{
    int o;                                          /* counter variable */
    register SURFACE *ob;                           /* pointer to actual element */

    for ( o = 0; o < size; ++o)                     /* loop on object */
    {
        ob     = object +o;
        ob->v_ = plus_(ob->v_, move);
    }
}

/* perform matrix mutiplications of surface vectors with a given 3 x 3 matrix */
void transform       ( SURFACE *object, int size, MATRIX m)
{
    int o;                                          /* counter variable */
    register SURFACE *ob;                           /* pointer to actual element */

    for ( o = 0; o < size; ++o)                     /* loop on object */
    {
        ob       = object +o;
        ob->v_   = operator_(m, ob->v_ );
        ob->n_   = operator_(m, ob->n_ );
        ob->Jr_  = operator_(m, ob->Jr_);
        ob->Ji_  = operator_(m, ob->Ji_);
        ob->Er_  = operator_(m, ob->Er_);
        ob->Ei_  = operator_(m, ob->Ei_);
        ob->Jtr_ = operator_(m, ob->Jtr_);
        ob->Jti_ = operator_(m, ob->Jti_);
    }
}

/* Calculate a metal surface -- faster code without second diffraction index */
/* Calculate current-distribution on object 2 taking the current distribution on object one as starting point.*/
void Calc_CurrentDist( SURFACE *surface1, SURFACE *surface2, int size1, int size2, double lambda, double erIn, double tanDelta, int add )
{
    /* Register-variables provide in theory faster access (compiler and cpu dependent) */
    register SURFACE *max1;                                    /* object 1 end-pointer */
    register SURFACE *s1;                                      /* actual element of surface 1 */
    register SURFACE *s2;                                      /* actual element of surface 2 */
    register double   k      = 2 * M_PI / lambda * sqrt(erIn); /* wave-number from wavelength and dielectric constant */
    register double   eta    = ETA / sqrt(erIn);               /* wave impedance for an isotropic, homogeneous dielectric */
    register double   tanD   = tanDelta;                       /* loss factor (set tanD from input data to register variable) */
    register VECTOR   r_;                                      /* normalized distance vector between two elements */
    register double   r;                                       /* distance between two elements (along r_) */
    register double   Jr_r, Ji_r;                              /* dot product between current vectors (real and imaginary) and r_ */
    register double   factor;                                  /* factor with first order terms in J*r */
    register double   ffactor_r;                               /* factor with second order terms in J*r (real-part)*/
    register double   ffactor_i;                               /* (and imaginary part) */
    register VECTOR   Help1_;                                  /* helper vectors to increas calculation speed */
    register VECTOR   Help2_;
    register double   n_Er_, n_Ei_;                            /* dot product between surface normal and electric field */
    register double   cosA;                                    /* cosine alpha (incident angle) */
    register double   sink,cosk;                               /* values of sin(k) and cos(k) */
    register VECTOR   Er_;                                     /* actual electrical field vectors (real and imaginary part) */
    register VECTOR   Ei_;
    /* standard variables (outside the fast inner loop) */
    int               i                  = -1;                 /* counter for progress display  */
    VECTOR            Erold_, Eiold_;                          /* electrical field vectors given as input of surface 2 */
    VECTOR            Jrold_, Jiold_;                          /* current vectors given as input of surface 2 */
    int               s;                                       /* point counter on surface 2 */
    int               spoint             = 0;                  /* calculated points */
    int               chunk              = size2 / maxCores;   /* size of the parallel computed data set */

    if (chunk > CHUNK )
        chunk = CHUNK;
    max1 = surface1 + size1;                                   /* calculate pointer to the last point on surface 1 */

    // add progress statement with maximum number of points to be calculated on surface 2
    printf("                              (of %d Points) -- reflection only\r", size2);

    /* start of parallelization */
    #pragma omp parallel\
    shared(surface2, size2, surface1, max1, i, k, eta, tanD, spoint)\
    private(s, s2, s1, Erold_,  Eiold_,  Jrold_, Jiold_, r_, r, sink, cosk, Jr_r, Ji_r, factor, ffactor_r, ffactor_i, Help1_, Help2_, Er_, Ei_, n_Er_, n_Ei_)\
    num_threads(maxCores)
    {
        #pragma omp for schedule(dynamic) nowait

        for ( s = 0; s < size2; ++s)                        /* loop on all points of surface 2 */
        {
            s2 = surface2 + s;
            i++;
            spoint++;
            if ( i >= OUTPUT )
            {
                i = 0;
                printf("    Calculated point %ld   \r", (long int)(spoint - 1));
                fflush(stdout);
            }
            if (add >= 0)                                       /* add calculated field to existing field ? */
            {
                Erold_ = equal_(0,0,0);                            /* if not, delete existing field data */
                Eiold_ = equal_(0,0,0);
                Jrold_ = equal_(0,0,0);
                Jiold_ = equal_(0,0,0);
            }
            else
            {
                Erold_ = s2->Er_;                                   /* else store existing field */
                Eiold_ = s2->Ei_;
                Jrold_ = s2->Jr_;
                Jiold_ = s2->Ji_;
            }
            s2->Er_ = equal_(0,0,0);                            /* delete all fields of the data point */
            s2->Ei_ = equal_(0,0,0);
            s2->Jr_ = equal_(0,0,0);
            s2->Ji_ = equal_(0,0,0);

            for ( s1 = surface1; s1 < max1; ++s1)               /* loop on all points of surface 1 (source surface) */
            {
                r_.x = -s1->v_.x + s2->v_.x;                        /* calculate distance vector */
                r_.y = -s1->v_.y + s2->v_.y;
                r_.z = -s1->v_.z + s2->v_.z;

                r  = sqrt( r_.x*r_.x + r_.y*r_.y + r_.z*r_.z );     /* calculate distance */

                r_.x = r_.x / r;                                    /* normalize vector */
                r_.y = r_.y / r;
                r_.z = r_.z / r;

                sink = exp(- r * k * tanD) * sin(  - r * k )/ r;    /* calculate Greens-Funktion exp((-tanD*i)rk)/r */
                cosk = exp(- r * k * tanD) * cos(  - r * k )/ r;

                /* calculate dot product Jr_ * r_ and Ji_ * r_ */
                Jr_r = s1->Jr_.x * r_.x +  s1->Jr_.y * r_.y +  s1->Jr_.z * r_.z ;
                Ji_r = s1->Ji_.x * r_.x +  s1->Ji_.y * r_.y +  s1->Ji_.z * r_.z ;

                /* calculate real-part of the factor for first order terms in J*r_  (used in Help1 only) */
                factor = ( 3. / ( r*r * k*k ) - 1.) * Jr_r - Ji_r * 3. / ( r*k );
                /* calculate higher order terms  (used in Help1 and Help2) */
                ffactor_i = -1. / ( k*r );
                ffactor_r = 1. - 1. / ( r*r * k*k );

                /* calculate helper vector 1 Help1_ = Jr_ - ( Jr_ * r_ ) * r_ */
                Help1_.x = ffactor_r  * s1->Jr_.x - ffactor_i * s1->Ji_.x + factor * r_.x;
                Help1_.y = ffactor_r  * s1->Jr_.y - ffactor_i * s1->Ji_.y + factor * r_.y;
                Help1_.z = ffactor_r  * s1->Jr_.z - ffactor_i * s1->Ji_.z + factor * r_.z;

                /* calculate imaginary part of the factor for first order terms (used in Help2 only) */
                factor = ( 3. / ( r*r * k*k ) -1. ) * Ji_r + Jr_r * 3. / ( r*k );

                /* Help2_ = Ji_ - ( Ji_ * r_ ) * r_ */
                Help2_.x = ffactor_r  * s1->Ji_.x + ffactor_i * s1->Jr_.x + factor * r_.x;
                Help2_.y = ffactor_r  * s1->Ji_.y + ffactor_i * s1->Jr_.y + factor * r_.y;
                Help2_.z = ffactor_r  * s1->Ji_.z + ffactor_i * s1->Jr_.z + factor * r_.z;

                /* calculate E_-Field induced from the currents of point o1 on surface 1 at the location o2 on surface 2 */
                /* Imaginary part: Ei_ = ( cos(k) * Hilf1_ - sin(k) * Hilf2_ ) * dF */
                Ei_.x = s1->dF * ( -cosk * Help1_.x + sink * Help2_.x );
                Ei_.y = s1->dF * ( -cosk * Help1_.y + sink * Help2_.y );
                Ei_.z = s1->dF * ( -cosk * Help1_.z + sink * Help2_.z );

                /* Real part: Er_ = ( sin(k) * Hilf1_ - cos(k) * Hilf2_ ) * dF */
                Er_.x = s1->dF * ( sink * Help1_.x + cosk * Help2_.x );
                Er_.y = s1->dF * ( sink * Help1_.y + cosk * Help2_.y );
                Er_.z = s1->dF * ( sink * Help1_.z + cosk * Help2_.z );

                /* calculate the resulting current distribution on surface 2 at location o2 (assuming a perfect conductor) */
                /* using Grassman identity : J = (n_ * E_) * r_ - (n_ * r_) * E_ */
                /* for the reflected part */
                n_Er_   = s2->n_.x * Er_.x + s2->n_.y * Er_.y + s2->n_.z * Er_.z;
                n_Ei_   = s2->n_.x * Ei_.x + s2->n_.y * Ei_.y + s2->n_.z * Ei_.z;

                cosA    = s2->n_.x * r_.x + s2->n_.y * r_.y + s2->n_.z * r_.z;
                s2->Jr_.x += r_.x * n_Er_ - cosA * Er_.x;
                s2->Jr_.y += r_.y * n_Er_ - cosA * Er_.y;
                s2->Jr_.z += r_.z * n_Er_ - cosA * Er_.z;

                s2->Ji_.x += r_.x * n_Ei_ - cosA * Ei_.x;
                s2->Ji_.y += r_.y * n_Ei_ - cosA * Ei_.y;
                s2->Ji_.z += r_.z * n_Ei_ - cosA * Ei_.z;

                /* Sum up E_-Fields */
                s2->Er_.x = s2->Er_.x + Er_.x;
                s2->Er_.y = s2->Er_.y + Er_.y;
                s2->Er_.z = s2->Er_.z + Er_.z;

                s2->Ei_.x = s2->Ei_.x + Ei_.x;
                s2->Ei_.y = s2->Ei_.y + Ei_.y;
                s2->Ei_.z = s2->Ei_.z + Ei_.z;
            } /* End of loop on surface 1 points */

            /* Normalize current distribution and add it to orignal field: J_ = k/(2*Pi) * J_    + old field */
            s2->Jr_ = plus_( Jrold_, skalar_( -k / ( 2. * M_PI ), s2->Jr_ ) );
            s2->Ji_ = plus_( Jiold_, skalar_( -k / ( 2. * M_PI ), s2->Ji_ ) );

            /* Normalize E-field and add it to the original field : E_ = - k/(4*Pi) * E_  + old field */
            s2->Er_ = plus_(Erold_, skalar_( eta * k / ( 4. * M_PI ), s2->Er_) );
            s2->Ei_ = plus_(Eiold_, skalar_( eta * k / ( 4. * M_PI ), s2->Ei_) );
            /* calculate field intensity at point o2 of surface 2: I = E_ * E_ */
            s2->I = ( s2->Er_.x * s2->Er_.x + s2->Ei_.x * s2->Ei_.x + s2->Er_.y * s2->Er_.y + s2->Ei_.y * s2->Ei_.y + s2->Er_.z * s2->Er_.z + s2->Ei_.z * s2->Ei_.z );
        } /* End of loop on surface 2 points */
    }/* End of parallel computing */

    printf("Done...                                                                                                        \r");
}

/* Calculate current-distribution on object 2 taking the current distribution on object one as starting point.*/
void Calc_CurrentDistFull( SURFACE *surface1, SURFACE *surface2, int size1, int size2, double lambda, double erIn, double erOut, double tanDelta, int add )
{
    /* Register-variables provide intheory faster access (compiler and cpu dependent) */
    register SURFACE *max1;                             /* object 1 end-pointer */
    register SURFACE *s1;                               /* actual element of surface 1 */
    register SURFACE *s2;                               /* actual element of surface 2 */
    register double n1   = sqrt(erIn);                  /* diffraction index of input material from dielectric constant (assuming permeability = 1) */
    register double n2   = sqrt(erOut);                 /* diffraction index of output material from dielectric constant (assuming permeability = 1) */
    register double k    = 2 * M_PI / lambda * n1;      /* wave-number from wavelength and dielectric constant */
    register double eta  = ETA / n1;                    /* wave impedance for an isotropic, homogeneous dielectric */
    register double tanD = tanDelta;                    /* loss factor, set tanD from input data to register variable */
    register VECTOR r_;                                 /* normalized distance vector between two elements */
    register double r;                                  /* distance between two elements (along r_) */

    register VECTOR Er_;                                /* actual electrical field vectors (real and imaginary part) */
    register VECTOR Ei_;
    /* helper variables to avoid double calculation */
    register double sinA;                               /* sinus alpha (incident angle) */
    register double cosA;                               /* cosine alpha (incident angle) */
    register double n1cosA;
    register double n22cosA;
    register double sqr;
    register VECTOR n_xr_;                              /* cross-product between n_and r_ */
    register double n_Er_, n_Ei_;                       /* dot product between surface normal and electric field */
    register VECTOR Eter_, Etei_, Etmr_, Etmi_;         /* TE and TM mode of the field at the new surface*/
    register VECTOR Etr_, Eti_, Err_, Eri_;             /* transmitted and reflected part of the field*/
    register double Jr_r, Ji_r;                         /* dot product between current vectors (real and imaginary) and r_ */
    register double factor;                             /* factor with first order terms in J*r */
    register double ffactor_r;                          /* factor with second order terms in J*r (real-part)*/
    register double ffactor_i;                          /* (and imaginary part) */
    register VECTOR Help1_;                             /* helper vectors to increas calculation speed */
    register VECTOR Help2_;
    register double sink,cosk;                          /* values of sin(k) and cos(k) */

    int             i    = -1;                          /* counter for progress display (actual point-number of surface 2) */

    VECTOR          Erold_, Eiold_, Jrold_, Jiold_;     /* electrical field and current vectors given as input of surface 2 */
    VECTOR          Jtrold_, Jtiold_;

    int             s;                                  /* point counter on surface 2 */
    int             spoint = 0;                         /* calculated points */
    int             chunk = size2 / maxCores;           /* size of the parallel computed data set */
    if (chunk > CHUNK )
        chunk = CHUNK;

    max1 = surface1 + size1;                            /* calculate pointer to the last point on surface 1 */

    // add progress statement with maximum number of points to be calculated on surface 2
    printf("                              (of %d Points) -- full simulation\r", size2);

    /* start of parallelization */
    #pragma omp parallel\
    shared(surface2, size2, surface1, max1, i, k, eta, tanD, n1, n2, spoint)\
    private(s, s2, s1, Erold_,  Eiold_,  Jrold_, Jiold_, Jtrold_, Jtiold_,\
    r_, r, sink, cosk, Jr_r, Ji_r, factor, ffactor_r, ffactor_i, Help1_, Help2_,\
    Er_, Ei_,n_Er_, n_Ei_, n_xr_, cosA, sinA, n1cosA, n22cosA, sqr,\
    Eter_, Etei_, Etmr_, Etmi_, Etr_, Eti_, Err_, Eri_)\
    num_threads(maxCores)
    {
        #pragma omp for schedule(dynamic) nowait

        for ( s = 0; s < size2; ++s)                        /* loop on all points of surface 2 */
        {
            s2 = surface2 + s;
            i++;
            spoint++;
            if ( i >= OUTPUT )
            {
                i = 0;
                printf("    Calculated point %ld   \r", (long int)(spoint - 1));
                fflush(stdout);
            }
            if (add >= 0)                                       /* add calculated field to existing field ? */
            {
                Erold_  = equal_(0,0,0);                          /* if not, delete existing field data */
                Eiold_  = equal_(0,0,0);
                Jrold_  = equal_(0,0,0);
                Jiold_  = equal_(0,0,0);
                Jtrold_ = equal_(0,0,0);
                Jtiold_ = equal_(0,0,0);
            }
            else
            {
                Erold_  = s2->Er_;                                /* else store existing field */
                Eiold_  = s2->Ei_;
                Jrold_  = s2->Jr_;
                Jiold_  = s2->Ji_;
                Jtrold_ = s2->Jtr_;
                Jtiold_ = s2->Jtr_;
            }
            s2->Er_  = equal_(0,0,0);                             /* delete all fields of the data point */
            s2->Ei_  = equal_(0,0,0);
            s2->Jr_  = equal_(0,0,0);
            s2->Ji_  = equal_(0,0,0);
            s2->Jtr_ = equal_(0,0,0);
            s2->Jti_ = equal_(0,0,0);

            for ( s1 = surface1; s1 < max1; ++s1)               /* loop on all points of surface 1 (source surface) */
            {
                r_.x = -s1->v_.x + s2->v_.x;                        /* calculate distance vector */
                r_.y = -s1->v_.y + s2->v_.y;
                r_.z = -s1->v_.z + s2->v_.z;

                r  = sqrt( r_.x*r_.x + r_.y*r_.y + r_.z*r_.z );     /* calculate distance */

                r_.x = r_.x / r;                                    /* normalize vector */
                r_.y = r_.y / r;
                r_.z = r_.z / r;

                /* calculate Greens function */
                sink = exp(- r * k * tanD) * sin(  - r * k )/ r;                             /* calculate Greens-Funktion exp((-tanD*i)rk)/r */
                cosk = exp(- r * k * tanD) * cos(  - r * k )/ r;

                /* calculate dot product Jr_ * r_ and Ji_ * r_ */
                Jr_r = s1->Jr_.x * r_.x +  s1->Jr_.y * r_.y +  s1->Jr_.z * r_.z ;
                Ji_r = s1->Ji_.x * r_.x +  s1->Ji_.y * r_.y +  s1->Ji_.z * r_.z ;

                /* calculate real-part of the factor for first order terms in J*r_  (used in Help1 only) */
                factor = ( 3. / ( r*r * k*k ) - 1.) * Jr_r - Ji_r * 3. / ( r*k );
                /* calculate higher order terms  (used in Help1 and Help2) */
                ffactor_i = -1. / ( k*r );
                ffactor_r = 1. - 1. / ( r*r * k*k );

                /* calculate helper vector 1 Help1_ = Jr_ - ( Jr_ * r_ ) * r_ */
                Help1_.x = ffactor_r  * s1->Jr_.x - ffactor_i * s1->Ji_.x + factor * r_.x;
                Help1_.y = ffactor_r  * s1->Jr_.y - ffactor_i * s1->Ji_.y + factor * r_.y;
                Help1_.z = ffactor_r  * s1->Jr_.z - ffactor_i * s1->Ji_.z + factor * r_.z;

                /* calculate imaginary part of the factor for first order terms (used in Help2 only) */
                factor = ( 3. / ( r*r * k*k ) -1. ) * Ji_r + Jr_r * 3. / ( r*k );

                /* Help2_ = Ji_ - ( Ji_ * r_ ) * r_ */
                Help2_.x = ffactor_r  * s1->Ji_.x + ffactor_i * s1->Jr_.x + factor * r_.x;
                Help2_.y = ffactor_r  * s1->Ji_.y + ffactor_i * s1->Jr_.y + factor * r_.y;
                Help2_.z = ffactor_r  * s1->Ji_.z + ffactor_i * s1->Jr_.z + factor * r_.z;

                /* calculate E_-Field induced from the currents of point o1 on surface 1 at the location o2 on surface 2 */
                /* Imaginary part: Ei_ = ( cos(k) * Hilf1_ - sin(k) * Hilf2_ ) * dF */
                Ei_.x = s1->dF * ( -cosk * Help1_.x + sink * Help2_.x );
                Ei_.y = s1->dF * ( -cosk * Help1_.y + sink * Help2_.y );
                Ei_.z = s1->dF * ( -cosk * Help1_.z + sink * Help2_.z );

                /* Real part: Er_ = ( sin(k) * Hilf1_ - cos(k) * Hilf2_ ) * dF */
                Er_.x = s1->dF * ( sink * Help1_.x + cosk * Help2_.x );
                Er_.y = s1->dF * ( sink * Help1_.y + cosk * Help2_.y );
                Er_.z = s1->dF * ( sink * Help1_.z + cosk * Help2_.z );


                /* calculate cosA and sinA from S2-normal and r_ */
                cosA    = -1. * ( s2->n_.x * r_.x + s2->n_.y * r_.y + s2->n_.z * r_.z );     /* cos(alpha) = n_ . r_ */
                n_xr_.x =  -( s2->n_.y * r_.z - s2->n_.z * r_.y );                           /* vector perpendicular to the plane defined by r_ and n_ */
                n_xr_.y =  -( s2->n_.z * r_.x - s2->n_.x * r_.z );
                n_xr_.z =  -( s2->n_.x * r_.y - s2->n_.y * r_.x );
                sinA    = sqrt( n_xr_.x * n_xr_.x + n_xr_.y * n_xr_.y + n_xr_.z * n_xr_.z ); /* sin(alpha) = |n_ x r_|*/
                n_xr_   = skalar_( 1./sinA, n_xr_ );                                         /* normalize the vector for TE-direction */

                /* spilt fields into transmittive and reflective part */
                n1cosA  = n1 * cosA;
                n22cosA = n2 * n2 * cosA;
                /* TE-Mode */
                Eter_   = skalar_( Er_.x * n_xr_.x + Er_.y * n_xr_.y + Er_.z * n_xr_.z, n_xr_);
                Etei_   = skalar_( Ei_.x * n_xr_.x + Ei_.y * n_xr_.y + Ei_.z * n_xr_.z, n_xr_);

                /* TM mode */
                Etmr_   = minus_( Er_, Eter_);
                Etmi_   = minus_( Ei_, Etei_);

                sqr     = n2*n2 - n1*n1 * sinA*sinA;
                if (sqr >= 0)
                {
                    sqr     = sqrt( sqr );
                    /* transmitted electric field */
                    Etr_ = plus_( skalar_( 2. * n1cosA / ( n1cosA + sqr ), Eter_ ), skalar_( 2. * n2 * n1cosA / ( n22cosA + n1 * sqr ), Etmr_ ) );
                    Eti_ = plus_( skalar_( 2. * n1cosA / ( n1cosA + sqr ), Etei_ ), skalar_( 2. * n2 * n1cosA / ( n22cosA + n1 * sqr ), Etmi_ ) );
                    /* reflected electric field */
                    Err_ = plus_( skalar_( ( n1cosA - sqr ) / ( n1cosA + sqr ) , Eter_), skalar_( ( -n22cosA + n1 * sqr ) / ( n22cosA + n1 * sqr ), Etmr_ ) );
                    Eri_ = plus_( skalar_( ( n1cosA - sqr ) / ( n1cosA + sqr ) , Etei_), skalar_( ( -n22cosA + n1 * sqr ) / ( n22cosA + n1 * sqr ), Etmi_ ) );
                }
                else   /*  complex value for sqr */
                {
                    sqr     = sqrt( - sqr );
                    /* transmitted electric field */
                    /* TE mode */
                    Etr_ = skalar_( 2 * n1cosA*n1cosA / ( n1cosA*n1cosA + sqr*sqr ), plus_( Eter_, skalar_(  sqr, Etei_ ) ) );
                    Eti_ = skalar_( 2 * n1cosA*n1cosA / ( n1cosA*n1cosA + sqr*sqr ), plus_( Etei_, skalar_( -sqr, Eter_ ) ) );
                    /* TM mode */
                    Etr_ = plus_( Etr_, skalar_( 2 * n2 * n1cosA / ( n22cosA*n22cosA + n1*n1 * sqr*sqr), plus_( skalar_( n22cosA, Etmr_ ), skalar_(  n1 * sqr, Etmi_ ) ) ) );
                    Eti_ = plus_( Eti_, skalar_( 2 * n2 * n1cosA / ( n22cosA*n22cosA + n1*n1 * sqr*sqr), plus_( skalar_( n22cosA, Etmi_ ), skalar_( -n1 * sqr, Etmr_ ) ) ) );
                    /* reflected electric field */
                    /* TE-mode */
                    Err_ = skalar_( 1. / ( n1cosA*n1cosA + sqr*sqr ), plus_( skalar_( n1cosA*n1cosA - sqr*sqr, Eter_), skalar_(  2 * n1cosA * sqr, Etei_) ) );
                    Eri_ = skalar_( 1. / ( n1cosA*n1cosA + sqr*sqr ), plus_( skalar_( n1cosA*n1cosA - sqr*sqr, Etei_), skalar_( -2 * n1cosA * sqr, Eter_) ) );
                    /* TM-mode */
                    Err_ = plus_( Err_, skalar_( 1. / ( n22cosA*n22cosA + n1*n1 + sqr*sqr ), plus_( skalar_( n22cosA*n22cosA - n1*n1 + sqr*sqr, Etmr_ ), skalar_(  2 * n1 * n22cosA * sqr, Etmi_ ) ) ) );
                    Eri_ = plus_( Eri_, skalar_( 1. / ( n22cosA*n22cosA + n1*n1 + sqr*sqr ), plus_( skalar_( n22cosA*n22cosA - n1*n1 + sqr*sqr, Etmi_ ), skalar_( -2 * n1 * n22cosA * sqr, Etmr_ ) ) ) );
                }

                /* calculate the resulting current distribution on surface 2 at location o2 (assuming a perfect conductor) */
                /* J_ =  n_ x ( r_ x  E_ ) ) using n_ as normal vector at location o2 on surface 2 with the area size dF */
                /* using Grassman identity : J = (n_ * E_) * r_ - (n_ * r_) * E_ (cosA was multiplied with -1 before, hence + instead of - before cosA part)*/
                /* for the reflected part */
                n_Er_   = s2->n_.x * Err_.x + s2->n_.y * Err_.y + s2->n_.z * Err_.z;
                n_Ei_   = s2->n_.x * Eri_.x + s2->n_.y * Eri_.y + s2->n_.z * Eri_.z;

                s2->Jr_.x += r_.x * n_Er_ + cosA * Err_.x;
                s2->Jr_.y += r_.y * n_Er_ + cosA * Err_.y;
                s2->Jr_.z += r_.z * n_Er_ + cosA * Err_.z;

                s2->Ji_.x += r_.x * n_Ei_ + cosA * Eri_.x;
                s2->Ji_.y += r_.y * n_Ei_ + cosA * Eri_.y;
                s2->Ji_.z += r_.z * n_Ei_ + cosA * Eri_.z;

                /* for the transmitted part */
                n_Er_   = s2->n_.x * Etr_.x + s2->n_.y * Etr_.y + s2->n_.z * Etr_.z;
                n_Ei_   = s2->n_.x * Eti_.x + s2->n_.y * Eti_.y + s2->n_.z * Eti_.z;

                s2->Jtr_.x += r_.x * n_Er_ + cosA * Etr_.x;
                s2->Jtr_.y += r_.y * n_Er_ + cosA * Etr_.y;
                s2->Jtr_.z += r_.z * n_Er_ + cosA * Etr_.z;

                s2->Jti_.x += r_.x * n_Ei_ + cosA * Eti_.x;
                s2->Jti_.y += r_.y * n_Ei_ + cosA * Eti_.y;
                s2->Jti_.z += r_.z * n_Ei_ + cosA * Eti_.z;

                /* Sum up E_-Fields */
                s2->Er_.x = s2->Er_.x + Er_.x;
                s2->Er_.y = s2->Er_.y + Er_.y;
                s2->Er_.z = s2->Er_.z + Er_.z;

                s2->Ei_.x = s2->Ei_.x + Ei_.x;
                s2->Ei_.y = s2->Ei_.y + Ei_.y;
                s2->Ei_.z = s2->Ei_.z + Ei_.z;
            } /* End of loop on surface 1 points */

            /* Normalize current distribution and add it to orignal field: J_ = k/(2*Pi) * J_    + old field */
            s2->Jr_  = plus_( Jrold_,  skalar_( -k / ( 2. * M_PI ), s2->Jr_  ) );
            s2->Ji_  = plus_( Jiold_,  skalar_( -k / ( 2. * M_PI ), s2->Ji_  ) );
            /* transmitted signal has 180deg phase-shift against reflected (negavive n-vector)*/
            s2->Jtr_ = plus_( Jtrold_, skalar_(  k / ( 2. * M_PI ), s2->Jtr_ ) );
            s2->Jti_ = plus_( Jtiold_, skalar_(  k / ( 2. * M_PI ), s2->Jti_ ) );
            /* Normalize E-field and add it to the original field : E_ = - k/(4*Pi) * E_  + old field */
            s2->Er_ = plus_(Erold_, skalar_( eta * k / ( 4. * M_PI ), s2->Er_) );
            s2->Ei_ = plus_(Eiold_, skalar_( eta * k / ( 4. * M_PI ), s2->Ei_) );
            /* calculate field intensity at point o2 of surface 2: I = E_ * E_ */
            s2->I = ( s2->Er_.x * s2->Er_.x + s2->Ei_.x * s2->Ei_.x + s2->Er_.y * s2->Er_.y + s2->Ei_.y * s2->Ei_.y + s2->Er_.z * s2->Er_.z + s2->Ei_.z * s2->Ei_.z );
        } /* End of loop on surface 2 points */
    }/* End of parallel computing */

    printf("Done...                                                                                                          \r");
}

