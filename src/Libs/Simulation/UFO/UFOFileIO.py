#!!/usr/bin/env python3
"""
This file is part of the PAFFrontendSim.

The PAFFrontendSim is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License v3 as published by the Free Software Foundation.

The PAFFrontendSim is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with the PAFFrontendSim. If not, see https://www.gnu.org/licenses/.
"""
_CLASSNAME       = "UFOFileIO"

# default values
_COMMENT         = '#'                             # comment string as used in internal files
_HEADERINDICATOR = _COMMENT * 80                   # for readability: start of the header
_KEYINDICATOR    = ':'

from Simulation.UFO.UFO_CCode import _UFOVector, UFODataPoint

def readUFO( columns ):
    """
    creating UFODataPoint from list of string values
    """
    d       = UFODataPoint()
    if len(columns) > 19 :
        d.v     = _UFOVector(float(columns[ 0]), float(columns[ 1]), float(columns[ 2]) )
        d.n     = _UFOVector(float(columns[ 3]), float(columns[ 4]), float(columns[ 5]) )
        d.Jr    = _UFOVector(float(columns[ 6]), float(columns[ 7]), float(columns[ 8]) )
        d.Ji    = _UFOVector(float(columns[ 9]), float(columns[10]), float(columns[11]) )
        d.Er    = _UFOVector(float(columns[12]), float(columns[13]), float(columns[14]) )
        d.Ei    = _UFOVector(float(columns[15]), float(columns[16]), float(columns[17]) )
        d.I     = float(columns[18])
        d.df    = float(columns[19])
    return d

def writeUFO( d ):
    """
    creating list of floats from UFODataPoints
    """
    columns  = [ d.v.x, d.v.y, d.v.z ]
    columns += [ d.n.x, d.n.y, d.n.z ]
    columns += [ d.Jr.x, d.Jr.y, d.Jr.z ]
    columns += [ d.Ji.x, d.Ji.y, d.Ji.z ]
    columns += [ d.Er.x, d.Er.y, d.Er.z ]
    columns += [ d.Ei.x, d.Ei.y, d.Ei.z ]
    columns += [ d.I, d.df]
    return columns

_UFOFile      = {
                    # config
                    "_NAME"                    :   'UFO Data file',             # special entry, name of the header profile
                    "_COMMENT"                 :   '#',                         # special line, indicating comment sign
                    "_KEYINDICATOR"            :   ':',                         # special line, indicating the character for ending keys
                    "_HEADERINDICATOR"         :   '#' * 80,                    # special line, indicating header start / stop-line
                    "_EMPTY"                   :   2,                           # special line, telling how many empty lines are added between HEADERINDICATOR and first Key-word
                    "_UPPERCASE"               :   True,                        # special line, telling to use all keywords in uppercase only (True)
                    "_ADDUNKNOWN"              :   False,                       # special line, telling to add unknown keywords to dictionary (and therewith write them also)
                    "_DELMITTER"               :   " ",                         # special line, indicating string used as delmitter inside the data array
                    "_ACCURACY"                :   12,                          # special line, indicating how many float pointing numbers are printed (int)
                    "_LENGTH"                  :   20,                          # special line, indicating how characters per entry are printed in minimum (int)
                    "_SCIENTIFIC"              :   '',                          # special line, indicating if present that floats are wiriten in scientific notation in the data section
                    "_ReadFunction"            :   readUFO,                     # special line, containing the translation function from column-list to data-line entry
                    "_WriteFunction"           :   writeUFO,                    # special line, containing the translation function from data-line entry to column-list
                    # header

                    "_Comment1"                : "Object description:",         # special comment line in written header
                    "Name"                     : [0, 'U',    "",              "Object name"],
                    "FileState"                : [0, 'd',    "",              "File-state: 8: only surface info; 10: only intensity and field information, 20: full information content"],
                    "SurfaceType"              : [0, 'U',    "TYPE:",         "string, describing the surface type (e.g. lens or aperture for special purpose)"],

                    "_Comment2"                : "Coordinate system:",          # special comment line in written header
                    "Center"                   : [0, 'fff',  "",              "Object coordinate system center (unit: mm)"],
                    "Orientation_x"            : [0, 'fff',  "",              "Object coordinate system x-axis orientation"],
                    "Orientation_z"            : [0, 'fff',  "",              "Object coordinate system z-axis orientation"],

                    "_Comment3"                : "Optical data:",               # special comment line in written header
                    "Er"                       : [0, 'f',    "",              "Permitivity of the free space towards this object"],
                    "tanD"                     : [0, 'f',    "",              "loss factor applied on the free space towards this object"],
                    "FocalLength"              : [0, 's',    "F:",            "expected unit: mm"],
                    "ReflectionAngle"          : [0, 'f',    "REFANGLE:",     "expected unit: degree"],

                    "_Comment4"                : "Simulation data:",            # special comment line in written header
                    "Wavelength"               : [0, 's',    "",              "expected unit: mm"],
                    "ExcitationCurrent"        : [0, 'f',    "I0:",           "expected unit: A"]
                }
