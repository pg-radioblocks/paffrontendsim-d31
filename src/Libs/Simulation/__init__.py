#!/bin/python3
"""
This file is part of the PAFFrontendSim.

The PAFFrontendSim is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License v3 as published by the Free Software Foundation.

The PAFFrontendSim is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with the PAFFrontendSim. If not, see https://www.gnu.org/licenses/.
"""
# Package to simulate PAF-receiver systems
#
# The Package makes use of the UFO-package for simulating mirrors
"""
# TODO
NoiseDataClass :
a) correct losses: Filter losses and antenna losses are to be taken as is, window losses need to be implemented in the radiation pattern

TelescopeDataClass: 
a) implement more telescopes  (Effelsberg secondary focus, FAST, APEX)
b) spill over regions at Effelsberg 100m and at the SKA prototype are still wrong

"""
import sys
import subprocess
import glob
from pathlib import Path
import numpy                          as np  


import matplotlib
try:
    matplotlib.use('TkAgg')
except:
    matplotlib.use('Agg')
    
import matplotlib.pyplot as plt

file_path = Path(__file__).resolve()  # get the path of this file
full_path_tuple = file_path.parts  # split the path into its components

root_index = full_path_tuple.index("paffrontendsim-d31")     # determine at what position in the path the root folder is
root_path_tuple = full_path_tuple[:root_index+1]  # store the path of the root folder
publicLibsPath = str(Path(*root_path_tuple, Path("src", "Libs")))  # reassemble the root path into a string


if publicLibsPath not in sys.path:
    sys.path.append(publicLibsPath)  # add the path to sys.path

_colors = ['blue', 'green',  'purple', 'brown', 'red', 'orange', 'magenta', 'yellow']

_CLASSNAME = "Rx-Sim"

#####################################
# support methods
def makeMovie(namePattern, name = "test.mov", duration = 10) :
    """
    Creates an mpg4 video.

    Parameters:
        namePattern (???): Wildcard-pattern including path for the file-names of the individual plots
        name (string):     Name string of the video
        duration (int):    Duration of the video in seconds
    """
    namePattern = str(namePattern)
    name = str(name)

    no = len(glob.glob(namePattern))
    rate = int(no/duration+0.5)
    x = subprocess.call("""
        #!/bin/bash
        ffmpeg -y -r %d -pattern_type glob -i '%s' -vcodec libx264 -pix_fmt yuv420p -r 25 %s > /dev/null """ %(rate, namePattern, name) , shell = True)
    return 

def getColors(number) :
    colors = _colors
    while len(colors) < number : colors += _colors
    return colors[:number]
