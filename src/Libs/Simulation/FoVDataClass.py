#!/usr/bin/env python3
"""
This file is part of the PAFFrontendSim.

The PAFFrontendSim is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License v3 as published by the Free Software Foundation.

The PAFFrontendSim is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with the PAFFrontendSim. If not, see https://www.gnu.org/licenses/.
"""
#############################################################################
# import libs
#############################################################################
import numpy                          as np
import matplotlib.pyplot              as plt
from   copy                           import copy, deepcopy
from pathlib import Path

try:
    from   scipy.interpolate              import griddata
    from   scipy.spatial                  import Delaunay
    _SciPy = True
except:
    _SciPy = False

import LogClass
from FileIO                         import HDF5Writer
from Simulation                     import ImpedanceMatrixDataClass
from Simulation                     import BeamWeightDataClass
from Simulation                     import SignalChainDataClass
from Simulation.BeamWeightDataClass import _Types as _BW_Types
from Simulation.UFO                 import UFODataClass
from Simulation                     import NoiseDataClass
from Simulation.constants           import _FOV_SIZE_FACTOR, _SkyDistance, _BaseAmp, _RFI_MAX_LEVEL, _SkySize
from Simulation.constants           import _I0iso, _C, _SigPol, _Impedance, _Bandwidth, _kB, _ETA
from Simulation.constants           import _GridSize
from Simulation                     import makeMovie

#############################################################################
# constants
#############################################################################
_CLASSNAME   = "FoVData"

# file-names for output data:

#############################################################################
# Class definition
#############################################################################
class SignalChainData :
    """
    Class with the signal chain data of all elements for direct access in matrix form to simulate the noise contributions.
    Is extracted from individual SignalChainDataClasses!!
    """
    def __init__   ( self ):
        self.Yc                 = []                     # Matrix of open circuit admittances [all 1/Ohm]
        self.VR2                = []                     # Matrix of noise voltage power [all V²]
        self.Zopt               = []                     # Matrix of optimum signal chain input impedances [all Ohm]
        self.ZS                 = []                     # Matrix of signal chain input impedances [all Ohm]
        self.Impedance          = _Impedance             # impedance [Ohm] of the signal chains (used for the mutual impedance
                                                         # matrices)
        self.Bandwidth          = _Bandwidth             # bandwidth [Hz] of the signal chain calculations and of the antenna
                                                         # calculation
        self.Gain               = 1.                     # Gain of the antenna (should be 1 -- is set via FarField.scaleI0 to 1. anyhow)
        self.Name               = "Matrices with array-data"


class FoVDataPoint :
    """
    class housing one data point (source) of the FoV
    """
    def __init__                  ( self , log = None) :
        """
        """
        if log == None:
            self.log = LogClass.LogClass("FoVDataPoint")
        else:
            self.log = deepcopy(log)             # logging class used for all in and output messages
            self.log.addLabel(self)

        Pos_FPlane    = [0., 0.]  # relative position (to bore-sight) of the source in the telescope focal plane [mm] 
        Pos_Sky       = [0., 0.]  # relative position (to bore-sight) of the source on sky [deg] 
        BW_CFM        = BeamWeightDataClass.BeamWeights( log = self.log ) # Beam-weights CFM (BeamWeightDataClass)
        BW_MD         = BeamWeightDataClass.BeamWeights( log = self.log ) # Beam-weights MD  (BeamWeightDataClass)
        BW_SNR        = BeamWeightDataClass.BeamWeights( log = self.log ) # Beam-weights SNR (BeamWeightDataClass)
        insideArray   = False     # boolean telling if the data point is inside the convex hull formed by the array elements


class FoVData :
    """
    The FoV (field of view) of a PAF is the area on sky that it can observe.
    This module derives data related to the FoV from the PAFs antenna farfield patterns and mutual impedance matrices.

    A telescope with a single antenna has a beam that is aligned with the boresight of the telescope.
    The beam is aligned because the antenna is placed in the focal point of the telescope.

    For a PAF, where the antennas are distributed on the focal plane of the telescope, only the central antenna can be in the focal point.
    All surrounding antennas are offset from the focal point. In other words, they are not aligned with the optical axis.
    This causes their beams to be slightly deflected away from the boresight.

    The result is that a PAF possesses a wider FoV than a single antenna at the same frequency.

    The higher the offset of an antenna from the focal point, the higher the angle of deflection of its beam.
    At extreme offsets, the beams noticeably "smear" away from the boresight, which is undesireable.

    In theory a PAFs FoV is roughly equivalent to the angle of deflection of the outermost antennas in the array.
    """
    def __init__(self, astroSourcePol, SimWavelength, telescopeFocalLength = 30000., log = None, dataWriter = None ):
        """
        Initialize class.
        
        Parameters:
            SimWavelength (float):        wavelength [mm] at which the far-field pattern simulation was performed
            astroSourcePol (float):       polarization angle [degree] of the astronomical source used for beam-forming
            telescopeFocalLength (float): telescope focal length [mm] to transform sky position to focal-plane position
            log (LogClass): Used for logging
        """
        if log == None:
            self.log             = LogClass.LogClass(_CLASSNAME)
        else:
            self.log             = deepcopy(log)                 # logging class used for all in and output messages
            self.log.addLabel(self)

        if dataWriter == None:
            self.dataWriter = HDF5Writer.HDF5Writer(_CLASSNAME+".h5")
        else:
            self.dataWriter = dataWriter             # HDF5 writer to store simulation data

        self.astroSourcePol  = astroSourcePol
        self.SimWavelength   = SimWavelength
        self.telescopeF      = telescopeFocalLength          # telescope focal length [mm] of the telescope performing the simulation (required only for unit conversion)
        # fov-data
        self.fovData         = []                            # list of FoV-data-points
        self.Rsig_cfm        = []                            # signal correlation matrix (sum of all outer products of the weights)
        self.Rsig_md         = []                            # signal correlation matrix (sum of all outer products of the weights)
        self.Rsig_snr        = []                            # signal correlation matrix (sum of all outer products of the weights)
        
        self.max_EtaA_CFM    = 0.                            # maximum Aperture efficiency in the FoV (for normalization purpose)
        self.max_EtaA_MD     = 0.                            # maximum Aperture efficiency in the FoV (for normalization purpose)
        self.max_EtaA_SNR    = 0.                            # maximum Aperture efficiency in the FoV (for normalization purpose)
        
        # Noise data class belonging to the far-field setup
        self.noise           = NoiseDataClass.NoiseData(log = self.log, dataWriter = self.dataWriter)
        self.Zopt            = []                            # impedance matching matrix
        self.signalChainData = SignalChainData()             # signal chain data (Zopt, Yc, VR2) from all chains in the array in matrix writing
        
        self.clearFarFieldResponse()                         # establish far-field response data-set
        
        self.idxList         = []                            # index list as used for the FoV calculations

        # mutual impedance matrices
        self.Asky            = ImpedanceMatrixDataClass.ImpedanceMatrix(log = self.log)

        self.Losses          = []                            # list of dictionaries with the individual loss components
        self.ALosses         = []                            # 2d-list of impedance matrices from the individual loss compnents


    def printOut                  ( self, line, message_type, noSpace = True, lineFeed = True ) :
        """
        Wrapper for the printOut method of the actual log class
        """
        self.log.printOut(line, message_type, noSpace, lineFeed)


    def in_Array                  ( self, points, hullPoints = []) :
        """
        Test if points are in the convex hull of 'hullPoints'.

        Parameters:
            points  (numpy.array?): N times d numpy array of the coordinates of N points in d dimensions
            hullPoints (int?): N times d numpy array of the coordinates of N points in d dimensions forming the hull. If empty self.e_pos is taken

        Returns:
            numpy.array (numpy.array): of booleans telling if the point in points is inside (True) or outside (False) the hull of hullPionts
        """
        if len(hullPoints) < 1 :
            hullPoints = self.e_pos[:, 0:2]   
            
        if ( not _SciPy ) or ( len( hullPoints ) < 3 ) :
            return [True] * len( points )
       
        hull = Delaunay( hullPoints )
        return hull.find_simplex( points ) > 0       # check for inside, the hull itself is already declared as outside!


    def getCenter                 ( self ) :    # Used in ArrayPlotClass
        """
        Determines the coordinates of the most central formed beam.

        Returns:
            fdMin (list): Coordinates of the beam closest to boresight.
        """
        minDist = 9.e12
        fdMin   = None
        for fd in self.fovData :
            d = np.sqrt(fd.Pos_FPlane[0]**2. + fd.Pos_FPlane[1]**2.)
            if d < minDist :
                minDist = d
                fdMin   = fd
        return fdMin
    
    #####################################
    # File IO methodes
    def writeFoVData              ( self, fileName, BW_Type = _BW_Types['CFM'] ) :
        """
        Save all noise data of the actual FoV to a file for external processing.

        Parameters:
            fileName (string): Full file path to write the data to.
            BW_Type (string): Beamweight type (CFM, MD, SNR are available) to be written.
        """

        self.printOut("Writing FoV data to file '%s'" % fileName, self.log.FLOW_TYPE)
        self.printOut("  Using Beam-weight type '%s'" % BW_Type, self.log.DATA_TYPE, False)
        bw_idx = 2      # default beam-weight type is SNR
        if BW_Type == _BW_Types['CFM'] : bw_idx = 0
        if BW_Type == _BW_Types['MD']  : bw_idx = 1

        fh = open(fileName, 'w')                                       # open file for writing
        fh.write("# xpos[deg] ypos[deg] phi[deg] theta[deg] lambda[mm] Eta_aperture Eta_spill Eta_rad ")
        fh.write("T_sig[K] T_sky[K] T_spill[K] T_loss[K] T_rec[K] T_sys[K] Tsys_oe[K] in/OutOfHull\n")
        #loop all data
        for p in self.fovData :
            fh.write("%.3f %.3f " % (p.Pos_Sky[0], p.Pos_Sky[1]  ) )  # write position in degrees
            fh.write("%.3f %.3f " % (p.BW_CFM.Phi, p.BW_CFM.Theta) )  # write polar coordinates
            fh.write("%.3f "      % (p.BW_CFM.Wavelength         ) )  # write wavelength
            # write noise data of the designated BW-Type
            bw = [p.BW_CFM, p.BW_MD, p.BW_SNR][bw_idx]
            fh.write("%.3f %.3f %.3f "      % ( bw.Efficiency.Eta_a, bw.Efficiency.Eta_spill,  bw.Efficiency.Eta_rad ) )
            fh.write("%.3f %.3f %.3f %.3f " % ( bw.Noise.T_sig, bw.Noise.T_sky, bw.Noise.T_spill, bw.Noise.T_loss) )
            fh.write("%.3f %.3f %.3f "      % ( bw.Noise.T_rec, bw.Noise.T_sys, bw.Noise.T_sys_oe) )
            fh.write("%d\n"                 % ( int(p.insideArray) ) )
        fh.close()


    def writeZopt                 ( self, fileName ) :
        """
        Save actual matching impedance in a file.

        Parameters:
            fileName (string): Full file path to write the data to.
        """
        self.printOut("Saving actual matching impedance to file '%s'" % fileName, self.log.FLOW_TYPE)

        fh_zopt = open(fileName, 'w')
        fh_zopt.write("# element_x element_y Zopt_real[Ohm] Zopt_imag[Ohm]\n")
        for x in range(self.Asky.noElements) :
            for y in range(self.Asky.noElements) :
                fh_zopt.write("%d %d %.2f %.2f\n" %(x, y, self.Zopt[x, y].real, self.Zopt[x, y].imag) )
        fh_zopt.close()


    def writeElementUsage         ( self, fileName ) :
        """
        Saves element usage within the FoV (all calculated beams) in a file.

        Parameters:
            fileName (string): Full file path to write the data to.
        """
        self.printOut("Saving actual element usage to file '%s'" % fileName, self.log.FLOW_TYPE)

        if len(self.Rsig_cfm) > 0 :
            fh = open(fileName, 'w')          # open file for writing
            fh.write("# element-number element_pos_x element_pos_y usage_cfm usage_md usage_snr (all normalized to number of best efficent beams)\n")
            for el in range(len(self.fovData[0].BW_CFM.Data)) :
                fh.write("%d %.3f %.3f %.3f %.3f %.3f\n" % (el,
                                                            self.e_pos[el, 0], self.e_pos[el, 1],
                                                            np.absolute(self.Rsig_cfm[el, el] ) / self.max_EtaA_CFM,
                                                            np.absolute(self.Rsig_md [el, el] ) / self.max_EtaA_MD,
                                                            np.absolute(self.Rsig_snr[el, el] ) / self.max_EtaA_SNR
                                                        ) )
            fh.close()

    #####################################
    # Set data

    def clearFarFieldResponse     ( self ) :
        """
        Clears all far-field response data from the array elements.
        """
        # signal response data
        self.v_signal    = []   # positions (3d) [mm] of all signal belonging data points
        self.df_signal   = []   # area element size belonging to the position [mm^2] (first index correspnds to first index of v_signal).
        self.e_signal    = []   # complex e-field at the position. First index is chain-number, second index correspnds to first index of v_signal.
        self.I_signal    = []   # field intensity at the position (first index correspnds to first index of v_signal).
        self.e_pos       = []   # positions of maximum intensity (first index correspnds to first index of v_signal).

        self.Losses      = []   # list of dictionaries with spill-over fields, position vectors, area elements and temperature weighted fields
        self.ALosses     = []

        ## spill-over response data
        #self.v_spill     = []   # positions (3d) [mm] of all spill-over belonging data points
        #self.df_spill    = []   # area element size belonging to the position [mm^2] (first index correspnds to first index of v_spill).
        #self.e_spill     = []   # complex e-field at the position. First index is chain-number, second index correspnds to first index of v_spill.

        ## spill-over response weighted with spill-over temperatures at the belonging positions
        #self.v_spillT    = []   # positions (3d) [mm] of all spill-over belonging data points weighted with the local temperature at this point
        #self.df_spillT   = []   # area element size belonging to the position [mm^2] (first index correspnds to first index of v_spillT).
        #self.e_spillT    = []   # complex temperature weighted e-field at the position. First index is chain-number, second index correspnds to first index of v_spillT.

        self.ufoTemplate = None # empty surface data class holding the template for the signal response


    def setFarfieldResponse       ( self, sky, spill, spillT ) :
        """
        Add a new signal chain far-field response to the far-field response arrays.

        Parameters:
            sky (UFODataClass):    UFO data class holding the signal (on sky) far-field response of the element
            spill (UFODataClass):  UFODataClass or list of UFODataClasses holding the spill-over far-field response(s) of the element
            spillT (UFODataClass): UFODataClass or list of UFODataClasses holding the spill-over far-field response(s) of the element weighted at each point with the belonging black-body temperature

        """
        self.printOut( "Setting farfield response", self.log.FLOW_TYPE )
        if isinstance( spill, UFODataClass.UFOData ) :
            spill  = [ spill ]
        if isinstance( spillT, UFODataClass.UFOData ) :
            spillT = [ spillT ]

        self.printOut( "  got %d loss-fields" % len( spill ),  self.log.DATA_TYPE, False )

        v_signal, e_signal, I_signal, df_signal = sky.getData()

        # check if a sky template for the beam-formed sky data is present (means also no data is present at all
        if  self.ufoTemplate == None :
            self.printOut( "  first Farfield, initializing all arrays", self.log.DATA_TYPE, False )
            self.ufoTemplate = deepcopy(sky)                                  # if not, copy sky data and use it as template
            self.v_signal    = v_signal                                       # and fill position and area sizes accordingly
            self.df_signal   = df_signal
            self.e_signal    = np.asarray( [e_signal] )
            self.I_signal    = np.asarray( [I_signal] )

            self.Losses = []
            for idx, sp in enumerate ( spill ) :
                if sp.Type > 10 :                                             # check that the UFO-type is current ==> field data are available
                    v_spill,  e_spill,  I_spill,  df_spill  = spill [idx].getData()
                    v_spillT, e_spillT, I_spillT, df_spillT = spillT[idx].getData()
                    self.Losses.append( {'Name' : sp.Name, 'v' :  v_spill, 'df' : df_spill, 'spill' :np.asarray(  [ e_spill ] ), 'spillT' : np.asarray( [ e_spillT ] ) } )

        else :
            # add data to the existing arrays
            self.e_signal = np.append( self.e_signal, [e_signal], axis = 0 )          # append signal field array
            self.I_signal = np.append( self.I_signal, [I_signal], axis = 0 )          # append intensity array

            for idx, sp in enumerate ( spill ) :
                name = sp.Name
                for lossEntry in self.Losses :
                    if lossEntry['Name'] == name :
                        v_spill,  e_spill,  I_spill,  df_spill  = spill [idx].getData()
                        v_spillT, e_spillT, I_spillT, df_spillT = spillT[idx].getData()
                        lossEntry['spill']  =  np.append( lossEntry['spill'],  [ e_spill ],  axis = 0 )
                        lossEntry['spillT'] =  np.append( lossEntry['spillT'], [ e_spillT ], axis = 0 )

        # set element positions in FoV data class
        max_idx = np.argmax(self.I_signal[-1, :])
        if len( self.e_pos ) == 0 :
            self.e_pos = [ self.v_signal[max_idx] ]
        else :
            self.e_pos = np.append( self.e_pos, [ self.v_signal[max_idx] ], axis = 0 )

        self.e_pos = np.asarray( self.e_pos )


    def _calcNoiseMatrices        ( self, noChains, element_list ):
        """
        calculate Zc, Yc and VR2 matrix for the chain distribution as set up.

        Parameters:
            noChains (int) :     Number of available receiver chains
            element_list (list): List of Array-Elements, or single signal chain (SignalChainDataClass)
        """
        self.printOut( "Calculating LNA noise matrix for wavelength %.2f mm" % self.SimWavelength, self.log.FLOW_TYPE )
        size = noChains

        self.signalChainData.Yc   = np.zeros( (size, size), dtype = np.complex128)
        self.signalChainData.Zopt = np.zeros( (size, size), dtype = np.complex128)
        self.signalChainData.VR2  = np.zeros( (size, size), dtype = np.complex128)
        self.signalChainData.ZS   = np.zeros( (size, size), dtype = np.complex128)

        if noChains != 0 :

            if isinstance( element_list, SignalChainDataClass.SignalChain) :
                tmin, rn, zopt, s11, s12, s21, s22 = element_list.getData( _C / self.SimWavelength / 1e6 )
                for ch in range(noChains) :
                    self.signalChainData.Yc  [ch, ch] = tmin / ( 2. * element_list.T0 ) / rn
                    self.signalChainData.Zopt[ch, ch] = zopt
                    self.signalChainData.VR2 [ch, ch] = 4. * _kB * element_list.T0 * rn
                    self.signalChainData.ZS  [ch, ch] = element_list.Zs
            else :
                ch  = 0
                for el in element_list :
                    for chain in el.chains :
                        tmin, rn, zopt, s11, s12, s21, s22 = chain.signalChain.getData( _C / self.SimWavelength / 1e6 )
                        self.signalChainData.Yc  [ch, ch] = tmin / ( 2. * chain.signalChain.T0 ) / rn
                        self.signalChainData.Zopt[ch, ch] = zopt
                        self.signalChainData.VR2 [ch, ch] = 4. * _kB * chain.signalChain.T0 * rn
                        self.signalChainData.ZS  [ch, ch] = chain.signalChain.Zs
                        ch += 1


    def calculateCouplingMatrices (self, telescopeDiameter ):
        """
        Calculate all impedance matrices required for beam-forming and noise determination.

        Parameters:
            telescopeDiameter (float): Diameter of the primary dish.
        """
        self.printOut( "Calculating all mutual coupling matrices", self.log.FLOW_TYPE )
        # set impedance matrix data
        self.Asky.Wavelength     = self.SimWavelength
        self.Asky.Impedance      = self.signalChainData.Impedance
        self.Asky.I0             = _I0iso
        self.Asky.Bandwidth      = self.signalChainData.Bandwidth
        self.Asky.DetectorGain   = self.signalChainData.Gain
        self.Asky.Aeff           = telescopeDiameter**2. / 4.*np.pi # telescope area
        self.Asky.Name           = 'sky'
        self.Asky.log            = self.log

        # calculate mutual impedance matrices
        self.Asky.correlateFields   (self.e_signal, self.df_signal )

        self.ALosses             = []
        for lossEntry in self.Losses :
            A      = deepcopy( self.Asky )
            A.Name = lossEntry['Name']
            AT     = deepcopy( A )

            A.correlateFields ( lossEntry['spill'],  lossEntry['df']  )
            AT.correlateFields( lossEntry['spillT'], lossEntry['df'] )
            self.printOut( "  Power data for A_%s[0,0] results in %.2e" % ( lossEntry['Name'], A.Data[0,0].real ), self.log.DATA_TYPE, False )
            self.ALosses.append    ( [A, AT] )

    #####################################
    # plotting methods_
    def plotFarFieldSkyResponse   ( self, element = -1, figname = "", show = False) :
        """
        Plot the far-field response of the full array on sky.

        Parameters:
            element (int):    array element to plot (-1 summs up all elements)
            figname (string): Filename of the figure (if given figure is only shown on screen if show == True
            show (boolean):   flag to show plot on screen even when being written to disk (True)
        """
        self.printOut("Plotting array far field foot-print", self.log.FLOW_TYPE)
        if not _SciPy :
            self.printOut("  SciPy-Lib is not available, can't create the plot!", self.log.WARNING_TYPE, False)
            return

        if element < 0 :
            self.printOut("  combining all elements", self.log.DATA_TYPE, False)
        else:
            self.printOut("  using element no. %d" % int(element), self.log.DATA_TYPE)

        freq    = "%.2fGHz" % ( int( _C / self.SimWavelength / 1e4 + .5 ) / 100. )

        if element > len(self.I_signal) :
            element = 0
            self.printOut("  element number out of range, using element 0 instead", self.log.WARNING_TYPE, False)

        x  = self.v_signal[:, 0]*180.*60./(np.pi*_SkyDistance)
        y  = self.v_signal[:, 1]*180.*60./(np.pi*_SkyDistance)

        xe = self.e_pos[:, 0]   *180.*60./(np.pi*_SkyDistance)
        ye = self.e_pos[:, 1]   *180.*60./(np.pi*_SkyDistance)

        plotEpos = False
        if element < 0 :
            z        = np.sum(self.I_signal, axis = 0)
            elem     = "all elements\n( markers denote the element positions)"
            plotEpos = True
        else :
            z        = self.I_signal[element, :]
            elem     = "element %d" % element

        # get dB scale
        z = z/np.max(z)
        for z_idx, zel in enumerate(z) :
            try:
                log = np.log10(zel)*10.
            except:
                log = -60.
            z[z_idx] = log

        # re-grid data
        xi, yi = np.mgrid[min(x):max(x):_GridSize*1j, min(y):max(y):_GridSize*1j]
        zi = griddata((x, y), z, (xi, yi), method='linear')

        fig, ax = plt.subplots()
        cf = ax.pcolormesh(xi, yi, zi, vmin= -40., vmax= 0., cmap = plt.get_cmap('rainbow'))
        ax.contour(xi, yi, zi, levels=[-30, -20, -10, -3, -1], linewidths = 0.5, colors = 'k')

        #cf = ax.pcolormesh(xi, yi, zi, cmap = plt.get_cmap('rainbow'))
        #ax.contour(xi, yi, zi, levels=[0.1, 0.5, 0.95], linewidths = 0.5, colors = 'k')

        if plotEpos :
            ax.plot( xe, ye, 'kx')

        ax.set_xlabel("sky position [minutes of arc]")
        ax.set_ylabel("sky position [minutes of arc]")
        ax.set_title ("%s : Far-field intensity of %s" % (freq, elem) )
        ax.set_aspect('equal')
        fig.colorbar(cf)

        if figname != "" :
            fig.savefig(figname)
            if show :
                plt.show()
        else :
            plt.show()

        plt.close()


    def plotNoise                 ( self, beamforming = 0, parameter = 6, figname = "", show = False ) :
        """
        Plot the far-field response of the full array on sky.

        Parameters:
            beamforming (int): beam-forming algorithm (0: CFM, 1: MD, 2: max.SNR)
            parameter (int):   noise parameter (0 : Tsig, 1 : Tsky, 2 : Tspill, 3 : Tloss, 4 : Trec, 5 : Tsys, 6 : Tsys/eta
            figname (string):  Filename of the figure (if given figure is only shown on screen if show == True
            show (boolean):    Flag to show plot on screen even when being written to disk (True)
        """
        labels    = ["Tsig", "Tsky", "Tspill", "Tloss", "Trec", "Tsys", "Tsys/eta"]
        bf_labels = ["CFM", "max. direct.", "max. SNR"]
        if parameter   < 0               : parameter   = 0
        if parameter   >= len(labels)    : parameter   = len(labels)-1
        if beamforming < 0               : beamforming = 0
        if beamforming >= len(bf_labels) : beamforming = len(bf_labels)-1

        self.printOut("Plotting %s for %s beamforming over the FoV :" % (labels[parameter], bf_labels[beamforming]), self.log.FLOW_TYPE)
        if not _SciPy :
            self.printOut("  SciPy-Lib is not available, can't create the plot!", self.log.WARNING_TYPE, False)
            return

        if len( self.fovData ) < 4 :
            self.printOut("  Not enough beams available to create a usefull map!", self.log.WARNING_TYPE, False)
            return

        freq    = "%.2fGHz" % ( int(_C / self.SimWavelength / 1e4 + .5 ) / 100. )

        x = np.zeros(len(self.fovData), dtype = np.float64)
        y = np.zeros(len(self.fovData), dtype = np.float64)
        z = np.zeros(len(self.fovData), dtype = np.float64)
        x = []
        y = []
        z = []

        xe = self.e_pos   [:, 0]*180.*60./(np.pi*_SkyDistance)
        ye = self.e_pos   [:, 1]*180.*60./(np.pi*_SkyDistance)

        self.printOut("  extracting data", self.log.DEBUG_TYPE, False)
        # extract data
        for p_idx, p in enumerate(self.fovData) :
            if p.insideArray :
                x.append(p.Pos_Sky[0]*60.)
                y.append(p.Pos_Sky[1]*60.)
                BW       = [p.BW_CFM.Noise, p.BW_MD.Noise, p.BW_SNR.Noise][beamforming]
                z.append([BW.T_sig, BW.T_sky, BW.T_spill, BW.T_loss, BW.T_rec, BW.T_sys, BW.T_sys_oe][parameter])

        x = np.asarray(x)
        y = np.asarray(y)
        z = np.asarray(z)

        self.printOut("  found %d data points" % len(z), self.log.DEBUG_TYPE, False)
        # limit z-range to 5 times minimum value
        minz = np.min(z)
        maxz = minz*5.
        if maxz > 500. : maxz = 500.
        self.printOut("  using as limits: %.2fK to %.2fK" %(minz, maxz), self.log.DEBUG_TYPE, False)
        # set levels for contours
        levels = np.asarray([1./np.sqrt(0.9), 1./np.sqrt(0.5), 1./np.sqrt(0.25)])*minz
        # re-grid data
        xi, yi = np.mgrid[min(x):max(x):_GridSize*1j, min(y):max(y):_GridSize*1j]
        zi = griddata((x, y), z, (xi, yi), method='linear')
        # plot data
        self.printOut("  mashing finished", self.log.DEBUG_TYPE, False)
        fig, ax = plt.subplots()
        cf = ax.pcolormesh(xi, yi, zi, cmap = plt.get_cmap('rainbow'),vmin=0, vmax=maxz)

        ax.contour(xi, yi, zi, levels=levels, linewidths = 0.5, colors = 'k')

        ax.plot(xe, ye, 'k+')
        ax.plot(x,  y,  'k.')

        # establish labling
        ax.set_xlabel("sky position [minutes of arc]")
        ax.set_ylabel("sky position [minutes of arc]")
        ax.set_title ("%s : %s [K] for %s beam-forming over the FoV\ncontours : Mapping speed 90%%, 50%%, 25%%)\nx-markers denote the element positions" % ( freq, labels[parameter], bf_labels[beamforming] ))

        fig.colorbar(cf)
        ax.set_aspect('equal')
        # safe of show figure?

        if figname != "" :
            fig.savefig(figname)
            if show :
                plt.show()
        else :
            plt.show()
        plt.close()


    def plotEta                   ( self, beamforming = 0, figname = "", show = False ) :
        """
        Plot the far-field response of the full array on sky.

        Parameters:
            beamforming (int): Beam-forming algorithm (0: CFM, 1: MD, 2: max.SNR)
            figname (string):  Filename of the figure (if given figure is only shown on screen if show == True
            show (boolean):    Flag to show plot on screen even when being written to disk (True)
        """
        bf_labels = ["CFM", "max. direct.", "max. SNR"]

        if beamforming < 0               : beamforming = 0
        if beamforming >= len(bf_labels) : beamforming = len(bf_labels)-1

        self.printOut("Plotting Eta for %s beamforming over the FoV :" % (bf_labels[beamforming]), self.log.FLOW_TYPE)
        if not _SciPy :
            self.printOut("  SciPy-Lib is not available, can't create the plot!", self.log.WARNING_TYPE, False)
            return

        if len( self.fovData ) < 4 :
            self.printOut("  Not enough beams available to create a usefull map!", self.log.WARNING_TYPE, False)
            return

        freq    = "%.2fGHz" % ( int(_C / self.SimWavelength / 1e4 + .5 ) / 100. )

        x = np.zeros(len(self.fovData), dtype = np.float64)
        y = np.zeros(len(self.fovData), dtype = np.float64)
        z = np.zeros(len(self.fovData), dtype = np.float64)

        xe = self.e_pos   [:, 0]*180.*60./(np.pi*_SkyDistance)
        ye = self.e_pos   [:, 1]*180.*60./(np.pi*_SkyDistance)
        # extract data
        for p_idx, p in enumerate(self.fovData) :
            x[p_idx] = p.Pos_Sky[0]*60.
            y[p_idx] = p.Pos_Sky[1]*60.

            z[p_idx] = [p.BW_CFM.Efficiency.Eta_a, p.BW_MD.Efficiency.Eta_a, p.BW_SNR.Efficiency.Eta_a][beamforming]

        # set levels for contours
        levels = np.asarray([np.sqrt(0.25), np.sqrt(0.5), np.sqrt(0.9)])*np.max(z)

        # re-grid data
        xi, yi = np.mgrid[min(x):max(x):_GridSize*1j, min(y):max(y):_GridSize*1j]
        zi = griddata((x, y), z, (xi, yi), method='linear')
        # plot data
        fig, ax = plt.subplots()
        cf = ax.pcolormesh(xi, yi, zi, cmap = plt.get_cmap('rainbow'))
        ax.contour(xi, yi, zi, levels=levels, linewidths = 0.5, colors = 'k')
        ax.plot(xe, ye, 'k+')
        ax.plot(x,  y,  'k.')
        # establish labling
        ax.set_xlabel("sky position [minutes of arc]")
        ax.set_ylabel("sky position [minutes of arc]")
        ax.set_title ("%s : Apperture-Efficiency for %s beam-forming over the FoV\ncontours : Mapping speed 90%%, 50%%, 25%%\nx-markers denote the element positions" % (freq, bf_labels[beamforming]))
        fig.colorbar(cf)
        ax.set_aspect('equal')
        # safe of show figure?
        if figname != "" :
            fig.savefig(figname)
            if show :
                plt.show()
        else :
            plt.show()

        plt.close()


    def plotElementUsage          ( self, figname = "", show = False ) :
        """
        Plot the element usage of the beam-forming over the FoV.

        Parameters:
            figname (string):  Filename of the figure (if given figure is only shown on screen if show == True
            show (boolean):    Flag to show plot on screen even when being written to disk (True)
        """
        self.printOut("Plotting the element usage", self.log.FLOW_TYPE)
        freq    = "%.2fGHz" % ( int( _C / self.SimWavelength / 1e4 + .5 ) / 100. )
        if len (self.Rsig_cfm) < 1 : return

        useCFM      = []
        useMD       = []
        useSNR      = []

        elements = range(len(self.fovData[0].BW_CFM.Data))
        for el in elements :
            useCFM.append( np.absolute(self.Rsig_cfm[el, el] ) )#/ self.max_EtaA_CFM )
            useMD.append ( np.absolute(self.Rsig_md [el, el] ) )#/ self.max_EtaA_MD  )
            useSNR.append( np.absolute(self.Rsig_snr[el, el] ) )#/ self.max_EtaA_SNR )

        useCFM  = np.asarray( useCFM )
        useCFM /= np.max( useCFM )
        useMD   = np.asarray( useMD )
        useMD  /= np.max( useMD )
        useSNR  = np.asarray( useSNR )
        useSNR /= np.max( useSNR )

        fig, ax = plt.subplots()

        elements = np.asarray(elements) + 1

        ax.plot(elements, useCFM, 'k-')
        ax.plot(elements, useMD , 'b-')
        ax.plot(elements, useSNR, 'r-')
        fig.legend(['CFM', 'MD', 'SNR'], loc='upper left', bbox_to_anchor=(0.75, 0.85))
        # establish labling
        ax.set_ylim(0., 1.)
        ax.set_xlabel("Signal-Chain number")
        ax.set_ylabel("Element usage [au]")
        ax.set_title ("%s : Relative element usage " % (freq) )

        # safe of show figure?
        if figname != "" :
            fig.savefig(figname)
            if show :
                plt.show()
        else :
            plt.show()

        plt.close()


    def plotCenterBW              ( self, beamforming = 0, figname = "", show = False ) :
        """
        Plot the far-field response of the full array on sky.

        Parameters:
            beamforming (int): Beam-forming algorithm (0: CFM, 1: MD, 2: max.SNR)
            figname (string):  Filename of the figure (if given figure is only shown on screen if show == True
            show (boolean):    Flag to show plot on screen even when being written to disk (True)
        """
        self.printOut("Plotting the central formed beam beam-weights", self.log.FLOW_TYPE)
        freq     = int( _C / self.SimWavelength / 1e4 + .5 ) / 100.

        if beamforming < 0 : beamforming = 0
        if beamforming > 2 : beamforming = 2

        # get beam closest to bore-sight
        dist      = 9.e12
        fCenter   = None

        for f in self.fovData :
            d = f.Pos_FPlane[0]**2. + f.Pos_FPlane[1]**2.
            if d < dist :
                fCenter = f
                dist    = d

        bws      = [fCenter.BW_CFM, fCenter.BW_MD, fCenter.BW_SNR]
        bws[beamforming].plot(figname = figname, show = show, freq = freq)


    def plotCenterBeam            ( self, beamforming = 0, figname = "", show = False ) :
        """
        Plot the far-field response of the central beam.

        Parameters:
            beamforming (int): Beam-forming algorithm (0: CFM, 1: MD, 2: max.SNR)
            figname (string):  Filename of the figure (if given figure is only shown on screen if show == True
            show (boolean):    Flag to show plot on screen even when being written to disk (True)
        """
        xpos = 0
        ypos = 0

        self.printOut("Plotting the formed beam at f-plane position (%.2f, %.2f)mm" %( xpos, ypos), self.log.FLOW_TYPE)
        if not _SciPy :
            self.printOut("  SciPy-Lib is not available, can't create the plot!", self.log.WARNING_TYPE, False)
            return

        freq    = "%.2fGHz" % ( int( _C / self.SimWavelength / 1e4 + .5 ) / 100. )

        # get beam closest to given point
        dist      = 9.e12
        fCenter   = None

        for f in self.fovData :
            d = (f.Pos_FPlane[0]-xpos)**2. + (f.Pos_FPlane[1]-ypos)**2.
            if d < dist :
                fCenter = f
                dist    = d

        beam     = ["CFM", "MD", "SNR"][beamforming]
        bws      = [fCenter.BW_CFM, fCenter.BW_MD, fCenter.BW_SNR][beamforming]
        #simulate the beam
        field = self.simulateBeam(bws)
        # plot the beam
        x = np.zeros(field.points, dtype = float)
        y = np.zeros(field.points, dtype = float)
        I = np.zeros(field.points, dtype = float)
        for idx in range(field.points) :
            x[idx] = field.Data[idx].v.x *180.*60./(np.pi*_SkyDistance)
            y[idx] = field.Data[idx].v.y *180.*60./(np.pi*_SkyDistance)
            I[idx] = np.absolute(field.Data[idx].I)*field.Data[idx].df

        I = I/np.max(I)
        I = np.log10(I)*10.

        # re-grid data
        xi, yi = np.mgrid[min(x):max(x):_GridSize*1j, min(y):max(y):_GridSize*1j]
        zi = griddata((x, y), I, (xi, yi), method='linear')

        fig, ax = plt.subplots()
        cf = ax.pcolormesh(xi, yi, zi, vmin= -40., vmax= 0., cmap = plt.get_cmap('rainbow'))
        ax.contour(xi, yi, zi, levels=[-30, -20, -10, -3, -1], linewidths = 0.5, colors = 'k')

        ax.set_xlabel("position [minutes of arc]")
        ax.set_ylabel("position [minutes of arc]")
        ax.set_title ("%s : Field intensity (%s-Beam ) in dB\n(contours are at -1, -3, -10, -20, and -30 dB)" % (freq, beam))
        fig.colorbar(cf)

        ax.set_aspect('equal')

        if figname != "" :
            fig.savefig(figname)
            if show :
                plt.show()
        else :
            plt.show()

        plt.close()


    def plotAllBeamsX             ( self, figname = "", show = False ) :
        """
        Plots all formed beam far-field pattern along the main axis in the f-plane.

        Parameters:
            figname (string):  Filename of the figure (if given figure is only shown on screen if show == True
            show (boolean):    Flag to show plot on screen even when being written to disk (True)
        """
        self.printOut("Plotting the formed beam alog x-axis", self.log.FLOW_TYPE)
        if not _SciPy :
            self.printOut("  SciPy-Lib is not available, can't create the plot!", self.log.WARNING_TYPE, False)
            return

        freq    = "%.2fGHz" % ( int( _C / self.SimWavelength / 1e4 + .5 ) / 100. )

        # extract all points along the x-axis and the maximum offset
        fPoints = []
        maxXp   = 0
        yLimit = 0.01
        while len(fPoints) < 2 :
            for p in self.fovData :
                if p.insideArray and (p.Pos_FPlane[1]**2 <= yLimit) and ( p.Pos_FPlane[0] >= 0. ) :
                    fPoints.append(p)
                    if p.Pos_Sky[0] > maxXp :
                        maxXp = p.Pos_Sky[0]
            yLimit += 1.

        maxXp *= 60.    # convert to minutes of arc

        noPlotsX = 5
        step = 1.*len(fPoints) / noPlotsX
        if step < 1. :
            step = 1.
            noPlotsX = len(fPoints)

        label = True
        fig, ax = plt.subplots(3, noPlotsX) #, figsize = (noPlotsX, 3.5) )
        plotNo = 0
        for i in np.arange(0, len(fPoints), step) :
            idx = int(i)
            p = fPoints[idx]

            for bf in range(3) :
                bw      = [p.BW_CFM, p.BW_MD, p.BW_SNR][bf]
                title   = ["CFM", "M-D", "M-SNR"][bf]
                field = self.simulateBeam(bw)
                # plot the beam
                x = np.zeros(field.points, dtype = float)
                y = np.zeros(field.points, dtype = float)
                I = np.zeros(field.points, dtype = float)
                for idx in range(field.points) :
                    dp = field.Data[idx]
                    x[idx] = dp.v.x *180.*60./(np.pi*_SkyDistance)
                    y[idx] = dp.v.y *180.*60./(np.pi*_SkyDistance)
                    I[idx] = np.absolute(dp.I)*dp.df

                I = 10.*np.log10(I/np.max(I))

                # re-grid data
                xi, yi = np.mgrid[min(x):max(x):_GridSize*1j, min(y):max(y):_GridSize*1j]
                zi = griddata((x, y), I, (xi, yi), method='linear')

                cf = ax[bf,plotNo].pcolormesh(xi, yi, zi, vmin= -40., vmax= 0., cmap = plt.get_cmap('rainbow'))
                ax[bf, plotNo].contour(xi, yi, zi, levels=[-10, -3], linewidths = 0.5, colors = 'k')
                ax[bf, plotNo].plot(p.Pos_Sky[0]*60., 0, 'k+')
                ax[bf, plotNo].set_aspect('equal')
                if bf == 0 :
                    ax[bf, plotNo].set_title ("x = %.1f'" % (p.Pos_Sky[0]*60.) )
                if bf != 2 :
                    ax[bf, plotNo].set_xticklabels([])
                if label :
                    ax[bf, plotNo].set_ylabel(title)
                else :
                    ax[bf, plotNo].set_yticklabels([])

            label = False
            plotNo += 1
            if plotNo >= noPlotsX : break

        fig.suptitle("%s : Field intensity in dB (contours are at -3, -10 dB)\n Cross marks the nominal position" % (freq))
        fig.tight_layout()
        plt.subplots_adjust(top = .85)

        if figname != "" :
            fig.savefig(figname)
            if show :
                plt.show()
        else :
            plt.show()

        plt.close()


    def plotZopt                  ( self, figname = "", show = False, limit = None ) :
        """
        Plot the optimum impednce for all chains.

        Parameters:
            figname (string): Filename of the figure (if given figure is only shown on screen if show == True
            show (boolean):   Flag to show plot on screen even when being written to disk (True)
            limit (boolean):  y-axis limit (if None, automatic determined)
        """
        self.printOut("Plotting the optimum impedance", self.log.FLOW_TYPE)
        freq    = "%.2fGHz" % ( int( _C / self.SimWavelength / 1e4 + .5 ) / 100. )

        elements = range(len(self.Zopt))
        z        = np.zeros(len(self.Zopt), dtype = np.float64)
        for el in elements :
            z[el] = np.real(self.Zopt[el, el])

        fig, ax = plt.subplots()
        ax.plot(elements, z, 'k-')

        if len(limit) > 1 :
            ax.set_ylim(limit[0], limit[1])

        ax.set_xlabel("Receiving chain number")
        ax.set_ylabel("absolute(Optimum impedance) [Ohm]")
        ax.set_title ("%s : Optimum impedance for the receiving chains in the array" % freq)

        if figname != "" :
            fig.savefig(figname)
            if show :
                plt.show()
        else :
            plt.show()

        plt.close()


    #####################################
    # calculation of beam-weights and their characteristics
    def getBW                     ( self, spx, spy) :
        """
        Calculate CFM beam-weight for sky-position (posx, posy).

        Parameters:
            spx (int): On sky position x [mm] at sky distance
            spy (int): On sky position y [mm] at sky distance

        Returns:
            bw (BeamWeightDataClass): Beamweight data.
        """
        if len(self.idxList) == 0 :
            idxList = range(len(self.e_sig))
        else :
            idxList = self.idxList
        # calculate polarization factors for the two field directions
        exFactor = np.cos(np.pi*self.astroSourcePol/180.)
        eyFactor = np.sin(np.pi*self.astroSourcePol/180.)

        # establish beam-weights
        bw            = BeamWeightDataClass.BeamWeights( log = self.log )
        bw.Wavelength = self.SimWavelength                               # add wavelength
        bw.Theta      = np.sqrt(spx**2.+spy**2.)/_SkyDistance/np.pi *180.# add Theta and
        bw.Phi        = np.arctan2(spy, spx)/np.pi*180.                  # Phi angles as position reference

        # calculate closest data point in sky far field
        pos   = np.array([spx, spy, 0])
        d     = (self.v_signal[:] - pos)    # substract skyposition
        d     = np.sum(d * d, axis=1)       # squared distance of all position vectors
        f_idx = np.argmin(d)
        # extract belonging e-fields
        e        = deepcopy(self.e_signal[idxList, f_idx, :])

        # calculate belonging BWs and take out sky distance (fields are lower because of the distance) for normalization
        bw.Data  = np.dot(e[:], np.array([exFactor, eyFactor, 0]))*_SkyDistance

        return bw


    def getBWList                 ( self, center = False, PAF = True, DS_List = [], matching = -1, Dead = [], writeToH5Flag = False) :
        """
        Calculate all beamweights in the FoV of the setup.

        matching strategy :
        > 0 : matching impedance
        0 : using signal chain impedance
        -1 : matched to antenna array (default)
        -2 : matching to external vector is not supported

        Parameters:
            center (boolean):        If true, only bore-sight beam is calculated
            PAF (boolean):           If true, the beams should be determined for the FoV of a PAF (NOTE elaborate)
            DS_List (list):          List of RFI-sources (optional)
            matching (int):          see description above (is there a better solution for long descriptions?)
            Dead (list):             List of dead elements (integers denoting the individual signal chains)
            writeToH5Flag (boolean): If true, ACMs are saved to h5-file

        Returns:
            tuple (tuple): center_idx (integer, index denoting the most central data point in the FoV), minDist
        """

        centerBeamOnlyFlag = center
        pafBeamsFlag = PAF
        deadElementList = Dead
        rfiSourceList = DS_List

        self.printOut("Calculating all Beam-weights within the possible FoV", self.log.FLOW_TYPE)
        if matching < -1 :
            matching = -1
            self.printOut("  matching to external vector is not supported so far!", self.log.WARNING_TYPE, False)

        self.printOut("  using matching : %d"    % matching,     self.log.DATA_TYPE, False)
        self.printOut("  using %d RFI sources"   % len(DS_List), self.log.DATA_TYPE, False)
        self.printOut("  using %d dead elements" % len(Dead),    self.log.DATA_TYPE, False)


        posList = self.determineBeamPositions(centerBeamOnlyFlag, pafBeamsFlag)


        # Making a list of good antenna elements
        validIdx             = []
        for idx in range(len(self.e_signal)):
            if not idx in deadElementList:
                validIdx.append(idx)

        self.idxList         = np.asarray(validIdx)


        # delete all old data
        self.fovData      = []
        self.Rsig_cfm     = []
        self.Rsig_md      = []
        self.Rsig_snr     = []


        center_idx, minDist = self.determineBeamweights(posList, rfiSourceList, matching, writeToH5Flag)

        # check if beams are inside or outside the convex hull of the array
        inList = self.in_Array(posList)
        for p_idx, p in enumerate(self.fovData) :
            p.insideArray = inList[p_idx]


        # calculate signal respone matrices for effective number of beams and so on
        self.calculateFoVSignalRespose()

        return center_idx, minDist


    def determineBeamPositions(self, centerBeamOnlyFlag, pafBeamsFlag):
        """
        Determine a list of positions (on sky) of all desired beams.

        The input parameters determine which kind of beam positions are calculated:
            Just the bore-sight beam at position 0,0.
            All possible positions within the FoV, which is nyquist sampled.
            The beams of the individual antennas as projected onto the sky.

        Parameters:
            centerBeamOnlyFlag (boolean): If true, only use the bore-sight beam
            pafBeamsFlag (boolean): If true, all possible beam position in the FoV of a PAF should be determined

        Returns:
            posList (np.array): A list of position for the beams in XY coordinates
        """

        # check if more positions than center are required and establish position-list
        if centerBeamOnlyFlag :
            posList = np.asarray( [[0, 0]] )
            self.printOut("  calculating only the bore-sight beam", self.log.DATA_TYPE, False)
        elif pafBeamsFlag :
            # calculate FoV-step-size, and list of positions to loop
            fovRadius            = np.sqrt( np.amax( self.e_pos[:, 0]**2 + self.e_pos[:, 1]**2 ) ) * _FOV_SIZE_FACTOR    # calculate size of the FoV
            telescopeDiameter    = np.sqrt( self.Asky.Aeff / np.pi * 4. )                                                # Telescope diameter not known by FoVDataClass, therefore calculated.
            hpbw                 = _SkyDistance * self.SimWavelength / telescopeDiameter                                 # calculate HPBW
            steps = int( fovRadius / hpbw * 2. )                                                                         # get number of steps

            pos     = np.arange(-steps, steps+1, 1)
            posList = []
            for x in pos :
                for y in pos :
                    posList.append( [x,y] )

            # establish positions
            posList = np.asarray( posList ) * hpbw / 2.
            self.printOut("  Calculating all PAF beams in the FoV", self.log.DATA_TYPE, False)
            self.printOut("  HPBW                   = %.2f '" % ( hpbw * 180. * 60. / np.pi / _SkyDistance )     , self.log.DATA_TYPE, False)
            self.printOut("  FoV-Radius (oversized) = %.2f '" % ( fovRadius / _SkyDistance / np.pi * 180. * 60. ), self.log.DATA_TYPE, False)
        else :
            posList = self.e_pos
            self.printOut("  Calculating discrete beams at positions of the existing horns", self.log.DATA_TYPE, False)

        return posList


    def determineBeamweights(self, posList, rfiSourceList, matching, writeToH5Flag):
        """
        Determines the beamweights at desired positions on sky.

        Parameters:
            posList (list):          List of coordinates on sky, that beams should point to
            rfiSourceList (list):    List of RFI sources in the form of FoVDataPoint-objects
            matching (int):          Matching strategy
            writeToH5Flag (boolean): If true, ACMs are saved to h5-file

        Returns:
            tuple (tuple): center_idx (integer, index denoting the most central data point in the FoV), minDist
        """
        minDistFromCenter = _SkyDistance*1000   # set the minimum distance from bore-sight to a very high value
        self.printOut("  Looping over all beam positions:", self.log.DEBUG_TYPE, False)

        for posIdx, pos in enumerate(posList) :
            p            = FoVDataPoint( log = self.log )
            # calculate on sky position in degree
            p.Pos_Sky    = np.asarray( [ pos[0] / _SkyDistance / np.pi * 180., pos[1] / _SkyDistance / np.pi * 180.] )
            # calculate Focal plane position in mm
            p.Pos_FPlane = p.Pos_Sky / 180. * np.pi * self.telescopeF
            # calculate CFM-beamforming weights from fields and therewith the source direction vector without RFI
            p.BW_CFM     = self.getBW( pos[0], pos[1] )

            if posIdx == 0 :                                                     # check if noise resistant matrices need to be set
                # first position to establish the noise resistance matrices
                ds_list          = deepcopy(rfiSourceList)                       # copy list of RFI sources
                ds_list.append     (p.BW_CFM)                                    # and append calculated CFM beam-weights
                # calculate noise resistant matrices using the


                # Re-match all matrices for the actual active elements
                Asky                 = deepcopy( self.Asky )
                Asky.Data            = deepcopy( self.Asky.Data   [self.idxList, :][:, self.idxList] )
                Asky.noElements      = len( self.idxList )

                Aspill               = []
                AspillT              = []
                for A_AT_List in self.ALosses:

                    Aset                = deepcopy( A_AT_List[0] )
                    Aset.Data           = Aset.Data [self.idxList, :][:, self.idxList]
                    Aset.noElements     = len( self.idxList )
                    Aspill.append         ( deepcopy( Aset ) )

                    Aset                = deepcopy( A_AT_List[1] )
                    Aset.Data           = Aset.Data [self.idxList, :][:, self.idxList]
                    Aset.noElements     = len( self.idxList )
                    AspillT.append        ( deepcopy( Aset ) )

                signalChainData      = deepcopy( self.signalChainData )
                signalChainData.Yc   = signalChainData.Yc  [self.idxList, :][:, self.idxList]
                signalChainData.Zopt = signalChainData.Zopt[self.idxList, :][:, self.idxList]
                signalChainData.VR2  = signalChainData.VR2 [self.idxList, :][:, self.idxList]
                signalChainData.ZS   = signalChainData.ZS  [self.idxList, :][:, self.idxList]


                self.Zopt, Match = self.noise.calculateNoiseResistances( Asky, Aspill, AspillT, ds_list, signalChainData, Impedance = matching, Iv = [] )
            else :                                                           # if not first,
                ds_list[-1]      = p.BW_CFM                                      # only set actual calculated CFM-beamweights
                self.noise.setSignal(ds_list)                                    # and re-calculate signal resistance matrix

            # calculate all beam-weights under the presents of RFI
            p.BW_CFM = self.noise.getBeamWeights(alg="CFM")                  # conjugate field match is not changing, but re-calculated for completeness here, can be removed also
            p.BW_MD  = self.noise.getBeamWeights(alg="MD")                   # also not affected by RFI. Only using total noise resistance and signal direction
            if len(rfiSourceList) > 0 :                                            # use full lin. equation solving if RFI is present (required because standard algorithm only uses signal direction and noise
                p.BW_SNR = self.noise.getBeamWeights(alg="RFI")                  # include RFI suppression
            else :
                p.BW_SNR = self.noise.getBeamWeights(alg="SNR")                  # faster but no RFI influence

            self.fovData.append(p)

            # determine the beam that is closest to the bore-sight
            distFromCenter = pos[0]*pos[0] + pos[1]*pos[1]
            if distFromCenter < minDistFromCenter:
                minDistFromCenter = distFromCenter
                center_idx = len(self.fovData)-1


            if writeToH5Flag and distFromCenter == 0:   # NOTE Temporary solution. Only works if there is a boresight beam since distFromCenter must be 0.
                frequency = np.round(_C / self.SimWavelength * 1e-6, 2)
                if len(rfiSourceList) > 0:
                    self.dataWriter.writeACM("rfi", self.noise.RT["signal"], frequency, np.asarray([np.round(rfiSourceList[0].Phi, 3), np.round(rfiSourceList[0].Theta,3)])) # NOTE currently missing RFI polarization
                else:
                    for acmKey, acmValue in self.noise.RT.items():
                        self.dataWriter.writeACM(acmKey, acmValue, frequency, pos, np.int16(self.astroSourcePol))

        self.printOut("", self.log.DEBUG_TYPE, False)

        minDist = np.sqrt(minDistFromCenter)/_SkyDistance/np.pi*180

        return center_idx, minDist


    def simulateBeam              ( self, bw) :
        """
        Simulate the far-field pattern of the beam formed by the beamweights bw.

        Parameters:
            bw (list?): beam-weights to be simulated

        Returns:
            field (UFODataClass): with the simulated beam data
        """
        if len(self.idxList) == 0 :
            idxList = range(len(self.e_sig))
        else :
            idxList = self.idxList

        self.printOut("Simulating a beam :", self.log.FLOW_TYPE)

        field = deepcopy(self.ufoTemplate)
        field.log = self.log

        for idx in range(field.points) :
            E_                 = np.dot(self.e_signal[idxList, idx, :].T, bw.Data)/_SkyDistance
            field.Data[idx].Er = UFODataClass._UFOVector(E_[0].real, E_[1].real, E_[2].real)
            field.Data[idx].Ei = UFODataClass._UFOVector(E_[0].imag, E_[1].imag, E_[2].imag)
            field.Data[idx].I  = np.dot(E_, E_.conj()).real
        return field


    #####################################
    # calculate FoV characteristics
    def calculateSSFoV            ( self, effLimit = np.sqrt(0.5)) :
        """
        Calculate figures of merit for the array sensitivity and the mapping speed. The calculation goes along
        "Performance of a Highly Sensitive, 19-element, Dual-polarization, Cryogenic L-band Phased-array Feed on the Green Bank Telescope".
        D. Anish Roshi et al., https://doi.org/10.3847/1538-3881/aab965
        SSFOM : survey-speed figure of merit
        SSFoV : survey-speed weighted field of view

        Parameters:
            effLimit (float): Giving the relative apperture efficiency compared to the maximum within the formed beams considered for a "usable" formed beam. The default value of sqrt(0.5) gives half of the mapping performance of the beam

        Returns:
            tuple (tuple): Beamforming data for the three figures of merit and number of beams.
        """
        self.printOut("Calculate mapping speed figure of merit", self.log.FLOW_TYPE)
        # first calculate signal response matrix, mainly to get maximum efficiencies correct.
        self.calculateFoVSignalRespose()
        # check number of formed beams available
        if len(self.idxList) == 0 :
            idxList = range(len(self.e_sig))
        else :
            idxList = self.idxList

        telescopeDiameter    = np.sqrt( self.Asky.Aeff / np.pi * 4. ) 

        # initialize variables
        hpbw       = self.SimWavelength/telescopeDiameter                           # calculate HPBW
        hpbw       = hpbw/np.pi*180.                                                #   and transform it to degree
        dOmega     = hpbw**2./4.                                                    # Nyquist sampling forward angle spacing = hpbw/2
        A          = telescopeDiameter**2. / 4. * np.pi / 1.e6                      # telescope area in m^2
        SSFoMcfm   = 0.                                                             # survey-speed figure of merit for CFM beamforming
        SSFoMmd    = 0.                                                             # survey-speed figure of merit for MD beamforming
        SSFoMsnr   = 0.                                                             # survey-speed figure of merit for SNR beamforming
        minTcfm    = 1.e10                                                          # minimum Tsys over eta for cfm beamforming
        minTmd     = 1.e10                                                          # minimum Tsys over eta for md beamforming
        minTsnr    = 1.e10                                                          # minimum Tsys over eta for snr beamforming
        sumCFM     = 0.                                                             # integral over all beam2beam correlations for cfm
        sumMD      = 0.                                                             # integral over all beam2beam correlations for md
        sumSNR     = 0.                                                             # integral over all beam2beam correlations for snr
        noBeam_cfm = 0.                                                             # number of 'good' beams for cfm beamforming
        noBeam_md  = 0.                                                             # number of 'good' beams for md beamforming
        noBeam_snr = 0.                                                             # number of 'good' beams for snr beamforming
        no_beams   = 0.                                                             # total number of beams inside the projected convex hul 
        
        for p in self.fovData :                                                     # loop all points in the FoV
            if p.insideArray :                                                        # check if the beam is inside the projected convex hul
                no_beams += 1                                                           # increas number of beams available
                SSFoMcfm  += dOmega * A**2. / (p.BW_CFM.Noise.T_sys_oe**2.)               # integrate for SSFoM
                SSFoMmd   += dOmega * A**2. / (p.BW_MD.Noise.T_sys_oe**2. )
                SSFoMsnr  += dOmega * A**2. / (p.BW_SNR.Noise.T_sys_oe**2.)

                if p.BW_CFM.Noise.T_sys_oe < minTcfm :                                    # find best Tsys/eta of all beams
                    minTcfm = p.BW_CFM.Noise.T_sys_oe
                if p.BW_MD.Noise.T_sys_oe  < minTmd  : 
                    minTmd  = p.BW_MD.Noise.T_sys_oe
                if p.BW_SNR.Noise.T_sys_oe < minTsnr : 
                    minTsnr = p.BW_SNR.Noise.T_sys_oe
                
                wcfm_n     = np.dot( self.noise.RT['sys'], p.BW_CFM.Data )                   # get normalized noise response for CFM
                wcfm_n    /= np.linalg.norm(wcfm_n)
                
                wmd_n      = np.dot( self.noise.RT['sys'], p.BW_MD.Data )                    # get normalized noise response for MD
                wmd_n     /= np.linalg.norm(wmd_n)
                
                wsnr_n     = np.dot( self.noise.RT['sys'], p.BW_SNR.Data )                   # get normalized noise response for SNR
                wsnr_n    /= np.linalg.norm(wsnr_n)

                # get beams within selected efficiency regime
                if p.BW_CFM.Efficiency.Eta_a > self.max_EtaA_CFM * effLimit:
                    noBeam_cfm     += 1.
                
                if p.BW_MD.Efficiency.Eta_a  > self.max_EtaA_MD  * effLimit:
                    noBeam_md      += 1.
                    
                if p.BW_SNR.Efficiency.Eta_a > self.max_EtaA_SNR * effLimit:
                    noBeam_snr     += 1.

                for p2 in self.fovData :                                                      # loop all points in the FoV
                    if p2.insideArray :
                        wcfm_n2    = np.dot( self.noise.RT['sys'], p2.BW_CFM.Data )                # get normalized noise response for CFM
                        wcfm_n2   /= np.linalg.norm(wcfm_n2)
                        sumCFM    += np.dot(wcfm_n.conj().T, wcfm_n2)                             # and calculate overlap

                        wmd_n2     = np.dot( self.noise.RT['sys'], p2.BW_MD.Data )
                        wmd_n2    /= np.linalg.norm(wmd_n2)
                        sumMD     += np.dot(wmd_n.conj().T,  wmd_n2) 

                        wsnr_n2    = np.dot( self.noise.RT['sys'], p2.BW_SNR.Data )
                        wsnr_n2   /= np.linalg.norm(wsnr_n2)
                        sumSNR    += np.dot(wsnr_n.conj().T, wsnr_n2) 

        SSFoVcfm = SSFoMcfm * minTcfm**2. / dOmega / 4. / A**2.                         # calculate SSFoV for all beam-former
        SSFoVmd  = SSFoMmd  * minTmd**2.  / dOmega / 4. / A**2.
        SSFoVsnr = SSFoMsnr * minTsnr**2. / dOmega / 4. / A**2.

        Neff_cfm = len(idxList)**2. / np.absolute(sumCFM)                               # calculate effective number of beams for all beam-former
        Neff_md  = len(idxList)**2. / np.absolute(sumMD)
        Neff_snr = len(idxList)**2. / np.absolute(sumSNR)

        self.printOut("  Number of beams inside array  : %.1f" % no_beams,   self.log.DEBUG_TYPE, False)    # debug statements
        self.printOut("  Number of good beams cfm      : %.1f" % noBeam_cfm, self.log.DEBUG_TYPE, False)
        self.printOut("  Number of good beams md       : %.1f" % noBeam_md , self.log.DEBUG_TYPE, False)
        self.printOut("  Number of good beams snr      : %.1f" % noBeam_snr, self.log.DEBUG_TYPE, False)
        self.printOut("  effective number of beams cfm : %.1f" % Neff_cfm  , self.log.DEBUG_TYPE, False)
        self.printOut("  effective number of beams md  : %.1f" % Neff_md   , self.log.DEBUG_TYPE, False)
        self.printOut("  effective number of beams snr : %.1f" % Neff_snr  , self.log.DEBUG_TYPE, False)
        self.printOut("  SSFoM cfm                     : %.1f" % SSFoMcfm  , self.log.DEBUG_TYPE, False)
        self.printOut("  SSFoM md                      : %.1f" % SSFoMmd   , self.log.DEBUG_TYPE, False)
        self.printOut("  SSFoM snr                     : %.1f" % SSFoMsnr  , self.log.DEBUG_TYPE, False)
        self.printOut("  HPBW (deg)                    : %.5f" % hpbw      , self.log.DEBUG_TYPE, False)
        self.printOut("  dOmega (deg^2)                : %.5f" % dOmega    , self.log.DEBUG_TYPE, False)

        return  [SSFoMcfm, noBeam_cfm, Neff_cfm, SSFoVcfm], \
                [SSFoMmd,  noBeam_md,  Neff_md,  SSFoVmd],  \
                [SSFoMsnr, noBeam_snr, Neff_snr, SSFoVsnr], \
                no_beams


    def calculateFoVSignalRespose ( self ) :
        """
        Calculate the signal response matrix sum over all beam-weight vector outer products.
        
        The main axis of the resulting matrix informs about the importance of the individual array-elements.
        From the matrix as a whole one can determine the information contents of the FoV.
        """
        if len(self.idxList) == 0 :
            idxList = range(len(self.e_sig))
        else :
            idxList = self.idxList    
            
        self.printOut("Calculating beam-weight response matrices over the FoV :", self.log.DEBUG_TYPE)
        # calculte signal response matrices for all beam-weights
        self.Rsig_cfm      = np.zeros((len(idxList), len(idxList)), dtype = np.complex128)
        self.Rsig_md       = np.zeros((len(idxList), len(idxList)), dtype = np.complex128)
        self.Rsig_snr      = np.zeros((len(idxList), len(idxList)), dtype = np.complex128)
        self.max_EtaA_CFM  = 0.
        self.max_EtaA_MD   = 0.
        self.max_EtaA_SNR  = 0.
        
        for p in self.fovData :                                                      # loop all points in the FoV
            if p.insideArray :
                
                w_cfm          = p.BW_CFM.Data 
                w_cfm          = w_cfm / np.linalg.norm(w_cfm)
                w_md           = p.BW_MD.Data 
                w_md           = w_md  / np.linalg.norm(w_md )
                w_snr          = p.BW_SNR.Data 
                w_snr          = w_snr / np.linalg.norm(w_snr)
                # add-up outer products of the normalized vectors
                self.Rsig_cfm +=  np.outer(w_cfm.conj().T, w_cfm)
                self.Rsig_md  +=  np.outer(w_md.conj().T , w_md )
                self.Rsig_snr +=  np.outer(w_snr.conj().T, w_snr)
                
            # extract maximum aperture efficiency
            if p.BW_CFM.Efficiency.Eta_a > self.max_EtaA_CFM : self.max_EtaA_CFM = p.BW_CFM.Efficiency.Eta_a
            if p.BW_MD.Efficiency.Eta_a  > self.max_EtaA_MD  : self.max_EtaA_MD  = p.BW_MD.Efficiency.Eta_a
            if p.BW_SNR.Efficiency.Eta_a > self.max_EtaA_SNR : self.max_EtaA_SNR = p.BW_SNR.Efficiency.Eta_a


    def calcEffectiveNumberOfBeams( self, effLimit = np.sqrt(0.5) ) :
        """
        Calculate effective number of beams (not the official definition, using signal correlation instead of noise correlation.
        
        Each beamformer data list has the following content:
            Effective number of beams in the FoV,
            number of beams formed withinthe selected efficiency regime,
            Effective number of beams in the selected efficiency regime

        Parameters:
            effLimit (float): limit for the aperture efficiency to calculate number of beams in the usable FoV (fraction of best aperture) -- default: 50% of mapping speed compared to best pixel

        Returns:
            tuple (tuple): Beamforming data of three figures of merit and mapping speeds.
        """
        
        self.printOut("Calculating effective number of beams :", self.log.FLOW_TYPE)
        # update signal response matrices
        self.calculateFoVSignalRespose()
        
        noBeamAll_cfm  = 0.             # effective number of all beams
        noBeamAll_md   = 0.
        noBeamAll_snr  = 0.
        noBeamGood_cfm = 0.             # effective number of beams within the selected efficiency regime
        noBeamGood_md  = 0.
        noBeamGood_snr = 0.
        noBeam_cfm     = 0              # absolute numer of formed beams within the selected efficiency regime
        noBeam_md      = 0
        noBeam_snr     = 0
        mapSpeed_cfm   = 0.
        mapSpeed_md    = 0.
        mapSpeed_snr   = 0.
        
        for p in self.fovData :                             # loop all points in the FoV
            if p.insideArray :
                # normalize BW-vectors to sqrt(Aperture efficiency)
                vsig           = p.BW_CFM.Data / np.linalg.norm(p.BW_CFM.Data)       # signal vector = cfm BW
                Vsig           = np.outer(vsig.conj().T, vsig)                       # outer product of the vector

                w_cfm          = vsig
                w_md           = np.dot(Vsig, p.BW_MD.Data.conj().T)
                w_md           = w_md / np.linalg.norm(w_md)
                w_snr          = np.dot(Vsig, p.BW_SNR.Data.conj().T)
                w_snr          = w_snr / np.linalg.norm(w_snr)
                
                # get signal respones from signal response matrices
                beamEff_cfm    = p.BW_CFM.Efficiency.Eta_a / np.absolute(np.dot(w_cfm, np.dot(self.Rsig_cfm, np.conj(w_cfm).T) ) )
                beamEff_md     = p.BW_MD.Efficiency.Eta_a  / np.absolute(np.dot(w_md,  np.dot(self.Rsig_md,  np.conj(w_md ).T) ) )
                beamEff_snr    = p.BW_SNR.Efficiency.Eta_a / np.absolute(np.dot(w_snr, np.dot(self.Rsig_snr, np.conj(w_snr).T) ) )
                
                noBeamAll_cfm += beamEff_cfm
                noBeamAll_md  += beamEff_md
                noBeamAll_snr += beamEff_snr
                
                # get beams within selected efficiency regime
                if p.BW_CFM.Efficiency.Eta_a > self.max_EtaA_CFM * effLimit:
                    noBeam_cfm     += 1
                    noBeamGood_cfm += beamEff_cfm
                    mapSpeed_cfm   += beamEff_cfm * p.BW_CFM.Efficiency.Eta_a / (p.BW_CFM.Noise.T_sys_oe**2.)
                
                if p.BW_MD.Efficiency.Eta_a  > self.max_EtaA_MD  * effLimit:
                    noBeam_md      += 1
                    noBeamGood_md  += beamEff_md    
                    mapSpeed_md    += beamEff_md  * p.BW_MD.Efficiency.Eta_a  / (p.BW_MD.Noise.T_sys_oe**2. )
                    
                if p.BW_SNR.Efficiency.Eta_a > self.max_EtaA_SNR * effLimit:
                    noBeam_snr     += 1
                    noBeamGood_snr += beamEff_snr
                    mapSpeed_snr   += beamEff_snr * p.BW_SNR.Efficiency.Eta_a / (p.BW_SNR.Noise.T_sys_oe**2.)
        
        return  [noBeamAll_cfm, noBeam_cfm,  noBeamGood_cfm], \
                [noBeamAll_md , noBeam_md ,  noBeamGood_md ], \
                [noBeamAll_snr, noBeam_snr,  noBeamGood_snr], \
                [mapSpeed_cfm,  mapSpeed_md, mapSpeed_snr  ]


    def RFI_test                  ( self, matching = -1, numberOfSimulations = 10, maxRFISources = -1, ResultPath = "./" ) :
        """
        Tests the noise performance of the bore-sight beam formed under RFI sources.
        The bore-sight beam is plotted, a video of all plots is created and a plot of Tsys/Ets vs. number of RFI sources is made.

        matching strategy:
            > 0 : matching impedance
            0 : using signal chain impedance
            -1 : matched to antenna array (default)
            -2 : matching to external vector is not supported

        Parameters:
            matching (int):             matching strategy
            numberOfSimulations (int):  number of RFI-source simulations
            maxRFISources (int):        maximum number of RFI-sources used (if negative, number of available array chains is used
            ResultPath (string):        Path to write all plots to
        """
        self.printOut("Testing RFI-implications", self.log.FLOW_TYPE)
        
        if maxRFISources < 0 :
            maxRFISources = len(self.e_pos)
            
        #calculate 15 RFI-loads, number of RFI sources for each step depends on number of elements
        step           = int( maxRFISources / numberOfSimulations + 1 )
        RFI_List       = []                   # list of RFI sources
        
        BWs            = []                   # list of normalized Beam-weights for the number of RFI sources
        Tsys           = []                   # list of Tsys/eta values for the number of RFI-sources
        RFISources     = []                   # number of RFI sources belonging to this value
        matrix         = []                   # amplitudes of the beam-weights
        
        baseName       = str(ResultPath) + "/RFI_Beam"
        
        if len(self.Losses[0]["v"]) == 0 : return

        for r in range( 0, maxRFISources, step ) :  # increase number of RFI-sources per loop

            self.printOut("  check %d RFI-sources" % r, self.log.FLOW_TYPE, False)

            # create new RFI sources
            idx     = int(np.random.random()*len(self.Losses[0]["v"]))  # get source direction from random spill-over point
            v       = self.Losses[0]["v"][idx,:]                   # get position point outside the dish

            pol     = np.random.random()*90                        # set random polarization [degree]
            phase   = np.random.random()*2.*np.pi                  # set random phase [radian]
            # set random amplitude
            amp     = np.random.random()*10.**(_RFI_MAX_LEVEL/10.) # set random amplitude along maximum level
            amp    *= _SkyDistance                                 # and compensate for sky distance
                                                                   # to be comparable with the astronomical source
            # create beam-weights for this position
            bw            = BeamWeightDataClass.BeamWeights( log = self.log ) # empty beam-weight data class
            # calculate Theta and Phy from position index
            bw.Phi        = np.arctan2(v[1], v[0])*180./np.pi
            bw.Theta      = np.arctan2(np.sqrt(v[0]**2 + v[1]**2), np.absolute(v[2]))*180/np.pi

            bw.Wavelength = self.SimWavelength
            # calculate belonging BW-data
            e             = deepcopy(self.Losses[0]["spillT"][:, idx, :])  # extract e-fields belonging to the random indext
            exFactor      = np.cos(np.pi*pol/180.)*np.cos(np.pi*bw.Theta/180.)
            eyFactor      = np.sin(np.pi*pol/180.)*np.cos(np.pi*bw.Theta/180.)
            ezFactor      = np.sin(np.pi*bw.Theta/180.)*np.cos(np.pi*(bw.Phi-pol)/180.)
            ## do beam-forming (CFM)
            bw.Data  = np.dot(e[:], np.array([exFactor, eyFactor, ezFactor]))*amp*np.exp(1j*phase)
            RFI_List.append(bw)


            # calculate performance
            centeridx, mindist = self.getBWList ( center = True, DS_List = RFI_List, matching = matching)
            name = baseName + "%04d.png" % r
            # plot SNR-beam on sky
            self.plotCenterBeam( beamforming = 2, figname = name)
            BWs.append(self.fovData[centeridx].BW_SNR)

            Tsys.append(BWs[-1].Noise.T_sys_oe)
            matrix.append(np.absolute(np.asarray(BWs[-1].Data)))

            RFISources.append(r)
            self.getBWList ( center = True, DS_List = [RFI_List[-1]], matching = matching, writeToH5Flag = True)    # Rerun to get ACM of only one RFI source

        self.printOut("", self.log.FLOW_TYPE, False)
        
        duration = 10.
        if len(RFI_List) < 10. : duration = len(RFI_List)

        makeMovie(namePattern = (baseName + "0*.png"), name = (baseName+".mov"), duration = duration)   # make RFI-movie
        # plot Tsys data
        fig, ax = plt.subplots()
        ax.plot(RFISources, Tsys, 'k-')
        ax.set_xlabel("Number of RFI-sources")
        ax.set_ylabel("Tsys/Eta [K]")
        ax.set_title ("Tsys/Eta versus number of RFI sources from spill-over region") 
        fig.savefig(baseName + "_Tsys.png")
        plt.close()
        # plot beam-weight amplitudes
        fig, ax = plt.subplots()
        matrix  = np.asarray(matrix).T
        matrix  = matrix / np.max(matrix)
        cf      = ax.matshow(matrix, cmap = plt.get_cmap('rainbow'), aspect = 'auto', vmin = 0.0, vmax = 1.0)
        ax.set_xlabel("Number of RFI-sources")
        ax.set_ylabel("relative beam-weight-amplitude")
        ax.set_title ("RFI affecting beam-weights") 
        fig.colorbar(cf)
        fig.savefig(baseName + "_BWs.png")
        plt.close()


    def deadElementTest ( self, matching = -1, maxDeadElements = -1, numberOfSets = 10., maxLoops = -1, ResultPath = "./" ):
        """
        Check statistically for dead elements.

        Parameters:
            matching (int):         matching strategy
            maxDeadElements (int):  maximum number of dead elements (if negative, number of available chains/2 is used)
            numberOfSets (int):     number of sets of dead elements
            maxLoops (int):         number of tries for the statistics (if negative, number of available chains/2 is used)
            ResultPath (string):    Path to write all plots to
        """
        self.printOut("Performing dead element-test on the array", self.log.FLOW_TYPE)
        
        # set basic parameters
        if maxDeadElements < 0:
            maxDeadElements = int( len(self.e_pos) / 2 )
        if maxLoops < 0 :
            maxLoops = len(self.e_pos) / 2
            
        step    = int( maxDeadElements / numberOfSets + 1 )                # use different numbers of dead elements
        results = []
        noBeams = []
        ssfom   = []
        deadEs  = []
        for noDead in range(0, maxDeadElements, step) :                    # increase number of dead elements per loop
            results.append([])
            noBeams.append([])
            deadEs.append(noDead)
            ssfom.append([])
            if noDead == 0: loops = 2
            else          : loops = int( maxLoops / noDead )
            
            self.printOut("  Dead element test for %d dead elements using %d loops)" %(noDead, loops), self.log.DEBUG_TYPE, False, False)
            
            for l in range(loops) :                               # loop all loops
                # select dead elements
                Dead = []
                for i in range(noDead):                             # select noDead random elements (without overlap)
                    while True :
                        d = int( np.random.random() * len(self.e_pos) )
                        if d not in Dead :
                            break
                    Dead.append(d)
                
                centeridx, mind = self.getBWList ( center = False, DS_List = [], matching = matching, Dead = Dead)
                results[-1].append( self.fovData[centeridx].BW_SNR.Noise.T_sys_oe )
                noBeams[-1].append( self.calcEffectiveNumberOfBeams()[2][2] )
                ssfom[-1].append  ( self.calculateSSFoV()[2][0] )
        
        self.printOut("", self.log.DEBUG_TYPE, False )
        maxBeams = np.zeros(len(noBeams))
        minBeams = np.zeros(len(noBeams))
        avBeams  = np.zeros(len(noBeams))
        for idx, nb in enumerate(noBeams) :
            nb            = np.asarray(nb)
            maxBeams[idx] = np.amax   (nb)
            minBeams[idx] = np.amin   (nb)
            avBeams [idx] = np.average(nb)
            
        maxTsys  = np.zeros(len(results))
        minTsys  = np.zeros(len(results))
        avTsys   = np.zeros(len(results))
        for idx, r in enumerate(results) :
            r        = np.asarray( r )
            maxTsys[idx] = np.amax   ( r )
            minTsys[idx] = np.amin   ( r )
            avTsys [idx] = np.average( r )
        
        maxSSFoM  = np.zeros(len(results))
        minSSFoM  = np.zeros(len(results))
        avSSFoM   = np.zeros(len(results))
        for idx, s in enumerate(ssfom) :
            s        = np.asarray( s )
            maxSSFoM[idx] = np.amax   ( s )
            minSSFoM[idx] = np.amin   ( s )
            avSSFoM [idx] = np.average( s )
            
        # plot Tsys data
        self.printOut("  Ploting data", self.log.DEBUG_TYPE, False)
        fig, ax = plt.subplots()
        ax.plot(deadEs, avTsys,  'k-' )
        ax.plot(deadEs, minTsys, 'k-.')
        ax.plot(deadEs, maxTsys, 'k--')
        
        ax.set_xlabel("Number of dead elements")
        ax.set_ylabel("Tsys/Eta [K]")
        ax.set_title ("Tsys/Eta versus number of dead elements\n(Maximum, minimum and average of %d tests)" % maxLoops) 
        fig.savefig(Path(ResultPath, "Dead_Elements.png"))
        plt.close()
        
        # plot number of beam data
        fig, ax = plt.subplots()
        ax.plot(deadEs, avBeams,  'k-' )
        ax.plot(deadEs, minBeams, 'k-.')
        ax.plot(deadEs, maxBeams, 'k--')
        
        ax.set_xlabel("Number of dead elements")
        ax.set_ylabel("Effective number of formed beams [K]")
        ax.set_title ("Effective number of formed beams versus number of dead elements\n(Maximum, minimum and average of %d tests)" % maxLoops) 
        fig.savefig(Path(ResultPath, "Dead_Elements_Beams.png"))
        plt.close()
        
        # plot figure of merit data
        fig, ax = plt.subplots()
        ax.plot(deadEs, avSSFoM,  'k-' )
        ax.plot(deadEs, minSSFoM, 'k-.')
        ax.plot(deadEs, maxSSFoM, 'k--')
        
        ax.set_xlabel("Number of dead elements")
        ax.set_ylabel("Survey-speed figure of merit")
        ax.set_title ("Survey-speed versus number of dead elements\n(Maximum, minimum and average of %d tests)" % maxLoops) 
        fig.savefig(Path(ResultPath, "Dead_Elements_SSFoM.png"))
        plt.close()
        
#############################################################################
# Main function;
#
#############################################################################
if __name__=="__main__":
   pass
# end
