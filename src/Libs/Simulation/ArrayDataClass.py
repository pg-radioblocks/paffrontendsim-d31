#!/usr/bin/env python3
"""
This file is part of the PAFFrontendSim.

The PAFFrontendSim is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License v3 as published by the Free Software Foundation.

The PAFFrontendSim is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with the PAFFrontendSim. If not, see https://www.gnu.org/licenses/.
"""
#############################################################################
# import libs
#############################################################################
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.mlab   as ml

try:
    from   scipy.interpolate               import griddata
    _SciPy = True
except:
    _SciPy = False

from   copy                            import copy, deepcopy


import LogClass
from FileIO                         import HDF5Writer
from Simulation                     import DistributionDataClass
from Simulation.Plots               import ArrayPlotClass
from Simulation                     import FoVDataClass

from Simulation.constants           import _Tgrd, _Tskatter, _Tsky, _Tatm, _Tcbg
from Simulation.constants           import _I0iso, _Impedance, _Bandwidth, _BaseAmp, _SkyDistance
from Simulation.constants           import _C, _kB, _ETA
from Simulation.constants           import _GridSize

from Simulation.BeamWeightDataClass import _Types as _BW_Types
from Simulation.UFO                 import getAngles

#############################################################################
# constants
#############################################################################
_CLASSNAME          = "ArrayData"

_ImageWidth         = "7.5cm"                   # latex image width
_MovieDuration      = 8                         # length of a movie in seconds

#############################################################################
# classes
#############################################################################
class ChainData :
    """
    Class with the signal chain data (usually LNA) belonging to one element.
    A signal chain performs the initial analog processing of the electical signal coming from the antenna.
    """
    # init method
    def __init__   ( self, sc, ff, pol = 0, phase = 0.) :
        # no individual amplitude given. Amplitude scaling is done via _BaseAmp and the loss. 
        # Otherwise we would need an individual current for each entry in the correlation matrix.
        # At the moment all exitation currents are set to an equale value throughout the array
        
        self.signalChain  = sc        # signalchaindata class used by the antenna (only signal chain of first element is used so far!
        self.farfield     = ff        # farfield data class of the antenna  # NOTE Farfield could be stored in ElementData instead.
        self.polarization = 1.*pol    # polarization vector (relative to the polarization of ff
        self.phase        = 1.*phase  # phase factor
        self.loss         = 0.        # loss of a circuit between antenna and first amplifier

class ElementData:
    """
    Class to handle the position of an element (antenna) on the PAFs plane.
    It also stores the signal chains associated with the element.
    """
    # init method
    def __init__   ( self ) :
        self.posx     = 0.     # x-position [mm] within the rx-focal plane
        self.posy     = 0.     # y-position [mm] within the rx-focal plane
        self.chains   = []     # List of receiver chains assosiated with this element
        self.noChains = 0      # number of signal chains within this element

    def setPos     ( self, x = 0, y = 0 ) :
        self.posx = x
        self.posy = y
    
    def getPos     ( self ) :
        return self.posx, self.posy

    def setChain   ( self, chain = None ) :
        if chain != None :
            self.chains.append(chain)
            self.noChains = len(self.chains)

class ArrayData :
    """
    This module represents the PAF.
    It is made up of elements (antennas) on a 2D plane (coinciding with the focal plane of the telescope).
    It also contains a loop for a full physical optics simulation of the entire PAF.

    Nested within ArrayData (PAF) are objects of ElementData (elements/antennas).
    And nested within the objects of ElementData is at least one object of ChainData (signal chain / LNA).

    """
    def __init__                 ( self, wavelength, PAF = True, log = None, dataWriter = None):
        """
        Initializes the ArrayDataClass.

        Parameters:
            wavelength (float): wavelength set in all farfield pattern. Only important if used at single frequency. If multiple farfield patterns for different frequencies are given this value is reset after changing frequency with the method 'changeFarFieldFreq'
            PAF (boolean): Flag to indicate if the receiver array is a PAF (True) or is a classical array receiver (or single pixel receiver). This is used within the EM-simulation to derive the possible beams in the Field of view.
        """
        if log == None:
            self.log = LogClass.LogClass(_CLASSNAME)
        else:
            self.log = deepcopy(log)             # logging class used for all in and output messages
            self.log.addLabel(self)

        if dataWriter == None:
            self.dataWriter = HDF5Writer.HDF5Writer(_CLASSNAME+".h5", self.log)
        else:
            self.dataWriter = dataWriter             # HDF5 writer to store simulation data

        # define array
        self.element_list     = []           # list of Element data (all type 'ElementData')
        self.spacing          = 50.          # spacing between elements
        self.matching         = -1           # matching strategy (-1: using antenna impedance, 
                                             #                     0: using LNA impedance,
                                             #                    >0: using given number as impedance)
        self.noElements       = 0            # number of elements
        self.noChains         = 0            # total number of receiving chains (can be more than one per element)
        self.Radius           = 0            # FoV maximum radius
        self.PAF              = PAF          # flag for being a PAF (True) or a classical receiver
        
        self.Wavelength       = wavelength   # consistent wavelength throughout all receiving chains
        
        self.elementSize      = self.spacing # element size (only for plotting)

    def printOut                 ( self, line, message_type, noSpace = True, lineFeed = True ) :
        """
        Wrapper for the printOut method of the actual log class.
        """
        self.log.printOut(line, message_type, noSpace, lineFeed)


    def getArraySpecs(self):
        """
        Return element IDs, positions and polarizations of arrayElements.

        Returns:
            IDList (list): List of IDs to identify each arrayElement.
            positionList (list): List of positions (x,y) of each arrayElement.
            polarizationList (list): List of polarizations (degree) of each arrayElement.
        """
        elementID = 0

        IDList = []
        positionList = []
        polarizationList = []
        for el in self.element_list:
            IDList.append(elementID)
            elementID += 1

            positionList.append(el.getPos())

            polarizationsPerElement = []
            for sc in el.chains:
                  polarizationsPerElement.append(sc.polarization)
            polarizationList.append(polarizationsPerElement)

        return IDList, positionList, polarizationList



    def setSpacing               ( self, spacing ) :
        """
        (re-)set the actual spacing if the array

        Parameters:
            spacing (float): new spacing [mm]
        """
        self.printOut("Setting array spacing to %.2f mm" % spacing, self.log.FLOW_TYPE)
        # re-calculate focal-plane radius
        self.Radius  = self.Radius/ self.spacing * spacing
        # set new spacing
        self.spacing = spacing

    def _checkFrequencies        ( self ) :
        """
        Check consistency of the far-field data to verify that frequencies are all the same and set actual far-field wavelengs as simulation wavelength.

        Returns:
            boolean (boolean): True if all frequencies are the same within 0.01 mm, false if not
        """
        wavelength = -1
        for el in self.element_list :
            for ch in el.chains :
                if wavelength < 0:
                    wavelength = int(ch.farfield.Wavelength*100 + 0.5)/100.
                if int(ch.farfield.Wavelength*100 + 0.5)/100. != wavelength :
                    self.printOut("Far-field data have different frequencies assigned! Simulations not possible", self.log.ERROR_TYPE, False)
                    return False

        self.Wavelength = wavelength
        return True


    #####################################
    # create focal plane
    def createPattern            ( self, elements, limitingRadius = 300, patternType = 'squared', offset = None, rotation = 0., new = False, shuffleRadius = 0., step = -1, **kwargs ):
        """
        Create elements and their positions.
        Parameters:
            elements (int):         number of elements
            limitingRadius (?):   Radius to truncate the pattern outside a circle with this radius.
            patternType (?):      see types defined in the Distribution data class
            offset (?):           array-like first element: shift in x; second element: shift in y (optional)
            rotation (?):         pattern rotation around center point (degree)
            new (?):              delete the old pattern (True) or add the new pattern to the existing (False, default)
            shuffleRadius (?):    radius in which the elements can be shuffled around (hypersymetric distribution)
            step (?):             step size between elements (if negative, try to adapt to fill area with number of points given)
        """
        if new :                                           # check if pattern is added to existing pattern or taken as new
            self.element_list = []
            self.Radius     = 0
            self.noElements = 0
            self.noChains   = 0
         
        if step > 0 :
            self.spacing = step
            
        # extract offsets
        xoff = 0.
        yoff = 0.
        if (offset != None ) and (len(offset) > 0) :
            xoff = offset[0] / self.spacing
            if len(offset) > 1 :
                yoff = offset[1] / self.spacing
        limitRad  = limitingRadius + np.sqrt(xoff**2. + yoff**2.) * self.spacing + 1.      
        # create element pattern
        self.printOut("Create focal plane pattern:", self.log.FLOW_TYPE)
        # use distribution data class to set pattern
        d = DistributionDataClass.DistributionData( log = self.log )
        d.setGeometry((limitRad)*2., -1, True)
       
        if patternType.upper() == "VOGEL" :     # shift vogels pattern always in a way that the distribution center is outside the array
            kwargs['voffset'] = limitRad*1.1
        
        d.setType(patternType, **kwargs)
            
        d.setPoints(elements)
        d.setSpacing(step)
        d.setMinimumSpacing(self.elementSize)

        self.spacing = d.createDistribution()

        if shuffleRadius > 0. :
            d.shuffle(shuffleRadius)
        # extract data from distibution data class
        plist = np.asarray(d.Data) / self.spacing
        self.printOut("Created %d elements" % len(plist), self.log.DATA_TYPE, False)
        
        rotation *= np.pi/180.
        
        # shift and rotate pattern and finally apply spacing variation
        pl = np.zeros((len(plist), 2), dtype = float)
        for idx, p in enumerate(plist) :
            px      = (p[0]+xoff)*np.cos(rotation)- (p[1]+yoff)*np.sin(rotation)
            py      = (p[0]+xoff)*np.sin(rotation)+ (p[1]+yoff)*np.cos(rotation)
            pl[idx] = [px, py]
            
        # sort pattern
        plist = pl                              # sort pattern for distance to center
        distances = np.sqrt(np.sum(plist*plist, axis=1))
        idx_list = np.argsort(distances)
       
        maxIdx = len(distances) - 1
        for i, idx in enumerate(idx_list):
            if distances[idx]*self.spacing > limitingRadius :
                maxIdx = i -1
                break
        
        Radius = distances[maxIdx]*self.spacing
        if Radius > self.Radius:
            self.Radius = Radius

        l = plist[idx_list,:]   # TODO Change this name
        antenna_coordinate_list = l[:maxIdx+1]
        return antenna_coordinate_list


    def getCentralElement        ( self ) :
        """
        Returns the element-number closest to the focal plane center
        """
        minDist = 1.e100
        element = None
        for idx, el in enumerate(self.element_list) :
            px, py = el.getPos()
            d      = px*px + py*py
            if d < minDist :
                minDist = d
                element = idx

        return element


    def add_antennas             (self, antenna_coordinate_list):
        number_el_before = self.noElements
        for xy in antenna_coordinate_list :
            element = ElementData()
            element.posx = xy[0]
            element.posy = xy[1]
            self.element_list.append(element)
            self.noElements += 1
        self.printOut("Added %d antenna elements to the PAF" % (self.noElements-number_el_before), self.log.DATA_TYPE, False)


    def add_signalchains         ( self, signalchain, wavelength, select = lambda el : el.noChains == 0, phaseShift = 0. ) :
        """
        Add chain 'chain' to all elements witch are selected by the function given.

        Parameters:
            signalchain (SignalChain): ChainData class which is set to all elements selected by 'select'.
            select (function):         Function which gets the element data and returns boolean which determins if 'chain' added to element or not (default: all)
        """
        self.printOut("Adding new detection chain to the array", self.log.FLOW_TYPE)
        for el in self.element_list:
            if select(el):
                x, y = el.getPos()
                x *= self.spacing   # apply spacing
                y *= self.spacing
                d  = np.sqrt(x*x + y*y)/wavelength  # The absolute distance from zero point in number of wavelengths
                ch = deepcopy(signalchain)
                ch.phase = np.pi * d * phaseShift   # this here makes no sense to me (SH)
                el.setChain(ch)
                self.noChains += 1


    def changeFarField           ( self, farfield, element = -1, chainNumber = -1 ) :
        """
        Change the far-field data of chain 'chainNumber' of element 'element'.

        Parameters:
            farfield (farfield): New farfield pattern to be used
            element (int):       Element number at which the far field is to be exchanged ( < 0: all elements)
            chainNumber (int):   Chain number at which the far field is to be exchanged ( < 0 : all chains)
        """
        self.printOut("Exchanging far-field data (Element: %d, Chain: %d)" %(element, chainNumber), self.log.FLOW_TYPE)

        if element > self.noElements :
            self.printOut("Element does not exist", self.log.ERROR_TYPE, False)
            return
        # get list of elements to modify
        if element < 0 :
            elementList = range(0, self.noElements)
        else :
            elementList = [element]

        for el in elementList :         # loop all elements
            # get list of chains to modify
            chainList = []
            if chainNumber < self.noChains :
                chainList = [chainNumber]
            if chainNumber < 0 :
                chainList = range(0, self.noChains)

            for ch in chainList :          # loop all chains
                # replace far-field data
                self.element_list[el].chains[ch].farfield = farfield

        self._checkFrequencies()


    def changeFarFieldFreq       ( self, frequency ) :
        """
        Changes the frequency of all far-field instances in the array an loads the available data

        Parameters:
            frequency (floatstringint): frequency [GHz] to be set. Can be a float, a string or an integer
        """
        freq = float(frequency)
        self.printOut("Changing the frequency of all far-field data to %.2fGHz" % freq, self.log.FLOW_TYPE)
        
        for el in range(0, self.noElements) :  # loop all elements
            for ch in self.element_list[el].chains :    # loop all chains
                # replace far-field data
                ch.farfield.setFrequency(freq)

        self._checkFrequencies()       # check frequencies and set actual wavelength
        
    #####################################
    # simulate array far field response
    def calculateElementPattern ( self, telescope, element, chainNumber = 0 ) :
        """
        Use telescope data class to create the far-field pattern of an array element. Internal method, used by calculateArrayPattern.

        Parameters:
            telescope (TelescopeDataClass):   Telescope data used for simulation
            element (int):     Element number of the array to simulate
            chainNumber (int): Chain number of the element to be simulated

        Returns:
            x (UFODataClass): On sky data.
            y (UFODataClass): Spill-over data.
            z (UFODataClass): Temperature weighted spill-over data.
        """
        # error checking
        self.printOut ("Simulating array element number %d, chain %d :" % ( element, chainNumber ), self.log.FLOW_TYPE )
        if ( element > self.noElements ) or ( element < 0 ) :
            self.printOut( "element number does not match an array element", self.log.ERROR_TYPE, False )
            return

        el = self.element_list[element]
        if ( chainNumber >el.noChains ) or ( chainNumber < 0 ) :
            self.printOut( "chain number does not match an chain of the element", self.log.ERROR_TYPE, False )
            return
        
        # set initial far field for telescope simulation
        farfield = deepcopy( el.chains[chainNumber].farfield )                                               # extract far-field data from chain
        farfield.resetRotation()                                                                             # make sure far-field propagation path is along positive z-axis
        farfield.rotate    ( 0., 0., el.chains[chainNumber].polarization )                                   # implement polarization
        farfield.setPhase  ( el.chains[chainNumber].phase )                                                  # add phase shift as set
        fieldLoss = np.sqrt( 1. - el.chains[chainNumber].loss )                                              # loss = field-loss **2
        farfield.setGain   ( fieldLoss, relative = False )                                                   # implement filter loss
        
        # move and rotate far-field to complie with the array orientation and location
        #   calculate rotation angles to rotate propagation vector towards the telescope
        rot, invRot, angles = getAngles (telescope.inputZ, telescope.inputX )
        
        #   calculate absolute position from relative offsets and move farfield accordingly 
        antennaPosition = np.asarray( [el.posx * self.spacing, el.posy * self.spacing, 0.] )
        antennaPosition = np.dot( rot, antennaPosition ) + telescope.inputPos  
        distance        = np.sqrt( el.posx**2 + el.posy**2 ) * self.spacing
        # introducs resulting phase shift (in degree) by position and therewith apply lossless resonant minimum scattering approximation 
        farfield.setPhase ( 360. * distance / self.Wavelength )
        
        # rotate far-field accordingly
        farfield.rotate ( 0., 0., -angles[2] )
        farfield.rotate ( 0., -angles[1], 0. )
        farfield.rotate ( -angles[0], 0., 0. )
        farfield.moveCenter( antennaPosition[0], antennaPosition[1], antennaPosition[2] )                    # shift antenna according position
        
        # simulate pattern with constant sky size (scaleWavelength set > 0)
        return farfield

    def calculatePattern         ( self, telescope, SourcePol = 0. ) :
        """
        Calculate all element patterns at the actual wavelength as set

        Parameters:
            telescope (TelescopeDataClass): Actual telescope used for the simulation
            SourcePol (float): Astronomical source polaritation vector (degree)

        Returns:
            FoV (FoVDataClass): Holding all simulated data
        """
        FoV            = FoVDataClass.FoVData( SourcePol, self.Wavelength, telescopeFocalLength = telescope.FocalLength, log = self.log, dataWriter = self.dataWriter ) # define FoV class with all analysis tools

        self.printOut("Looping all elements and calculate their far-field response pattern", self.log.FLOW_TYPE)
        # check frequency setup for consistency
        if not self._checkFrequencies() :
            return FoV
        
        FoV._calcNoiseMatrices(self.noChains, self.element_list )

        # determine far-field patterns (chain index by chain index to have sorted polarizations if possible)
        c_idx          = 0                                                                      # chain counter
        foundChain     = True                                                                   # flag if a chain with this index exists

        while foundChain :                                                                      # while chains exist loop all elements
            foundChain = False                                                                      # chain exist flag set to false
            self.printOut( "using chain %d" % c_idx, self.log.DATA_TYPE, False )
            for el_idx, el in enumerate( self.element_list ) :                                      # loop all elements
                self.printOut( "using element %d" % el_idx, self.log.DATA_TYPE, False )
                if len(el.chains) > c_idx :                                                         # if chain exists
                    foundChain  = True                                                                  # chain exist falg set to true

                    elementFarfield = self.calculateElementPattern ( telescope, el_idx, c_idx )     # simulate chain
                    sky, spill, spillT = telescope.simulate(elementFarfield, scaleWaveLength = self.Wavelength)
                    FoV.setFarfieldResponse ( sky, spill, spillT )                                      # and add result to FoVDataClass

            c_idx += 1                                                                              # increase chain index after element loop

        self.printOut( "calculate mutual impedance matrices in FoVDataClass", self.log.DEBUG_TYPE, False )
        FoV.calculateCouplingMatrices( telescope.Diameter )
        
        return FoV

    def EM_Simulation            ( self, telescope, SourcePol = 0., matching = -1, resultFolder = "./" ) :
        """
        Full simulation of the far-field pattern and their belonging spill-over regions of all chains defined in the array for all frequencies available for the antenna far-field data.

        Parameters:
            telescope (TelescopeDataClass): Actual telescope used for the simulation
            SourcePol (float): Astronomical source polaritation vector (degree)
            resultFolder (string): Folder where plots and data results are stored

        Returns:
            FoVList (TelescopeDataClass): List of FoV data-classes holding all simulation results. These classes can now be used for beamforming and performance testing
            ArrayPlots (TelescopeDataClass): Array plot class with all plots from the data
        """
        # TODO: create a file with all result data
        self.printOut("Simulating array and creating plots", self.log.FLOW_TYPE)

        # get list of available frequencies for the array
        avFreq      =  sorted(self.element_list[0].chains[0].farfield.freqDict)
        
        FoVList     = []
        ArrayPlots  = None

        for f_idx, freq in enumerate(avFreq) :                               # loop all available frequencies

            self.changeFarFieldFreq( freq )                                      # change all far-field instances to the new frequency

            FoVList.append( self.calculatePattern ( telescope, SourcePol ) )     # simulate the array-setup
            
            # do beamforming over the full FoV
            center_idx, minDist = FoVList[-1].getBWList( matching = matching, PAF = self.PAF, writeToH5Flag = True)

            if ArrayPlots == None :
                ArrayPlots = ArrayPlotClass.ArrayPlotClass( self, FoVList[-1], resultFolder, self.log )
                
            if ArrayPlots.zoptLimit == None :
               ArrayPlots.zoptLimit = [ 10., 80.]

            ArrayPlots.makePlots(self, FoVList[-1])                              # use actual data set for plotting

        ArrayPlots.makeMovies( FoVList )                                    # make all movies and create all plots versus frequency
        return FoVList, ArrayPlots
