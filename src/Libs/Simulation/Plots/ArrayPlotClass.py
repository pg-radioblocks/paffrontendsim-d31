#!/usr/bin/env python3
"""
This file is part of the PAFFrontendSim.

The PAFFrontendSim is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License v3 as published by the Free Software Foundation.

The PAFFrontendSim is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with the PAFFrontendSim. If not, see https://www.gnu.org/licenses/.
"""
#############################################################################
# import libs
#############################################################################
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.mlab   as ml
from   copy                            import copy, deepcopy
from pathlib import Path

try:
    from   scipy.interpolate               import griddata
    _SciPy = True
except:
    _SciPy = False

import LogClass
from Simulation.Telescope           import TelescopeDataClass
from Simulation                     import FoVDataClass
from Simulation                     import DistributionDataClass
from Simulation                     import makeMovie
from Simulation.constants           import _Tgrd, _Tskatter, _Tsky, _Tatm, _Tcbg
from Simulation.constants           import _I0iso, _SigPol, _Impedance, _Bandwidth, _BaseAmp, _SkyDistance
from Simulation.constants           import _C, _kB, _ETA
from Simulation.constants           import _GridSize
from Simulation.BeamWeightDataClass import _Types as _BW_Types

#############################################################################
# constants
#############################################################################
_CLASSNAME          = "ArrayData"

_ImageWidth         = "7.5cm"                   # latex image width
_MovieDuration      = 8                         # length of a movie in seconds

#############################################################################
# classes
#############################################################################
class ArrayPlotClass:
    """
    Class with all plot-names (or movie-names) for the report. Plotting and movie creation is also automated.
    """
    def __init__             ( self, element_array, FoV, folder, log):
        self.log                = log
        self.folder             = folder
        self.zoptLimit          = None
        self.element_array      = element_array
        self.FoV                = FoV
        self._plotDic           = self.setDict(element_array, FoV)
        self.plotNames          = []    # array of all plot-names
        for i in range(0, len(self._plotDic)):
            self.plotNames.append([])

        # internal plot-dictionary. Plots of this dictionary will be created whithin the makeMovies method, making sure all simulation data was inserted into the dictionary before.
        # The plots are available via the getPlot method, like all others
        self.internalDict       = {
                                    "Sensitivity" : ( 0, self.plotSensitivity, 'figname', 'Sensitivity',  {} ),
                                    "NoiseLevel"  : ( 0, self.plotNoiseLevel,  'figname', 'NoiseLevel',   {} ),
                                    "Efficiency"  : ( 0, self.plotEfficiency,  'figname', 'Efficiency',   {} ),
                                    "ZoptvsFreq"  : ( 0, self.plotZoptvsFreq,  'figname', 'ZoptvsFreq',   {} ),
                                    "EffNoBeams"  : ( 0, self.plotEffNoBeams,  'figname', 'EffNoBeams',   {} )
                                  }
       
    def setDict              ( self, element_array, FoV) :
        # setting the plotts in a internal dictionary
        freq    = "%.2fGHz" % (int(_C/element_array.Wavelength/1e4+.5)/100.)
        
        plotDict = { # dictionary for all plot-details: array index where stored, plot-rutiene, filename-key, filename, kargs
            
            # plots of the central array element far field pattern (or movie)
            "CenterBeams"     : (  0, FoV.plotFarFieldSkyResponse,   'figname', 'centerBeam',    {'element' :  0}  ),
            
            # plots of the combined intensity pattern of all elements (or movie)
            "AllBeams"        : (  1, FoV.plotFarFieldSkyResponse,   'figname', 'allBeams',      {'element' : -1}  ),
            
            # plots of the mutualimpedance matrix on sky and spill-over (or movie)
            "Asignal"         : (  2, FoV.noise.Asignal.plotMatrix,  'figname', 'ASignal',       {'name' : '%s : $A_{signal}$' % freq}                ),
            "Aspill"          : (  3, FoV.noise.ALossSum.plotMatrix, 'figname', 'ALossSum',      {'name' : '%s : $A_{spill}$'  % freq}                ),
            
            # plot of the element usage over the array for the FoV (or movie)
            "ElementUsageCFM" : (  4, FoV.plotElementUsage,          'figname', 'ElemtUsage',    {}                ),
            
            # plot beam-weights of the central beam (or movie)
            "BeamWeight_CFM"  : (  5, FoV.plotCenterBW,              'figname', 'BW_CFM',        {'beamforming' : 0} ),
            "BeamWeight_MD"   : (  6, FoV.plotCenterBW,              'figname', 'BW_MD',         {'beamforming' : 1} ),
            "BeamWeight_SNR"  : (  7, FoV.plotCenterBW,              'figname', 'BW_SNR',        {'beamforming' : 2} ),

            # plot central formed beams
            "FormedBeam_CFM"  : (  8, FoV.plotCenterBeam,            'figname', 'FormedBeam_CFM',{'beamforming' : 0} ),
            "FormedBeam_MD"   : (  9, FoV.plotCenterBeam,            'figname', 'FormedBeam_MD' ,{'beamforming' : 1} ),
            "FormedBeam_SNR"  : ( 10, FoV.plotCenterBeam,            'figname', 'FormedBeam_SNR',{'beamforming' : 2} ),
            
            # plot of Tsys/Eta over the FoV (or movie)
            "TsysCFM"         : ( 11, FoV.plotNoise,                 'figname', 'TsysCFM',       {'beamforming' : 0, 'parameter' : 6} ),
            "TsysMD"          : ( 12, FoV.plotNoise,                 'figname', 'TsysMD',        {'beamforming' : 1, 'parameter' : 6} ),
            "TsysSNR"         : ( 13, FoV.plotNoise,                 'figname', 'TsysSNR',       {'beamforming' : 2, 'parameter' : 6} ),
            
            # plot of the LNA-matching (or movie)
            "Zopt"            : ( 14, FoV.plotZopt,                  'figname', 'Zopt',          {'limit' : self.zoptLimit} ),
            "allX"            : ( 15, FoV.plotAllBeamsX,             'figname', 'allAlongX',     {}                  ),

            "Eta_CFM"         : ( 16, FoV.plotEta,                   'figname', 'Eta_CFM',       {'beamforming' : 0} ),
            "Eta_MD"          : ( 17, FoV.plotEta,                   'figname', 'Eta_MD',        {'beamforming' : 1} ),
            "Eta_SNR"         : ( 18, FoV.plotEta,                   'figname', 'Eta_SNR',       {'beamforming' : 2} )
                  }
        
        # plots of the Far-Field pattern
        no = len(plotDict)
        if len(element_array.element_list) > 0 :
            plotDict["Array_Layout"] = ( deepcopy(no), self.plotArray, 'figname', "Array_Layout", {} )
            no += 1
            for idx, ch in enumerate(element_array.element_list[0].chains) :
                plotDict['Farfield%d' % idx]        = ( deepcopy( no + 2 * idx    ), ch.farfield.plotFarField2d, 'figname', ( 'Farfield%d'        % idx ), {}                          )
                plotDict['ElementFarfield%d' % idx] = ( deepcopy( no + 2 * idx + 1), self.plotElementBeams,      'figname', ( 'ElementFarfield%d' % idx ), {'chainNo' : deepcopy(idx)} )
                
        return plotDict

    def getPlot              ( self, key , LaWidth = _ImageWidth):
        """
        Get the actual plot or movie including the first movie image belonging to the key given.

        Parameters:
            key (string): key-string as defined in _plotDic
            LaWidth: width string (width + latex unit) for the LateX output

        Returns:
            movieName (string): movie or image name
            firstPlot (???): first movie image
            movie (boolean): indicating movie or image
            teXInclude (string): tex-include string
        """
        
        # get dictionary entry

        if self._plotDic.get(key, None) == None :   # check if key exists
            return None, None, False, ""
       
        plot_idx = self._plotDic[key][0]            # get plot idx
        if len (self.plotNames[plot_idx]) < 1  :    # check if plots available
            return None, None, False, ""

        firstPlot  = self.plotNames[plot_idx][0]     # get first plot
        movieName  = self.plotNames[plot_idx][-1]    # get last plot or movie
        movie      = True                            # establish type-flag
        if movieName.suffix != ".mov" :
            movie    = False
        
        # compose LaTeX include string
        teXInclude = "\\includegraphics[width=%s, angle=0]{%s}" % (LaWidth, movieName)
        if movie :
            teXInclude = """
            \\includemedia[
                           activate=pagevisible,
                           deactivate=onclick,
                           width=%s,
                           transparent,
                           addresource=%s,
                           flashvars={source=%s &loop=true &scaleMode=stretch}
                          ]{\\includegraphics[width=%s]{%s}}{VPlayer.swf}
            """ % (LaWidth, movieName, movieName, LaWidth, firstPlot )
        return movieName, firstPlot, movie, teXInclude
        
    def plot                 ( self, element_array, key = "" ) :
        """
        Plot the data along the key given (plot-parameters taken from dictionary.
        """
        if not key in self._plotDic :
            return False
        plotData = self._plotDic.get(key)

        # get id-string for the frequency in plot-names
        freqString = str(int(_C / element_array.Wavelength / 1000.+0.5))
        
        fileName = Path(self.folder, f"{plotData[3]}_{freqString}MHz.png")
        
        # compose plot-method arguments
        keyargs  = plotData[4]
        keyargs[plotData[2]] = fileName
        
        # plot figure
        if type(plotData[1]) != type(None) :
            plotData[1](**keyargs)
            # add plot-name to internal repository
            self.plotNames[plotData[0]].append(fileName)

    def makePlots            ( self, element_array, FoV) :
        """ 
        Make all plots available.
        """
        self._plotDic           = self.setDict( element_array, FoV )     # re-establish dictionary with all plotting details
        
        for k in self._plotDic.keys() :
            self.plot(element_array, k)
        
    def makeMovies           ( self, FoVList = None ) :
        """
        create movies out of all plots in the repository.
        """
        # establish data-folder
        if (len(self.plotNames[0]) < 1) : return
    
        fd = str(self.plotNames[0][0]).split('/')
        print(f"DEBUG: {fd}")
        folder = ""
        fd[-1] = ""
        for f in fd :
            folder += f + '/'
            
        duration = len(self.plotNames[0] )  
        if duration > _MovieDuration : duration = _MovieDuration
        
        for pl in self._plotDic.values() :           # loop all individual plots
            if (len(self.plotNames[pl[0]] ) < 2 ) : continue
        
            # build wild-card name for the plot-data
            wildcardName = Path(folder, pl[3] + '_*MHz.png')
            movieName    = Path(folder, pl[3] + '.mov')
            makeMovie(wildcardName, movieName, duration)
            self.plotNames[pl[0]].append(movieName)
        
        if type( FoVList ) == type( None ) :    # check if FoV data versus frequency is given
            return
        
        # make plots vs frequency :
        for key in self.internalDict:
            plot              = self.internalDict[key]
            plotID            = len( self.plotNames )
            filename          = Path(folder, plot[3] + '.png')
            # compose plot-method arguments
            keyargs  = plot[4]
            keyargs[plot[2]] = filename
            print (keyargs)
            plot[1]( FoVList, **keyargs )
            self._plotDic[key] = ( plotID, None, plot[2], plot[3], plot[4] )
            self.plotNames.append( [filename] )
            
    def plotArray            ( self, figname = "", show = False) :
        """
        Plot the element positions.

        Parameters:
                figname (string): file-name of the figure (if given figure is only shown on screen if show == True
                show (boolean):    flag to show plot on screen even when being written to disk (True)
        """

        self.log.printOut("Plotting Array element positions", self.log.FLOW_TYPE)
        x   = []
        y   = []
        pol = []
        for idx, obj in enumerate(self.element_array.element_list) :   # loop all telescope objects except the sky
            xp, yp = obj.getPos()
            for c in obj.chains :
                x.append(xp)
                y.append(yp)
                p, t, ep, et = c.farfield.getField( 0., 0.)
                pol.append(int(c.polarization + np.arctan2(np.absolute(ep), np.absolute(et))/np.pi*180. + 0.5) )

        x   = np.asarray(x)
        y   = np.asarray(y)
        pol = np.asarray(pol)
        # multiply with element spacing
        x   *= self.element_array.spacing
        y   *= self.element_array.spacing
        # get color by angular quadrant)
        pp   = np.divmod(pol, 90.)
        pp   = np.remainder(pp[0], 2)
        pp   = np.absolute(pp)
        pol  = np.pi/180.*pol

        fig, ax = plt.subplots()
        for i in range(0, len(x)) :

            xp = [
                   np.cos(pol[i])*self.element_array.elementSize/2. + x[i],
                  -np.cos(pol[i])*self.element_array.elementSize/2. + x[i]
                 ]
            yp = [
                   np.sin(pol[i])*self.element_array.elementSize/2. + y[i],
                  -np.sin(pol[i])*self.element_array.elementSize/2. + y[i]
                 ]
            if pp[i] == 0 :
                ax.plot(xp, yp, 'b')
                ax.plot(x[i], y[i], 'ob')
            else :
                ax.plot(xp, yp, 'r')
                ax.plot(x[i], y[i], '+r')

        ax.set_xlabel("distance [mm]")
        ax.set_ylabel("distance [mm]")
        ax.set_title ("Projected array element positions (x-y plane)")
        ax.set_aspect('equal')
        if figname != "" :
            fig.savefig(figname)
            if show :
                plt.show()
        else :
            plt.show()

        plt.close()

    def plotElementBeams     ( self, figname = "", show = False, chainNo = 0 ) :
        """
        Plots 9 element beam far-field pattern.

        Parameters:
            figname (string): file-name of the figure (if given figure is only shown on screen if show == True
            show (boolean):   flag to show plot on screen even when being written to disk (True)
            chainNo (int):    chain data to plot
        """
        self.log.printOut("Plotting a sub-set of 9 element beams spread over the FoV", self.log.FLOW_TYPE)
        if not _SciPy :
            self.log.printOut("  SciPy-Lib is not available, can't create the plot!", self.log.WARNING_TYPE, False)
            return

        freq    = "%.2fGHz" % (int(_C/self.element_array.Wavelength/1e4+.5)/100.)

        # extract a sub set of 9 elements spread over theFoV
        elPoints = []
        step     = int(len(self.element_array.element_list)/9)
        if step == 0 : step = 1
        for el_idx in range(0, len(self.element_array.element_list), step) :
            elPoints.append(el_idx)

        label = True
        fig, ax = plt.subplots(3, 3)

        plotNo = 0
        for el_idx in elPoints :
            row    = int(plotNo/3.)
            col    = plotNo - 3*row

            field_idx = el_idx + chainNo * self.element_array.noElements
            if field_idx >= self.element_array.noChains : break

            x = self.FoV.v_signal[:, 0]*180.*60./(np.pi*_SkyDistance)
            y = self.FoV.v_signal[:, 1]*180.*60./(np.pi*_SkyDistance)
            I = self.FoV.I_signal[field_idx, :]
            I = 10.*np.log10(I/np.max(I))

            # re-grid data
            xi, yi = np.mgrid[min(x):max(x):_GridSize*1j, min(y):max(y):_GridSize*1j]
            zi = griddata((x, y), I, (xi, yi), method = 'linear')

            cf = ax[row,col].pcolormesh(xi, yi, zi, vmin= -40., vmax= 0., cmap = plt.get_cmap('rainbow'))
            ax[row, col].contour(xi, yi, zi, levels=[-10, -3], linewidths = 0.5, colors = 'k')
            ax[row, col].set_aspect('equal')
            ax[row, col].set_title ("pos (%.2f, %.2f)" % ( self.element_array.element_list[el_idx].posx, self.element_array.element_list[el_idx].posy) )
            if row != 2 :
                ax[row, col].set_xticklabels([])
            if col > 0 :
                ax[row, col].set_yticklabels([])

            plotNo += 1
            if plotNo >= 9 : break

        fig.suptitle("%s : Chain %d: Element Far-Field intensity in dB\n(contours are at -3, -10 dB)" % (freq, chainNo))
        fig.tight_layout   (h_pad = 2.  )
        plt.subplots_adjust(top   = 0.85)

        if figname != "" :
            fig.savefig(figname)
            if show :
                plt.show()
        else :
            plt.show()

        plt.close()

    # plots using all frequencies and are called within the makeMovies method
    ##############################
    def plotSensitivity      ( self, FoVList, figname = "", show = False) :
        """
        Plot the bore-sight beam sensitivity data.

        Parameters:
            FoVList (list):   list of FoV data classes with the simulation results
            figname (string): file-name of the figure (if given figure is only shown on screen if show == True
            show (boolean):   boolean flag to show plot on screen even when being written to disk (True)
        """

        self.log.printOut("Plotting the bore-sight sensitivity of the Array", self.log.FLOW_TYPE)
        if len( FoVList ) < 2 :
            self.log.printOut("  no frequency data available", self.log.ERROR_TYPE, False)

        centerData = []
        for fov in FoVList :
            centerData.append( fov.getCenter() )
            
        freq    = []
        Trec    = [[], [], []]
        Tsys    = [[], [], []]
        Tsys_oe = [[], [], []]

        for fd in centerData:
            freq.append( int( _C / fd.BW_CFM.Wavelength / 1e4 + 0.5 ) / 100. )
            Trec   [0].append(fd.BW_CFM.Noise.T_rec)
            Trec   [1].append(fd.BW_MD.Noise.T_rec)
            Trec   [2].append(fd.BW_SNR.Noise.T_rec)
            Tsys   [0].append(fd.BW_CFM.Noise.T_sys)
            Tsys   [1].append(fd.BW_MD.Noise.T_sys)
            Tsys   [2].append(fd.BW_SNR.Noise.T_sys)
            Tsys_oe[0].append(fd.BW_CFM.Noise.T_sys_oe)
            Tsys_oe[1].append(fd.BW_MD.Noise.T_sys_oe)
            Tsys_oe[2].append(fd.BW_SNR.Noise.T_sys_oe)

        maxY    = np.median( Tsys_oe[1] ) * 2.
        if maxY > np.amax( Tsys_oe[1] ) * 1.1 :
            maxY = np.amax( Tsys_oe[1] ) * 1.1

        fig, ax = plt.subplots()

        ax.set_xlabel("Frequency [GHz]")
        ax.set_ylabel("Noise-Temperature [K]")
        ax.set_title ("Array bore-sight sensitivity over frequency")
        ax.set_ylim(0, maxY)

        ax.plot(freq, Trec[0],    'k-.')
        ax.plot(freq, Trec[1],    'k--')
        ax.plot(freq, Trec[2],    'k-' )
        ax.plot(freq, Tsys[0],    'b-.')
        ax.plot(freq, Tsys[1],    'b--')
        ax.plot(freq, Tsys[2],    'b-' )
        ax.plot(freq, Tsys_oe[0], 'r-.')
        ax.plot(freq, Tsys_oe[1], 'r--')
        ax.plot(freq, Tsys_oe[2], 'r-' )

        fig.legend(['Trec CFM', 'Trec MD', 'Trec SNR','Tsys CFM', 'Tsys MD', 'Tsys SNR', 'Tsys/Eta CFM', 'Tsys/Eta MD', 'Tsys/Eta SNR'], loc='upper left', bbox_to_anchor=(0.75, 0.65))

        if figname != "" :
            fig.savefig(figname)
            if show :
                plt.show()
        else :
            plt.show()

        plt.close()

    def plotNoiseLevel       ( self, FoVList, figname = "", show = False) :
        """
        Plot the Noise-levels of the bore-sight beams.

        Parameters:
            FoVList (list):   list of FoV data classes with the simulation results
            figname (string): file-name of the figure (if given figure is only shown on screen if show == True
            show (boolean):   boolean flag to show plot on screen even when being written to disk (True)
        """

        self.log.printOut("Plotting the bore-sight noise-levels of the Array", self.log.FLOW_TYPE)
        if len( FoVList ) < 2 :
            self.log.printOut("  no frequency data available", self.log.ERROR_TYPE, False)
        
        centerData = []
        for fov in FoVList :
            centerData.append( fov.getCenter() )
            
        freq    = []
        Tloss   = [[], [], []]
        Tspill  = [[], [], []]
        Tsig    = [[], [], []]

        for fd in centerData:
            freq.append( int( _C / fd.BW_CFM.Wavelength / 1e4 + 0.5 ) / 100. )
            Tloss   [0].append(fd.BW_CFM.Noise.T_loss)
            Tloss   [1].append(fd.BW_MD.Noise.T_loss)
            Tloss   [2].append(fd.BW_SNR.Noise.T_loss)
            Tspill  [0].append(fd.BW_CFM.Noise.T_spill)
            Tspill  [1].append(fd.BW_MD.Noise.T_spill)
            Tspill  [2].append(fd.BW_SNR.Noise.T_spill)
            Tsig    [0].append(fd.BW_CFM.Noise.T_sig)
            Tsig    [1].append(fd.BW_MD.Noise.T_sig )
            Tsig    [2].append(fd.BW_SNR.Noise.T_sig)

        maxY = np.amax([ np.amax(Tloss[1]), np.amax(Tspill[1]), np.amax(Tsig[1]) ])*1.05

        fig, ax = plt.subplots()

        ax.set_xlabel("Frequency [GHz]")
        ax.set_ylabel("Noise-Temperature [K]")
        ax.set_title ("Array bore-sight noise levels over frequency")
        ax.set_ylim(0, maxY)

        ax.plot(freq, Tloss [0], 'k-.')
        ax.plot(freq, Tloss [1], 'k--')
        ax.plot(freq, Tloss [2], 'k-' )
        ax.plot(freq, Tspill[0], 'b-.')
        ax.plot(freq, Tspill[1], 'b--')
        ax.plot(freq, Tspill[2], 'b-' )
        ax.plot(freq, Tsig  [0], 'r-.')
        ax.plot(freq, Tsig  [1], 'r--')
        ax.plot(freq, Tsig  [2], 'r-' )

        fig.legend(['Tloss CFM', 'Tloss MD', 'Tloss SNR','Tspill CFM', 'Tspill MD', 'Tspill SNR', 'Tsig CFM', 'Tsig MD', 'Tsig SNR'], loc='upper left', bbox_to_anchor=(0.75, 0.65))

        if figname != "" :
            fig.savefig(figname)
            if show :
                plt.show()
        else :
            plt.show()

        plt.close()

    def plotEfficiency       ( self, FoVList, figname = "", show = False) :
        """
        Plot the efficiencies of the bore-sight beams.

        Parameters:
            FoVList (list):   list of FoV data classes with the simulation results
            figname (string): file-name of the figure (if given figure is only shown on screen if show == True
            show (bool):      boolean flag to show plot on screen even when being written to disk (True)
        """

        self.log.printOut("Plotting the bore-sight efficiencies of the Array", self.log.FLOW_TYPE)
        if len( FoVList ) < 2 :
            self.log.printOut("  no frequency data available", self.log.ERROR_TYPE, False)
        
        centerData = []
        for fov in FoVList :
            centerData.append( fov.getCenter() )
            
        freq    = []
        E_a     = [[], [], []]
        E_rad   = [[], [], []]
        E_spill = [[], [], []]

        for fd in centerData:
            freq.append(int(_C/fd.BW_CFM.Wavelength/1e4 + 0.5)/100.)
            E_a     [0].append(fd.BW_CFM.Efficiency.Eta_a)
            E_a     [1].append(fd.BW_MD.Efficiency.Eta_a)
            E_a     [2].append(fd.BW_SNR.Efficiency.Eta_a)
            E_spill [0].append(fd.BW_CFM.Efficiency.Eta_spill)
            E_spill [1].append(fd.BW_MD.Efficiency.Eta_spill)
            E_spill [2].append(fd.BW_SNR.Efficiency.Eta_spill)
            E_rad   [0].append(fd.BW_CFM.Efficiency.Eta_rad)
            E_rad   [1].append(fd.BW_MD.Efficiency.Eta_rad)
            E_rad   [2].append(fd.BW_SNR.Efficiency.Eta_rad)

        fig, ax = plt.subplots()

        ax.set_xlabel("Frequency [GHz]")
        ax.set_ylabel("Efficiency")
        ax.set_title ("Array bore-sight efficiencies over frequency")
        ax.set_ylim(0, 1.)

        ax.plot(freq, E_a     [0], 'k-.')
        ax.plot(freq, E_a     [1], 'k--')
        ax.plot(freq, E_a     [2], 'k-' )
        ax.plot(freq, E_spill [0], 'b-.')
        ax.plot(freq, E_spill [1], 'b--')
        ax.plot(freq, E_spill [2], 'b-' )
        ax.plot(freq, E_rad   [0], 'r-.')
        ax.plot(freq, E_rad   [1], 'r--')
        ax.plot(freq, E_rad   [2], 'r-' )

        fig.legend(['Eta_a CFM', 'Eta_a MD', 'Eta_a SNR','Eta_spill CFM', 'Eta_spill MD', 'Eta_spill SNR', 'Eta_rad CFM', 'Eta_rad MD', 'Eta_rad SNR'], loc='upper left', bbox_to_anchor=(0.75, 0.65))

        if figname != "" :
            fig.savefig(figname)
            if show :
                plt.show()
        else :
            plt.show()

        plt.close()

    def plotZoptvsFreq       ( self, FoVList, figname = "", show = False) :
        """
        Plot the optimum impedance of the signal chain input versus frequency.

        Parameters:
            FoVList (list):   list of FoV data classes with the simulation results
            figname (string): file-name of the figure (if given figure is only shown on screen if show == True
            show (bool):      boolean flag to show plot on screen even when being written to disk (True)
        """

        self.log.printOut("Plotting the optimum impedance of the signal chain input versus frequency", self.log.FLOW_TYPE)
        
        # extract data
        freq        = []
        zopt        = []
        
        for fov in FoVList :
            freq.append    ( _C / fov.SimWavelength / 1e6 )
            zopt.append( 0. )
            for el in range(0, len( fov.Zopt ) ):
                zopt[-1] += np.real( fov.Zopt[el, el] )
            zopt[-1]   = zopt[-1] / len( fov.Zopt )
            
        fig, ax = plt.subplots()
        ax.plot(freq, zopt, 'k-')
        ax.set_xlabel("Frequency [GHz]")
        ax.set_ylabel("Zopt [Ohm]")
        ax.set_title ("Optimum signal chain input impedance over frequency")

        if figname != "" :
            fig.savefig(figname)
            if show :
                plt.show()
        else :
            plt.show()

        plt.close()

    def plotEffNoBeams       ( self, FoVList, figname = "", show = False) :
        """
        Plot the effective number of beams versus frequency.

        Parameters:
            FoVList (list):   list of FoV data classes with the simulation results
            figname (string): file-name of the figure (if given figure is only shown on screen if show == True
            show (bool):      boolean flag to show plot on screen even when being written to disk (True)
        """
        self.log.printOut("Plotting the effective number of beams versus frequency", self.log.FLOW_TYPE)
        
        # extract data
        freq        = []
        effBeams    = []
        
        for fov in FoVList :
            freq.append    ( _C / fov.SimWavelength / 1e9 )
            effBeams.append( fov.calculateSSFoV() )
        
        # extract numbers:
        noBeams     = np.zeros(len(freq))
        formed_CFM  = np.zeros(len(freq))
        formed_MD   = np.zeros(len(freq))
        formed_SNR  = np.zeros(len(freq))
        effNo_CFM   = np.zeros(len(freq))
        effNo_MD    = np.zeros(len(freq))
        effNo_SNR   = np.zeros(len(freq))
        SSFoM_CFM   = np.zeros(len(freq))
        SSFoM_MD    = np.zeros(len(freq))
        SSFoM_SNR   = np.zeros(len(freq))

        for idx, eb in enumerate(effBeams) :
            noBeams   [idx] = eb[3]
            effNo_CFM [idx] = eb[0][2]
            effNo_MD  [idx] = eb[1][2]
            effNo_SNR [idx] = eb[2][2]
            formed_CFM[idx] = eb[0][1]
            formed_MD [idx] = eb[1][1]
            formed_SNR[idx] = eb[2][1]
            SSFoM_CFM [idx] = eb[0][0]
            SSFoM_MD  [idx] = eb[0][0]
            SSFoM_SNR [idx] = eb[0][0]

        fig, ax = plt.subplots()

        plt.title("Survey-Speed / Effective number of beams : ")
        ax.plot(freq, SSFoM_CFM, 'r-.')
        ax.plot(freq, SSFoM_MD,  'r--')
        ax.plot(freq, SSFoM_SNR, 'r-' )
        ax.set_ylabel("Survey speed figure of merit [$deg^2 m^4 K^{-2}$]", color="red")
        ax.set_xlabel("Frequency [GHz]")
        for label in ax.get_yticklabels():
            label.set_color("red")

        ax2 = ax.twinx()
        ax2.plot(freq, effNo_CFM,  'b-.')
        ax2.plot(freq, effNo_MD ,  'b--')
        ax2.plot(freq, effNo_SNR,  'b-' )
        ax2.plot(freq, formed_CFM, 'k-.')
        ax2.plot(freq, formed_MD , 'k--')
        ax2.plot(freq, formed_SNR, 'k-' )

        ax2.set_ylabel("Number of beams")

        fig.legend(['SSFoM CFM', 'SSFoM MD', 'SSFoM SNR',
                    'Eff. No CFM', 'Eff. No MD', 'Eff. No SNR',
                    'Total CFM', 'Total MD', 'Total SNR'],
                   loc='upper left', bbox_to_anchor=(0.75, 0.85))

        if figname != "" :
            fig.savefig(figname)
            if show :
                plt.show()
        else :
            plt.show()

        plt.close()
