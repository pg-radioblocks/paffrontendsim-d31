#!!/usr/bin/env python3
"""
This file is part of the PAFFrontendSim.

The PAFFrontendSim is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License v3 as published by the Free Software Foundation.

The PAFFrontendSim is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with the PAFFrontendSim. If not, see https://www.gnu.org/licenses/.
"""
#############################################################################
# import libs
#############################################################################
import numpy as np
import os
from pathlib import Path
#############################################################################
# constants
#############################################################################
_CLASSNAME       = "Constants"

#############################################################################
# import environment variables
#############################################################################
# Path-names
try:
    _CSTInstallationPath = os.environ["CSTLibPath"]
except:
    _CSTInstallationPath = os.environ["HOME"] + "/CST"

try:
    _CSTProjectPath      = os.environ["CSTFilePath"]+"/"
except:
    _CSTProjectPath      = os.environ["HOME"] + "/PAF-Simulation/CST/"

file_path            = Path(__file__).resolve()                       # get the path of this file
full_path_tuple      = file_path.parts                                # split the path into its components
root_index           = full_path_tuple.index("paffrontendsim-d31")  # determine at what position in the path the root folder is
root_path_tuple      = full_path_tuple[:root_index+1]                 # store the path of the root folder
_pythonPath          = str(Path(*root_path_tuple))                    # reassemble the root path into a string


#############################################################################
# common physical constants
#############################################################################

_kB                 = 1.38064852E-23               # J/K

# vacuum properties
_EPS                = 8.854187812813e-12           # electric constant of vacuum [F/m]          (CODATA 2018 -- measured)
#_MU                 = 4.*np.pi*10**-7             # magnetic constant of vacuum [H/m == N/A^2] (before 2019 -- definition) 
_MU                 = 1.2566370621219e-6           # magnetic constant of vacuum [H/m == N/A^2] (CODATA 2018 -- measured)
_C                  = 299792458                    # speed of light in vacuum    [m/s]          (definition 1983)
#_ETA                = np.sqrt(_MU/_EPS)           # free space impedance        [Ohm]          (before 2019 -- definition)
_ETA                = 376.73031366857              # free-space impedance        [Ohm]          (after 2019 -- measured)

#############################################################################
# Simulation constants
#############################################################################
# System constants
# scattering efficiency for hitting the support-leg structure
_Esca               =   0.9                        # scattering efficiency
# temperatures:
_Tcbg               =   2.7                        # cosmic background radiation
_Tatm               =   3.0                        # loss in the atmospher (assumed 3K) 
_Tiso               = 290.                         # isometric temperature [K]
_Tgrd               = 290.                         # Spill-over temperature [K]
_Tsky               =  _Tcbg + _Tatm               # Sky temperature [K] (cosmic background + atmospheric losses)
_Tloss              =  20.                         # temperature of the antenna and matching losse [K]
_Tskatter           =  _Tsky + _Tgrd*(1-_Esca)     # temperature of spill-over scattered to sky
# signal data
_Sig                =   1.                         # define simulated sky signal strength [Jy]    
_SigPol             =   0.                         # default sky signal polarization

# iso exitation current
_I0iso_Start        = .2                           # isometric exitation current[A]. All detector fields are scaled to this
                                                   # number and hence simulation can tollerate individual exitation
                                                   # currents per element within the initial simulation

_BaseAmp            = 1000000.                     # basis amplitude of the far-field data to compensate for the sky-distance distance. 
_I0iso              = _I0iso_Start*_BaseAmp        # combined final exitation current

_Bandwidth          = 1000000.                     # Default Detector bandwidth factor (Hz) (integration factor)
_Impedance          = 50.                          # Default detector impedance [Ohm]

_RFI_MAX_LEVEL      = 60.                          # Maximum level for the RFI-simulations [dB]

# FoV definition
_FOV_SIZE_FACTOR    = 1.1                          # fov size is this factor time largest element offset from bore-sight

# simulation dimensions
_SkyDistance        = 1e9                          # distance between telescope apex and sky-plane
_SkySize            = 25                           # size of the sky-plane in units of the HPBW 
_SkyStep            = 10.                          # number of data-points per HPBW
_TELESCOPE_RASTER   =  5.                          # rastering of the telescope : this number times wavelength

_DEFAULT_WL         = 90.                          # default wavelength where the sky size and rastering is set to.

# constants for the plotting
_GridSize           = 300                          # default resolution for re-gridding 3d- false-color data. Final number of data-points is  GridSize**2

# Gaussian optics beam diameter
_GaussBeamDiam      = 5.                           # default size of the optical elements in units of the Gaussian-baem waist size

#############################################################################
# End
#############################################################################
