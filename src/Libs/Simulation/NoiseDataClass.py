#!/usr/bin/env python3
"""
This file is part of the PAFFrontendSim.

The PAFFrontendSim is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License v3 as published by the Free Software Foundation.

The PAFFrontendSim is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with the PAFFrontendSim. If not, see https://www.gnu.org/licenses/.
"""
# ToDo
# - add a possibility to introduce rx-internal optical losses (see telescope data class for a single horn implementation)
#
#############################################################################
# import libs
#############################################################################
import numpy as np
from copy import copy, deepcopy

import LogClass
from FileIO               import HDF5Writer
from Simulation           import BeamWeightDataClass
from Simulation           import SignalChainDataClass
from Simulation           import ImpedanceMatrixDataClass
from Simulation           import FoVDataClass
from Simulation.constants import _ETA, _C, _kB, _Impedance
from Simulation.constants import _Tiso, _Tgrd, _Tsky, _Tloss, _Tskatter, _Sig

#############################################################################
# constants
#############################################################################
_CLASSNAME   = "NoiseData"

#############################################################################
# classes
#############################################################################
class NoiseData :
    """
    Class to handle PAF noise calculations.
    """
    def __init__                  ( self, log = None, dataWriter = None) :
        """
        Initialize class.

        Parameters:
            log (LogClass):          Logging class to be used
            dataWriter (HDF5Writer): Module to write ACMs to disk
        """
        # simulation input data and constants
        if log == None:
            self.log = LogClass.LogClass(_CLASSNAME)
        else:
            self.log = deepcopy(log)             # logging class used for all in and output messages
            self.log.addLabel(self)

        if dataWriter == None:
            self.dataWriter = HDF5Writer.HDF5Writer(_CLASSNAME+".h5")   # NOTE why is HDF5Writer needed here? It is unused.
        else:
            self.dataWriter = dataWriter             # HDF5 writer to store simulation data

        self.Tiso        = _Tiso                              # isometric temperature [K]
        self.Tgrd        = _Tgrd                              # temperature of the spill-over terminal [K]
        self.Tsky        = _Tsky                              # temperature of the sky [K]
        self.Tloss       = _Tloss                             # temperature of the antenna and matching losse [K]
        self.Sig         = _Sig                               # Signal strength [Jy]
        self.Aphys       = 1.                                 # physical nominal area of the main dish including blockage [m^2]
        self.Wavelength  = 1.                                 # Wavelength of the calculations [mm]
        
        self.Theta       = 0.                                 # steering vector theta-direction (from input beam-weights data) [deg]
        self.Phi         = 0.                                 # steering vector phi-direction (from input beam-weights data)   [deg]
        
        #self.SignalChain = SignalChainDataClass.SignalChain(log = self.log) # class with all signal chain data
        # simulation intermediate products 
        self.Q           = []                                 # transformation matrix from open circuit to signal chain input
        self.QH          = []                                 # hermetian of Q
        
        self.Asignal     = None                               # impedance matrix of the sky response
        self.ALossSum    = None                               # impedance matrix of all combined losses
        
        self.R           = {}                                 # dictionary of all noise resistance matrices
        self.rxKeys      = []                                 # list of keys for the noise contributions which are assigned to the receiver

        self.Vsig        = []                                 # output voltage vector (without amplifier gain)
        self.Vnorm       = 1.                                 # normalization factor to calculate Vsig
        self.BwNorm      = 1.                                 # normalization factor for BWs (taken from steering vector)


    def printOut                  ( self, line, message_type, noSpace = True, lineFeed=True ) :
        """
        Wrapper for the printOut method of the actual log class.
        """
        self.log.printOut(line, message_type, noSpace, lineFeed)


    #####################################
    # Methods to calculate the noise
    def calculateNoiseResistances ( self, Asignal, Aloss, AlossTemp, DS_List, SC, Impedance = -1., Iv = [] ) :
        """
        Calculate all noise resistant matrices from simulation data and store them internally.
        Can be seen as an init-function.

        Impedance:
            > 0  : matching impedance
            0  : using signal chain impedance
            -1  : matched to antenna array (default)
            -2  : matched to external vector Iv (see parameter Iv)

        Parameters:
            Asignal (ImpedanceMatrixDataClass): mutual impedance matrix of the sky signal.
            Aloss (ImpedanceMatrixDataClass or list of Impedance matrices): mutual impedance matrix of the spill-over signal.
            AlossTemp (ImpedanceMatrixDataClass or list of Impedance matrices): mutual impedance matrix of the spill-over signal including temperature variations
            DS_List (list): list of beam-weight vectors (BeamWeightDataClass) for RFI sources. Last vector in the list points to signal of interest.
            SC (SignalChainDataClass): SignalChain data class
            Impedance (int): see above
            Iv (list): Impedance vector (individual matching of each element) if Impedance is set to -2

        Returns:
            tuple (tuple): ZoptAntenna, Zopt
        """
        self.printOut( "Calculating noise resistances", self.log.FLOW_TYPE )
        self.printOut( "  with %d RFI sources" % ( len( DS_List ) - 1 ), self.log.DATA_TYPE, False )
        
        self.printOut( "  using signal chain '%s'" % SC.Name, self.log.DATA_TYPE, False )
        
        # set signal chain data
        self.SignalChain = deepcopy( SC )
        
        # check for multiple input losses
        if isinstance( Aloss, ImpedanceMatrixDataClass.ImpedanceMatrix ) :
            Aloss = [ Aloss ]
        if isinstance( AlossTemp, ImpedanceMatrixDataClass.ImpedanceMatrix ) :
            AlossTemp = [ AlossTemp ]

        self.Asignal     = Asignal
        # extract simulation data from the impedance matrix:
        B                = self.Asignal.Bandwidth
        noElements       = self.Asignal.noElements 
        self.Wavelength  = self.Asignal.Wavelength
        Wavelength       = self.Wavelength / 1000.                           # wavelength in m not in mm
        self.Aphys       = self.Asignal.Aeff * 1.e-6                         # effective area im m^2 not mm^2
        I0               = self.Asignal.I0                                   # extract excitation current 
        DetectorGain     = self.Asignal.DetectorGain                         # extract detector gain (only used to scale the signal, all other matrices used relative to each other only)
        DetImpedance     = self.Asignal.Impedance                            # extract detector impedance
        InputPower       = 0.5 * DetImpedance * I0**2.                       # calculate overall input power
        
        self.printOut( "  Input power : %.2f" % InputPower, self.log.DATA_TYPE, False )
            
        self.ALossSum    = None 
        # Calculate open circuit thermal noise matrices (R in power, RT, scaled with the individual power)
        R_oc             = {}               # dictionary with all open circuit noise matrices
        RT_oc            = {}               # dictionary with all open circuit noise matrices weighted by temperature
        self.rxKeys      = ['horn', 'lna']  # list of receiver components
        
        for idx, Al in enumerate( Aloss ):
            key              = Aloss[idx].Name
            R_oc[key]        = ( 8. * _kB * B * Al.Data             * DetImpedance / InputPower * 1.e-6 )   # factor 1/10^6: r^2 in in mm not in m 
            RT_oc[key]       = ( 8. * _kB * B * AlossTemp[idx].Data * DetImpedance / InputPower * 1.e-6 * self.Tiso )

            if not key in ( 'atmosphere', 'telescope' ) :
                self.rxKeys.append( key )

            if self.ALossSum == None :
                self.ALossSum       = deepcopy ( Al )
                R_oc['allLoss']     = deepcopy( R_oc[key] )
                RT_oc['allLoss']    = deepcopy( RT_oc[key] )
            else :
                self.ALossSum.Data += Al.Data
                R_oc['allLoss']    += R_oc[key]
                RT_oc['allLoss']   += RT_oc[key]

        self.printOut(" Losses contributing to receiver noise : %s" % ( str( self.rxKeys ) ), self.log.DATA_TYPE, False )
        # Element loss noise resistance matrix ( we here assign all losses in the signal chain to the radiating device (horn).
        # calculate the radiation efficiency via the overall power  (only for one element here which is not 100% correct. we can in principle calculate the efficiencies for all elements)
        rad_eff          = np.sqrt( ( self.Asignal.Data[0, 0] + self.ALossSum.Data[0, 0] ) * 1.e-6 / InputPower )
        R_oc['horn']     = 8. * _kB * B * ( 1. - rad_eff**2 ) * np.identity( noElements, dtype=np.complex128 ) * DetImpedance
        RT_oc['horn']    = R_oc['horn'] * self.Tloss
        # Isotropic noise resistance matrix for the power reaching sky ( Rsky)
        R_oc['sky']      = 8. * _kB * B * self.Asignal.Data * DetImpedance / InputPower * 1.e-6
        RT_oc['sky']     = R_oc['sky'] * self.Tsky
        # thermal noise resistance matrix (contributions from isotropic temperature and loss mechanisms)  
        R_oc ['t']       = R_oc ['sky'] + R_oc ['allLoss'] + R_oc ['horn']
        RT_oc['t']       = RT_oc['sky'] + RT_oc['allLoss'] + RT_oc['horn']
        # Array input impedance matrix using resonant approximation (X = 0)
        ZA               = np.real ( ( R_oc['t'] ) / ( 8. * _kB * B ) )
        ZA_H             = np.transpose( ZA.conj() )                         # Hermetian of ZA
        
        # LNA noise (or better signal chain noise since it includes potentially also contributions from second stage amplifiers and other parts of the signal chain in the uncorrelated part)
        # get signal chain data for the actual wavelength
        if   isinstance(self.SignalChain, SignalChainDataClass.SignalChain) :
            f                = _C / Wavelength * 10**(-9.)                       # frequency in GHz
            tmin, rn, zopt, s11, s12, s21, s22  = self.SignalChain.getData( f )
            VR2              = 4. * _kB * self.SignalChain.T0 * rn * np.identity( noElements, dtype=np.complex128 )
            Yc               = tmin / ( 2. * self.SignalChain.T0 ) / rn * np.identity( noElements, dtype=np.complex128 )
            Yopt             = np.identity( noElements, dtype=np.complex128 ) / zopt
            Zopt             = np.identity( noElements, dtype=np.complex128 ) * zopt
            # analog signal chain input impedance matrix
            ZS               = np.identity( noElements, dtype=np.complex128 ) * self.SignalChain.Zs
            
        elif isinstance(self.SignalChain, FoVDataClass.SignalChainData )  :
            Yc               = self.SignalChain.Yc
            Zopt             = self.SignalChain.Zopt
            VR2              = self.SignalChain.VR2     # NOTE only stored in this method
            ZS               = self.SignalChain.ZS

            Yopt             = np.identity( noElements, dtype=np.complex128 )
            for i in range ( 0, noElements ) :
                Yopt[i, i]       = 1. / Zopt[i, i]      # NOTE Yopt is the inverse of Zopt
            # analog signal chain input impedance matrix

        else :
            self.printOut( "  No signal chain data given (or wrong type of data given)", self.log.ERROR_TYPE, False )
            return None, None
        
        ZoptAntenna = np.identity( noElements, dtype=np.complex128 )
        for i in range ( 0, noElements ) :
            ZoptAntenna[i, i] = ZA[i, i]

        # Matching strategy
        if   ( Impedance ==  0 ) :                                           # match to signal chain impedance
            self.printOut( "  using signal chain impedance for matching", self.log.DATA_TYPE, False )
        elif ( Impedance == -1 ) :                                           # match to antenna element self impedance
            for i in range ( 0, noElements) :
                Yopt[i, i] = 1 / ZA[i, i] 
                Zopt[i, i] = ZA[i, i]
            self.printOut( "  using antenna array self-impedance for matching", self.log.DATA_TYPE, False )
        elif ( Impedance == -2 ) :                                           # match to external vector
            for i in range ( 0, noElements ) :
                Yopt[i, i] = 1. / Iv[i]
                Zopt[i, i] = Iv[i]
            self.printOut( "  using individual impedance vector Iv for matching", self.log.DATA_TYPE, False )
        else :                                                               # match to external impedance
            Yopt          = np.identity( noElements, dtype=np.complex128 ) / Impedance
            Zopt          = np.identity( noElements, dtype=np.complex128 ) * Impedance
            self.printOut( "  using given impedance for matching", self.log.DATA_TYPE, False )

        self.printOut( "  Zopt = %.2f Ohm, Phase : %.2f deg" %( np.absolute( Zopt[0, 0] ), np.angle( Zopt[0, 0], 'deg' ) ), self.log.DATA_TYPE, False )

        # calculate open circuit LNA noise matrix
        Yc               = Yc - Yopt;
        Yc_H             = np.transpose( Yc.conj() )   
        IR2              = np.dot( np.absolute( Yopt )**2., VR2 )
        
        Rlna_oc          = VR2 + np.dot( np.dot( ZA, Yc ), VR2 )             # receiver noise covariance matrix 
        Rlna_oc         += np.dot( np.dot( VR2, Yc_H ), ZA_H )               #   continuing
        Rlna_oc         += np.dot( np.dot( ZA, IR2   ), ZA_H )               #   continuing
        Rlna_oc          = 2. * B * Rlna_oc                                  #   continuing
      
        # calculate signal response matrix including RFI
        # Signal response
        k                = 2. * np.pi / Wavelength                           # wavenumber (rad/m)
        E_sig            = np.sqrt( _ETA * self.Sig * 1e-26 * B )            # electric field intensity in one polarization (V/m)
        c1               = 4. * np.pi * 1j / ( k * _ETA * I0 )               # reciprocity theorem scale factor

        self.Vnorm       = c1 * E_sig / DetectorGain / 1000.                 # normalization + take out detector gain and mm to m 
        
        # Transform all open circuit voltages to loaded array output voltages
        # transfer matrix (antenna voltage output -> signal chain input)
        self.Q           = np.dot( ZS, np.linalg.inv( ZS + ZA ) )            # transfer matrix from antenna input to signal chain input
        self.QH          = np.transpose( self.Q.conj() )                     # Hermetian of Q
        
        # Transfer all noise resistance matrices from open circuitry to LNA input
        self.R           = {}
        self.RT          = {}
        for key in R_oc :
            self.R[key]      =  np.dot(np.dot(self.Q, R_oc [key]),  self.QH) 
            self.RT[key]     =  np.dot(np.dot(self.Q, RT_oc[key]),  self.QH) 
            
        self.RT['lna']    = np.dot(np.dot(self.Q, Rlna_oc),  self.QH)    
        self.RT['sys']    = self.RT['t'] + self.RT['lna']
        
        # finally: create signal response and signal resistance matrix
        self.setSignal(DS_List)

        return ZoptAntenna, Zopt
    
    def setSignal                 ( self, DS_List ) :
        """
        Recalculate signal response and signal resistance matrix.

        Parameters:
            DS_List (list): list of beam-weight vectors (BeamWeightDataClass) for RFI sources. Last vector in the list points to signal of interest.
        """
        noElements       = len(DS_List[-1].Data)                             # get number of elements from pointing vector length
        Rsig_oc          = np.zeros((noElements, noElements), dtype = np.complex128)
        
        # build matrix
        for DS in DS_List :                                                  # loop all available steering vectors (RFI and signal)
            # condition Beam-weights
            ds               = deepcopy(DS)                                      # create a copy to leave input data unchanged
            # open circuit voltages
            Vsig_oc          = self.Vnorm * np.conj(ds.Data)                     # open circuit voltage; simulation adds fields, therefore phase is shifted by 180deg to receiving
            # signal correlation
            Rsig_oc         += np.outer(np.conj(Vsig_oc), Vsig_oc)#.T, Vsig_oc)  # signal correlation matrix # NOTE transposing 1D array returns same 1D array
            self.BwNorm      = np.linalg.norm(ds.Data)                           # keep normalization of the signal steering vector
            self.Theta       = ds.Theta
            self.Phi         = ds.Phi

        self.RT['signal']    = np.dot(np.dot(self.Q, Rsig_oc),  self.QH)

        self.Vsig        = np.dot(self.Q, Vsig_oc.conj().T)
        self.Vsig_oc     = Vsig_oc

    #####################################
    # main functions to simulate noise performance
    def getBeamWeights            ( self, alg = "CFM" ) :
        """
        Get beam-weight vectors for the implemented beam-forming algorithms.

        Parameters:
            alg (string): Figure of merit to determine which beamforming algorithm to use.

        Returns:
            Bw (BeamWeightDataClass): Object containing beamweights.
        """
        self.printOut( "Calculating beamweights (algorithm ID = %s)" % alg, self.log.FLOW_TYPE )
        Bw               = BeamWeightDataClass.BeamWeights( log = self.log )
        Bw.Theta         = self.Theta
        Bw.Phi           = self.Phi
        Bw.Wavelength    = self.Wavelength
        
        if   alg == "SNR" :   # beamforming -- maximum signal to noise from noise matrix (not influenced by RFI)
            self.printOut( "  with maximum Signal to noise ratio beam-former from noise-resistance matrix", self.log.DATA_TYPE, False )
            Bw.Data          = np.dot(np.linalg.inv(self.RT['sys']), self.Vsig) #/ self.Vnorm
            Bw.Type          = BeamWeightDataClass._Types['SNR']
        
        elif alg == "MD"  :   # beamforming -- maximum directivity
            self.printOut( "  with maximum directivity beam-former from thermal noise resistance matrix", self.log.DATA_TYPE, False )
            Bw.Data          = np.dot(np.linalg.inv(self.R['t']), self.Vsig) #/ self.Vnorm
            Bw.Type          = BeamWeightDataClass._Types['MD']
        
        elif alg == "CFM" :   # beamforming -- conjugate field match
            self.printOut( "  with conjugate field match beam-former from signal response", self.log.DATA_TYPE, False )
            Bw.Data          = self.Vsig_oc.conj()
            Bw.Type          = BeamWeightDataClass._Types['CFM']

        else :
            # Default beam-former: "RFI"
            # beamforming -- full eigenvalue problem solving (max-snr with orthogonality to RFI)
            self.printOut( "  with maximum Signal to noise ratio beam-former from eigenvalue problem solving", self.log.DATA_TYPE, False )
            Ron              = self.RT['signal'] + self.RT['sys']
            Roff             = self.RT['sys']
            Roff_inv         = np.linalg.inv( Roff )
            RnRs             = np.dot( Roff_inv, Ron )
            # solve eigenvalue problem
            y, WT            = np.linalg.eig(RnRs)                               # solve eigenvalue problem for 
            W                = WT.T                                              #   Roff^-1Ron (y = eigenvalues, W = eigenvektor)
            # find correct eigenvector (there are RFI-eigenvectors too !!! Correct one has maximum dot product with snr-vector
            snrV             = np.dot(np.linalg.inv( self.RT['sys'] ), self.Vsig)     # beamforming -- maximum signal to noise from noise matrix
            snrV             = snrV / np.sqrt(np.dot(snrV, np.conj(snrV)))       # and normalize it
           
            # find best match :
            maxDot           = 0.

            for i, w in enumerate(W) :                                              # loop all eigenvectors
                wTest = w*y[i]
                wTest = wTest/np.linalg.norm(wTest)
                dot = np.dot(wTest, np.conj(snrV))                                    # build dot-product with max-snr
                dot = np.absolute(dot)                                                # use amplitude only
                if ( dot > maxDot ):
                    maxDot    = dot                                                      # use best vector as beam-forming vector
                    Bw.Data   = wTest                                                    # use it as BW-vector

            Bw.Type          = BeamWeightDataClass._Types['MSNR']
         
        # condition beam-weights and calculate noise performance 
        Bw.Data          = self.BwNorm*Bw.Data/np.linalg.norm(Bw.Data)       # normalize Beam-weights
        Bw               = self.calcNoiseTemperatures(Bw)                    # calculate noise performance of the BWs
        return Bw


    def calcNoiseTemperatures     ( self, Bw) :
        """
        Calculate all temperatures and efficiencies for a given beamweight vector Bw.

        Parameters:
            Bw (BeamWeightDataClass): Beam-weight class

        Returns:
            Bw (BeamWeightDataClass): Beam-weight class with belonging noise-temperatures and efficiencies
        """

        self.printOut( "Calculating noise performance of the BW-Vector given:", self.log.FLOW_TYPE )
        w       = Bw.Data                            # extract vector data and
        wT      = w.conj().T                         # its transposed

        # calculate efficiencies
        # total efficiency (from signal and total amount of thermal noise:
        eta_t   = np.absolute( np.real( np.dot( np.dot( wT, self.RT['signal'] ), w) / np.dot( np.dot( wT, self.R['t'] ), w ) ) )
        eta_t   = eta_t * _kB / ( 0.5 * self.Aphys * self.Sig * 1e-26 )
        self.printOut( "  eta_%s : %.2f" % ( ('total' + ' '*10)[:10], eta_t ), self.log.DATA_TYPE, False )

        # calculate efficiencies from losses
        etas    = {}                  # dictionary of all 'useful' efficiencies
        eta_all = 1.                  # all efficiencies contributing to the total efficiency (aperture efficiency is the only part unknown)
        eta_rad = 1.                  # all efficiencies of the receiver system are combined as radiation efficiency
        for key in self.R :

            if not key in ( ) : #'t', 'signal', 'sys', 'lna', 'allLoss') :
                etas[key]       = 1. - np.absolute( np.real( np.dot( np.dot( wT, self.R[key]  ), w ) / np.dot( np.dot( wT, self.R['t'] ), w ) ) )
                if key in ( 'sky', 't' ) :
                    etas[key]   = 1. - etas[key]

                if not key in ( 'sky', 'signal', 'sys', 'lna', 't', 'allLoss' ) :
                    eta_all *= etas[key]
                    if key in self.rxKeys :
                        eta_rad *= etas[key]

                self.printOut( "  eta_%s : %.2f" % ( (key+' '*10)[:10], etas[key] ), self.log.DATA_TYPE, False )

        # calculate noise temperature contributions from temperature weighted losses
        Ts      = {}                  # dictionary of all 'useful' noise contributions
        Trec = 0.
        for key in self.RT :
            if not key in ( 't', 'allLoss') :
                Ts[key]     = np.absolute( np.real( np.dot( np.dot( wT, self.RT[key] ), w ) / np.dot( np.dot( wT, self.R['t'] ), w ) ) )
                self.printOut( "  T_%s   : %.2f" % ( (key+' '*10)[:10], Ts[key] ), self.log.DATA_TYPE, False )
                if key in self.rxKeys :
                    Trec += Ts[key]

        self.printOut( "  eta_%s : %.2f" % ( ('rec' + ' '*10)[:10], eta_rad ), self.log.DATA_TYPE, False )
        self.printOut( "  T_%s   : %.2f" % ( ('rec' + ' '*10)[:10], Trec    ), self.log.DATA_TYPE, False )

        # extract spill-over efficiency (not necessarily available):
        eta_sp  = etas.get( 'telescope', 1. )
        Tsp     = Ts.get  ( 'telescope', 0. )

        # calculate aperture efficiency from all other relevant efficiencies (not 100% correct since parts of the spill-over will count for aperture efficiency)
        eta_ae   = eta_t / eta_all

        self.printOut( "  eta_%s : %.2f" % ( ('ae' + ' '*10)[:10], eta_ae ), self.log.DATA_TYPE, False )
        self.printOut( "  eta_%s : %.2f" % ( ('total' + ' '*10)[:10], eta_t ), self.log.DATA_TYPE, False )

        Tsys_oe = Ts['sys'] / eta_t
        self.printOut( "  T_%s   : %.2f" % ( ('sys/eta' + ' '*10)[:10], Tsys_oe ), self.log.DATA_TYPE, False )

        # set efficiencies and noise temperatures in the beam-weight class
        Bw.Efficiency.setEff( ae = eta_ae, sp = eta_sp, rd = eta_rad, to = eta_t )
        Bw.Noise.setTemp( tsig = Ts['signal'], tsky = Ts['sky'], tspill = Tsp, tloss = Ts['horn'], tlna = Ts['lna'], trec = Trec, tsys = Ts['sys'], tsysoe = Tsys_oe )

        return Bw
