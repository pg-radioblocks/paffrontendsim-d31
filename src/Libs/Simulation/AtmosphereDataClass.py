#!/usr/bin/env python3
"""
This file is part of the PAFFrontendSim.

The PAFFrontendSim is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License v3 as published by the Free Software Foundation.

The PAFFrontendSim is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with the PAFFrontendSim. If not, see https://www.gnu.org/licenses/.
"""
#############################################################################
# ToDo
# - embed elevation
#############################################################################
# import libs
#############################################################################
import numpy                          as np  
import matplotlib.pyplot              as plt
from   copy                           import deepcopy

import LogClass
from   Simulation.constants           import _Tcbg, _Tgrd, _C

#############################################################################
# constants
#############################################################################
_CLASSNAME     = "Atmosphere"

#############################################################################
# classes
#############################################################################
class Atmosphere :
    """
    This module is used to read atmospheric transmission data from a .csv-file.
    The data is an optional component of the telescopes environment and meant to improve accuracy.
    """
    #####################################
    # internal data and init method

    # init method
    def __init__        ( self, log = None ):
        """
        Initialize class.

        Parameters:
            log (LogClass): Documentation Class used by the calling application
        """
        if log == None:
            self.log = LogClass.LogClass(_CLASSNAME)
        else:
            self.log = deepcopy(log)             # logging class used for all in and output messages
            self.log.addLabel(self)

        self.DataSet = np.asarray(  [   # frequency  [GHz], transmission, Temperature [K]
                                    [  1.0, 0.99, 2.2],
                                    [100.0, 0.95, 7.4]
                                    ])

        self.printOut("Initializes Atmospheric-Data-Class", self.log.DEBUG_TYPE)
        return

    def printOut        ( self, line, message_type, noSpace = True, lineFeed = True ) :
        """
        Wrapper for the printOut method of the actual log class.
        """
        self.log.printOut(line, message_type, noSpace, lineFeed)

    #####################################
    # class-methods

    def readFile        ( self, filename ):
        """
        Method to read the atmosphere file.

        The file is expected to be in CSV with three columns (frequency [Ghz], transmittance [0-1], temperature [K]).

        Parameters:
            filename (string): Name of the input file
        """
        self.printOut("Reading atmosphere file %s" % filename, self.log.FLOW_TYPE)
        DataSet = []                         # delete all data

        with open(filename, 'r') as f :           # open file for reading
            for line in f :                           # and loop all lines in the file
                line = line.strip()                       # remove CR / NL / EOF from line end
                columns = line.split()                    # split line

                freq                 = float(columns[0])
                transmittance        = float(columns[1])
                temperature_planck   = float(columns[2])

                DataSet.append([freq, transmittance, temperature_planck])
        f.close()

        self.DataSet = np.asarray(DataSet)


    def SkyTransmission ( self, frequency ):
        """
        Return the interpolated sky temperature and sky transmittance at a given wavelength.

        Parameters:
            frequency (float): frequency of interpolated data
        """
        interpolated_transmittance = np.interp( frequency, self.DataSet[:, 0], self.DataSet[:, 1] )
        interpolated_temperature = np.interp( frequency, self.DataSet[:, 0], self.DataSet[:, 2] )

        return interpolated_transmittance, interpolated_temperature


    def plotData        ( self, freq_list = [], figname = "", show = False ):
        """
        Plot the atmospheric data.

        Transmittance and temperature over frequency.

        Parameters:
            freq_list (list): List of frequencies [GHz] at which the atmospheric data is calculated
            figname (string): file-name of the figure (if given figure is only shown on screen if show == True
            show (boolean):   boolean flag to show plot on screen even when being written to disk (True)

        """
        self.printOut("plotting atmosphere data", self.log.FLOW_TYPE)

        interpolated_transmittance = np.zeros((len(freq_list), 1))
        interpolated_temperature = np.zeros((len(freq_list), 1))

        for idx, freq_entry in enumerate(freq_list):
            transmittance, temperature = self.SkyTransmission(freq_entry)
            interpolated_transmittance[idx] = transmittance
            interpolated_temperature[idx] = temperature

        fig, ax_1 = plt.subplots()

        ax_1.plot(freq_list, interpolated_transmittance, label='Transmittance', color='blue')
        ax_1.set_xlabel('Frequency [GHz]')
        ax_1.set_ylabel('Transmittance', color='blue')
        ax_1.tick_params(axis='y', labelcolor='blue')

        ax_2 = ax_1.twinx()   # Create another axis sharing the same x-axis

        ax_2.plot(freq_list, interpolated_temperature, label='Temperature', color='red')
        ax_2.set_ylabel('Temperature [K]', color='red')
        ax_2.tick_params(axis='y', labelcolor='red')

        plt.title('Atmosphere transmittance and temperature over frequency')    # Add title
        fig.tight_layout()  # Adjust layout for labels

        transmittance_line, transmittance_label = ax_1.get_legend_handles_labels()  # Add legend
        temperature_line, temperature_label = ax_2.get_legend_handles_labels()
        ax_2.legend(transmittance_line + temperature_line, transmittance_label + temperature_label, loc='best')


        if show :
            plt.show()
        if figname != "" :
            fig.savefig(figname)
            plt.close()

