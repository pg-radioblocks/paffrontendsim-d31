#!/usr/bin/env python3
"""
This file is part of the PAFFrontendSim.

The PAFFrontendSim is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License v3 as published by the Free Software Foundation.

The PAFFrontendSim is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with the PAFFrontendSim. If not, see https://www.gnu.org/licenses/.
"""
#############################################################################
#
# Module for  the Telescope Class to create the Effelsberg telescope in dependence
# of the frequency. Background is that the outer 10m are a mash structure and are
# getting transperent for frequencies above approx. 20 GHz.
# This module creates a frequency depoendent spill-over area and restricts the
# usable surface area to 80 m diameter above 18 GHz. Above 58 GHz the mash is taken
# as fully transperent.
#
# This module provides the
# function to create the secondary focus telescope system. The primary focus system
# can be accessed via 'Effelsberg.py', making use of the 'modPrimary' function.
#
#############################################################################
# ToDo
#############################################################################
# - improve readability, add more comments
#
# - add better simulation of the mash, maybe by decreasing the active area with
#   frequency and mix it with spill-over
#
#############################################################################
# import libs
#############################################################################

import numpy as np

# import local libs
from Simulation.Telescope.Functions.modPrimary import modPrimary

# Antenna description string
_description_        = "Effelsberg 100m telescope, secondary focus with simulated mash (frequency dependent)"

def EffelsbergSec     ( telescope, wavelength, CalcSpill = True, RMS = 0., spillPrim = True, specs = {}) :
    """
    Creates the Effelsberg 100m telescope, secondary focus.

    Parameters:
        telescope   (TelescopeDataClass) : Telescope-data to be modified
        wavelength  (float)              : wavelength to adjust gridding
        CalcSpill   (bool)               : Shall the spill-over area be calculated and set for simulation
        RMS         (float)              : RMS of the surface (< 0 : uses telescope RMS as measured; >= 0: uses given number for the surface RMS)
        spillPrim   (bool)               : shall the primary spill-over region being used for the simulation (increasing calculation time massively for secondary focus telescopes)
    """

    telescope.printOut( "  Creating the Effelsberg 100m telescope for secondary focus and variable wavelength",  telescope.log.FLOW_TYPE )

    # check wavelength
    if wavelength <= 16.5:                    # if frequency is above 18 GHz don't use full 100m
        specs["kargs"]['TelTaper'] = -24           # set new edge-taper for the high frequency setup
        specs["kargs"]['TelDiam']  = 80000         # set new diameter
        
    telescope.createFromSpecs(specs, wavelength, CalcSpill, RMS, spillPrim )   # create the telescope
  
    # modify Primary spill-over regions if required
    if CalcSpill:
        modPrimary( telescope, wavelength, True, True , specs)
