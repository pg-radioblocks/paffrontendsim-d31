"""
This file is part of the PAFFrontendSim.

The PAFFrontendSim is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License v3 as published by the Free Software Foundation.

The PAFFrontendSim is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with the PAFFrontendSim. If not, see https://www.gnu.org/licenses/.
"""
# This module provides the main dish modification procedure 'modPrimary'
# The primary focus system
# can be accessed via 'Effelsberg.py', making use of the 'modPrimary' function.

import numpy as np
from   copy  import deepcopy

from   Simulation.UFO          import UFODataClass
from   Simulation.constants    import _Tgrd, _Tsky, _Tskatter, _C

def modPrimary        ( telescope, wavelength, spillPrim, sec, specs) :
    """
    Modify Effelsberg primary mirror (also used for the Effelsberg prime focus).

    Parameters:
        telescope   (TelescopeDataClass) : Telescope-data to be modified
        wavelength  (float)              : wavelength to adjust skatter temperature
        spillPrim   (bool)               : flag to indicte if the spill-over area of the primary is used in the simulation
        sec         (bool)               : flag to indicate if using primary focus or secondary
    """
    
    def modifySpill( spillarea ) :
        data              = []
        dataSkatter       = []
        for d in spillarea.Data :
            r                 = d.v.x**2 + d.v.y**2
            if r < 100000. :
                dataSkatter.append(d)
            else :
                data.append( d )

        spillarea.points    = len( data )
        spillarea.Data      = ( UFODataClass.UFODataPoint * spillarea.points    )( *data )
        
        spillSkatter        = deepcopy( spillarea )
        spillSkatter.points = len( dataSkatter )
        spillSkatter.Data   = ( UFODataClass.UFODataPoint * spillSkatter.points )( *dataSkatter )
        
        return spillarea, spillSkatter
    
    # selection between secondary and primary focus
    if sec :
        spillStep          =  0
    else:
        spillStep          = -1
    
    # Set skattering temperature (frequency dependent), if above 58 GHz, mash is taken as fully transparent
    freq                            = _C / wavelength / 1e6
    if freq >= 58. :
        return
    # calculate skatter temperature in dependence of the frequency : fully sky at 18 GHz, full ground > 58 GHz, in between linear interpolated
    TskatterGnd                     = _Tsky     + (_Tgrd     - _Tsky) * (freq - 18)/40.
    TskatterSky                     = _Tskatter - (_Tskatter - _Tsky) * (freq - 18)/40.
    
    # determine primary ground and sky spill-over regions
    sIdx = -1
    gIdx = -1
    for idx, name in enumerate( telescope.spillNames ) :
        print(idx, name, telescope.spillList[idx].Name )
        if name == 'ground' : 
            gIdx = idx
        if ( name == 'sky' ) and ( telescope.spillList[idx].Name == "Primary sky terminated spill-over area" ) :
            sIdx = idx
    
    if sIdx > -1 :
        telescope.printOut( "    modifying sky-spill-over region",  telescope.log.FLOW_TYPE )
        telescope.spillList[sIdx], skatterSky = modifySpill( telescope.spillList[sIdx] ) 
        
        telescope.spillList.append        ( skatterSky )
        telescope.spillStep.append        ( spillStep )
        telescope.spillTemp.append        ( TskatterSky )  # spill over terminated at calculated skattering temperature
        telescope.spillNames.append       ( 'skatter' )
    
    if gIdx > -1 :
        telescope.printOut( "    modifying ground-spill-over region",  telescope.log.FLOW_TYPE )
        telescope.spillList[gIdx], skatterGnd = modifySpill( telescope.spillList[gIdx] ) 
    
        telescope.spillList.append        ( skatterGnd )
        telescope.spillStep.append        ( spillStep )
        telescope.spillTemp.append        ( TskatterGnd )  # spill over terminated at calculated skattering temperature
        telescope.spillNames.append       ( 'skatter' )
