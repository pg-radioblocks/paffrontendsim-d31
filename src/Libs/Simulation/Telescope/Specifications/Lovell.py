"""
This file is part of the PAFFrontendSim.

The PAFFrontendSim is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License v3 as published by the Free Software Foundation.

The PAFFrontendSim is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with the PAFFrontendSim. If not, see https://www.gnu.org/licenses/.
"""
Lovell = {
		"Description": "Jodrell Bank Lovell telescope (prime focus, 76.2m)",
		"Module"     : "Parabolic",                  # use the internal Parabolic telescope constructor
		"kargs"      : {
						'TelRMS'         :      1.0, # nominal telescope surface RMS [mm] no numbers given, estimate
						'TelDiam'        :   76200., # telescope diameter (diameter of the primary mirror) [mm]
						'TelTaper'       :     -12., # nominal edge taper at the subreflector [dB]
						'TelPrimF'       :   29000., # focal length of telescope primary mirror [mm]
						'TelBlockage'    :    1000., # radius of central blockage [mm]; if negative, default value is calculated (size of the sub-reflector)
						'TelLegWidth'    :      -1., # leg-half-width [mm]; if negative, default value of 0. mm is taken (estimate from Foto)
						'TelLegRadius'   :      -1., # radius at which the support legs hit the subreflector [mm]; if negative, primary radius is taken
						'TelLegDistance' :       1., # radial distance at which the legs pass the primary focal point [mm], if negative, subreflector radius is taken
						'TelRotSupport'  :       0.  # Angle of the support of the secondary (0 : horizontal and vertical)
						}
		}
