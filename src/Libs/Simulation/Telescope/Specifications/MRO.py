"""
This file is part of the PAFFrontendSim.

The PAFFrontendSim is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License v3 as published by the Free Software Foundation.

The PAFFrontendSim is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with the PAFFrontendSim. If not, see https://www.gnu.org/licenses/.
"""
MRO = {
        "Description": "Metsähovi radio telescope in Finland",
        "Module"     : "GregorianOrCass",                  # use the internal Gregorian or Cassegrain telescope constructor
        "kargs"      : {
                        'TelRMS'         :       0.1,      # nominal telescope surface RMS [mm]
                        'TelDiam'        :   13700.,       # telescope diameter (diameter of the primary mirror) [mm]
                        'TelTaper'       :     -12.,       # nominal edge taper at the subreflector [dB]
                        'TelPrimF'       :    5080.,       # focal length of telescope primary mirror [mm]
                        'TelSecDiam'     :    1086.,       # diameter of the telescope secondary mirror [mm]
                        'TelSecR1'       :   -4238. / 11., # secondary mirror distance to primary focus (geometric) [mm]
                        'TelSecR2'       :    4238.,       # secondary mirror distance to telescope focus (geometric) [mm]
                        'TelBlockage'    :      -1,        # radius of central blockage [mm]; if negative, default value is calculated (size of the sub-reflector)
                        'TelLegWidth'    :      50.,       # leg-half-width [mm]; if negative, default value of 0. mm is taken
                        'TelLegRadius'   :    6000.,       # radius at which the support legs hit the subreflector [mm]; if negative, primary radius is taken
                        'TelLegDistance' :      -1 ,       # radial distance at which the legs pass the primary focal point [mm], if negative, subreflector radius is taken
                        'TelRotSupport'  :       0.        # Angle of the support of the secondary (0 : horizontal and vertical)
                        }
        }
