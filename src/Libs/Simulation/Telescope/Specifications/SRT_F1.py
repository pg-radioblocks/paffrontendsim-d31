"""
This file is part of the PAFFrontendSim.

The PAFFrontendSim is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License v3 as published by the Free Software Foundation.

The PAFFrontendSim is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with the PAFFrontendSim. If not, see https://www.gnu.org/licenses/.
"""
SRT_F1 = {
        "Description": "Sardinia Radio Telescope primary focus (F1) up to 20 GHz with best fit paraboloid",
        "Module"     : "Parabolic",                           # use the internal Parabolic telescope constructor
        "kargs"      : {
                        'TelRMS'         :       0.290,       # nominal telescope surface RMS [mm] with active surface control
                        'TelDiam'        :   64008.,          # telescope diameter (diameter of the primary mirror) [mm]
                        'TelTaper'       :     -12.,          # nominal edge taper at the subreflector [dB] (estimated)
                        'TelPrimF'       :   21023.,          # focal length of telescope primary mirror [mm] (best fit parabola)
                        'TelBlockage'    :    4560.,          # radius of central blockage [mm]; if negative, default value is calculated (size of the sub-reflector), use 0. for nullify effect of central blockage
                        'TelLegWidth'    :     225.,          # leg-half-width [mm]; if negative, default value of 0. mm is taken (estimate from Foto)
                        'TelLegRadius'   :   12./22*64008/2., # radius at which the support legs hit the subreflector [mm]; estimated from drawing
                        'TelLegDistance' :   4.1/22*64008/2., # radial distance at which the legs pass the primary focal point [mm]; estimated from drawing
                        'TelRotSupport'  :      45.           # Angle of the support of the secondary (0 : horizontal and vertical)
                        }
        }
