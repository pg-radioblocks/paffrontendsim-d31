"""
This file is part of the PAFFrontendSim.

The PAFFrontendSim is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License v3 as published by the Free Software Foundation.

The PAFFrontendSim is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with the PAFFrontendSim. If not, see https://www.gnu.org/licenses/.
"""
EffelsbergSec = {
                "Description"    : "Effelsberg 100m telescope secondary focus up to 18 GHz",
                "Module"         : "GregorianOrCass",                # use the internal Gregorian or Cassegrain telescope constructor
                "CreateWithFunc" : False,                            # use internal spec data for creation if a dedicated function for creation is available (optional, default == True)?
                "kargs"          : {
                                     'TelRMS'         :       0.550, # nominal telescope surface RMS [mm]
                                     'TelDiam'        :  100000.,    # telescope diameter (diameter of the primary mirror) [mm]
                                     'TelTaper'       :     -12.,    # nominal edge taper at the subreflector [dB]
                                     'TelPrimF'       :   30000.,    # focal length of telescope primary mirror [mm]
                                     'TelSecDiam'     :    6500.,    # diameter of the telescope secondary mirror [mm]
                                     'TelSecR1'       : 14305.0 - (14305.**2 - 7387.2**2 )**0.5, # secondary mirror distance to primary focus (geometric) [mm]
                                     'TelSecR2'       : 14305.0 + (14305.**2 - 7387.2**2 )**0.5, # secondary mirror distance to telescope focus (geometric) [mm]
                                     'TelBlockage'    :      -1,     # radius of central blockage [mm]; if negative, default value is calculated (size of the sub-reflector)
                                     'TelLegWidth'    :    1250.,    # leg-half-width [mm]; if negative, default value of 0. mm is taken
                                     'TelLegRadius'   :   22000.,    # radius at which the support legs hit the subreflector [mm]; if negative, primary radius is taken
                                     'TelLegDistance' :    3500.,    # radial distance at which the legs pass the primary focal point [mm], if negative, subreflector radius is taken
                                     'TelRotSupport'  :       0.     # Angle of the support of the secondary (0 : horizontal and vertical)
                                   }
                }
