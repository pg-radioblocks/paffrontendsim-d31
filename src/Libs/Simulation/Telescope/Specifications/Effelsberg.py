"""
This file is part of the PAFFrontendSim.

The PAFFrontendSim is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License v3 as published by the Free Software Foundation.

The PAFFrontendSim is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with the PAFFrontendSim. If not, see https://www.gnu.org/licenses/.
"""
Effelsberg = {
            "Description"    : "Effelsberg 100m telescope primary focus up to 20 GHz",
            "Module"         : "Parabolic",                     # use the internal Parabolic telescope constructor
            "CreateWithFunc" : False,                           # use internal spec data for creation if a dedicated function for creation is available (optional, default == True)?
            "kargs"          : {
                                'TelRMS'         :       0.550, # nominal telescope surface RMS [mm]
                                'TelDiam'        :  100000.,    # telescope diameter (diameter of the primary mirror) [mm]
                                'TelTaper'       :     -12.,    # nominal edge taper at the subreflector [dB]
                                'TelPrimF'       :   30000.,    # focal length of telescope primary mirror [mm]
                                'TelBlockage'    :    3250.,    # radius of central blockage [mm] (estimate)
                                'TelLegWidth'    :    1250.,    # leg-half-width [mm]; if negative, default value of 0. mm is taken (estimate)
                                'TelLegRadius'   :   22000.,    # radius at which the support legs hit the subreflector [mm]; if negative, primary radius is taken (estimate)
                                'TelLegDistance' :    3500.,    # radial distance at which the legs pass the primary focal point [mm] (estimate)
                                'TelRotSupport'  :       0.     # Angle of the support of the secondary (0 : horizontal and vertical)
                               }
            }
