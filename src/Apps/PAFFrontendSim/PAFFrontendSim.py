#!/usr/bin/env python3
"""
This file is part of the PAFFrontendSim.

The PAFFrontendSim is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License v3 as published by the Free Software Foundation.

The PAFFrontendSim is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with the PAFFrontendSim. If not, see https://www.gnu.org/licenses/.
"""
#############################################################################
# import libs
#############################################################################
from pathlib import Path
import sys
import argparse

if __name__ == "__main__":  # if the module is being run independently
    file_path = Path(__file__).resolve()  # get the path of this file
    full_path_tuple = file_path.parts  # split the path into its components

    root_index = full_path_tuple.index("paffrontendsim-d31")     # determine at what position in the path the root folder is
    root_path_tuple = full_path_tuple[:root_index+1]  # store the path of the root folder
    publicLibsPath = Path(*root_path_tuple, Path("src", "Libs"))  # reassemble the root path into a string
    sys.path.append(str(publicLibsPath))  # add the path to sys.path


import LogClass
from FileIO                           import HDF5Writer
from Simulation.Telescope             import TelescopeDataClass
from Simulation                       import SignalChainDataClass
from Simulation                       import FarFieldDataClass
from Simulation                       import ArrayDataClass
from Simulation.constants             import _C

#############################################################################
# constants
#############################################################################

_PROGNAME    = "PAFFrontendSim"

#############################################################################
# main simulation class definition
#############################################################################
class SimulationRunnerPafFrontend():
    """
    This Python script is considered a reference implementation to perform a full simulation.

    Upon execution, a folder structure is created for input and output files, and various modules are called with the necessary parameters.
    The parameters are default values stored here. They can be modified by passing arguments to the script.
    The list of arguments can be found at the bottom of this script.
    In case a full simulation is not desired, a new script can be created to call only the required modules.
    """
    def __init__(self) -> None:
        """
        Declares variables with default values for the simulation.

        This includes file paths, names, physical dimensions, astro source characteristics, sky conditions, simulation flags and more.
        """
        # input and output folder definitions
        self.inputFolder     = "defaultInput"
        self.outputFolder    = "defaultOutput"

        self.logVerbosity    = "run"    # "data" "verbose"  # logging verbosity

        self.season          = "annual" # season of amotsphere data (annual, DJF, MAM, JJA, SON)
        self.pwv             = "50"     # precipitable wator vapor percentage of atmosphere

        # Simulation data
        self.TelescopeName  = "Effelsberg"                                 # name of the telescope, see TelesopeDataClass for possible choices
        self.Elevation      = 40                                           # observing elevation
        self.SourcePol      = 45                                           # polarization of the astronomical source

        # PAF data
        self.ArrayRadius     = 50#268                                      # radius of the PAF array (physical) [mm]
        self.Spacing         = 42                                          # define spacing [mm]
        self.ArrayGeo        = "squared"                                   # geometry of the array pattern
        self.Loss            = 0.20                                        # losses in front of the LNA (e.g. RFI filter)
        self.Matching        = 50                                          # matching between LNA and antenna:
                                                                           #   -1 : lossless matching network
                                                                           #    0 : no matching, using the existing missmatch
                                                                           #  > 0 : using the given number as antenna impedance (lossless matching of antenna to this impedance)

        self.UseSigChainFile    = False                                    # whether or not a file should be used to define the signal chain
        self.LNA                = "lna.s2p"                                # name of an optional LNA specification file

        # Farfield data. The default is a dipole with groundplane.
        self.UseFarfieldFile    = False                                    # whether or not a file should be used to define the farfield
        self.MakeGaussianFF     = False                                    # whether or not a Gussian farfield should be generated
        self.FarFieldWc         = "*.*_GHz.ffs"                            # wildcard pattern for far-field files
        self.FrequencyListGauss = [2.9+i*0.2 for i in range(3)]            # list of frequencies being used for Gaussian illumination (not using FarFieldfile)

        # test-specification
        self.TestDeadElement    = False #True                              # enable / disable dead element testing
        self.TestRFI            = False #True                              # enable / disable RFI test

        # output data
        self.WriteReport = False                                           # whether or not a report is written
        self.ReportName  = "{\\it Effelsberg 1$^{st}$ Generation Cryo-PAF}"# name of the report (LaTex notation posible, but use '\\' instead of '\' )

        # Other data not accessible via comand line
        self.wavelength      = 50                                          # define initial wavelength [mm], mainly to create the telescope (should correspond to at least the highest frequency used)
        self.SimWavelength   = 100                                         # default simulation wavelength

        self.rxXpol          = True                                        # enables x-polarization of the receiver
        self.rxYpol          = True                                        # enables y-polarization of the receiver


    def prepareDataFolders(self):
        """
        Prepares and defines the folder structure for input and output files.
        If a folder does not exist yet, it is created.
        If the output folder already exists and isn't empty, its contents are deleted. Be aware of data loss!
        Creates an object of LogClass to enable logging. This object is passed down the object hierarchy. See LogClass for more information.
        """
        file_path = Path(__file__).resolve()                                        # get the path of this file
        full_path_tuple = file_path.parts                                           # split the path into its components
        root_index = full_path_tuple.index("paffrontendsim-d31")                  # determine at what position in the path the root folder is
        root_path_tuple = full_path_tuple[:root_index+1]                            # store the path of the root folder

        self.dataFolder = Path(*root_path_tuple, "simData")
        self.Input_path = Path(self.dataFolder, "InputFolder", self.inputFolder)    # folder containing files used for data input
        self.CSTInput = Path(self.Input_path, "CST_Files")                          # put your CST files here
        self.SignalChains = Path(self.Input_path, "LNA_Files")                      # put your LNA files here
        self.Atmosphere_path = Path(self.Input_path, "Atmosphere_Files")            # put your atmosphere files here
        # self.AtmosphereFile  = Path(self.Atmosphere_path, "atm.out")              # atmospheric tranmission data # TODO

        self.Results = Path(self.dataFolder, "OutputFolder", self.outputFolder)     # folder for output files
        # self.Farfield = Path(self.Results, "FarFieldPattern")
        self.LogPath = Path(self.Results, "Logs")
        
        self.HDF5Path = Path(self.Results, _PROGNAME + ".h5")

        Path.mkdir(self.CSTInput, exist_ok=True, parents=True)  # create directories if they don't exist
        Path.mkdir(self.Atmosphere_path, exist_ok=True)
        Path.mkdir(self.SignalChains, exist_ok=True)
        Path.mkdir(self.Results, exist_ok=True, parents=True)
        Path.mkdir(self.LogPath, exist_ok=True)
        
        [f.unlink() for f in self.LogPath.iterdir() if f.is_file() and f.suffix == ".log"]                          # clean up logs
        [f.unlink() for f in self.Results.iterdir() if f.is_file() and f.suffix in [".dat", ".png", ".mov", ".h5"]] # clean up other files
        
        self.log             = LogClass.LogClass(_PROGNAME, logPath = self.LogPath)                # set console debug class
        self.log.setScenario(self.logVerbosity) #('data') #('run')

        self.dataWriter = HDF5Writer.HDF5Writer(outputFile = self.HDF5Path, log = self.log)                       # Set up HDF5Writer


    def setupSimulation(self):
        """
        Creates objects of various classes (found in the Lib folder) that are needed for the simulation.
        They are created according to the variables of __init__.
        This includes TelescopeDataClass.Telescope, SignalChainDataClass.SignalChain, FarFieldDataClass.FarFieldData, ArrayDataClass.ArrayData and ArrayDataClass.ChainData.
        """
        # Setup simulation
        #############################################################################
        # set and establish the telescope
        self.log.printOut      ( "Setup telescope", self.log.USER_TYPE, False )
        self.telescope             = TelescopeDataClass.Telescope( log = self.log )
        self.telescope.Elevation   = self.Elevation
        self.telescope.setTelescope( self.TelescopeName, self.wavelength)#, self.Results )
        #self.telescope.importAtmosphere( self.Atmosphere_path, self.TelescopeName, self.season, self.pwv)
        self.telescope.plotTelescope( Path(self.Results, "telescope") )             # plot telescope setup

        # Setup PAF
        #############################################################################
        # define antenna pattern
        self.log.printOut( "Defining element antenna pattern", self.log.USER_TYPE, False )
        ff = FarFieldDataClass.FarFieldData( self.SimWavelength, raster = 1, log = self.log )

        if self.UseFarfieldFile == True:
            ff.setFolder( self.CSTInput, wildcard = self.FarFieldWc )   # single farfield source files
            #ff.setCST(CSTInput, FarField)  # default: open CST project
        else:
            if self.MakeGaussianFF == False:
                ff.createFarfield(Type = "Dipole", intensity = 1, antennaLength = 103.3767) # 103.3767mm antennaLength for 2.9GHz
            else:
                ff.createFarfield(Type = "Gaussian", intensity = 1, width = 80)
            ff.freqDict = {}
            for f in self.FrequencyListGauss:
                ff.freqDict["%.2f" % f] =  None

        self.frequencyList = sorted(list(ff.freqDict.keys()))

        ff.plotOpeningAngle( Path(self.Results, "openingAngle_chain_0") )                                # make farfield opening angle plot

        # set up array
        self.element_array = ArrayDataClass.ArrayData( wavelength = self.SimWavelength, log = self.log, dataWriter = self.dataWriter ) # set default wavelength
        self.element_array.setSpacing( self.Spacing )                                                    # set array-spacing
        self.element_array.elementSize = 0.85*self.Spacing                                               # set element size (only for plotting)


       # create signal chain data
        self.log.printOut      ( "Setup signal chain", self.log.USER_TYPE, False )
        SignalChain          = SignalChainDataClass.SignalChain( log = self.log )

        if self.UseSigChainFile == True:
            SignalChain.name     = self.LNA.split( '.' )[0]
            SignalChain.readFile  (str(Path(self.SignalChains, self.LNA)))
        else:
            F                    = [1+i*0.05 for i in range(61)]
            Tmin                 = [2.5, 2.0, 2.0, 2.3, 2.5]
            Rn                   = [0.35, 0.55, 0.45, 0.35, 0.3]
            Zopt                 = [28.5+24.5j, 13.3- 7.2j, 18.0-20.4j, 27.4-29.0j, 40.9-30.0j]
            SignalChain.makeDataset( "My_LNA", F, Tmin, Zopt, Rn )

        SignalChain.selectFreq( 1, 4 )

        # define Array geometry
        self.log.printOut( "Defining array geometry", self.log.USER_TYPE, False )

        if self.rxXpol :    # X polarization
            # create pattern
            antenna_coordinate_list = self.element_array.createPattern(
                elements       = 1,
                limitingRadius = self.ArrayRadius,
                patternType    = self.ArrayGeo,
                offset         = (-self.Spacing/2, -self.Spacing/2),
                rotation       = 45,
                new            = True,
                shuffleRadius  = 0,
                step           = self.Spacing
                )
            self.element_array.add_antennas( antenna_coordinate_list )

            signalchain      = ArrayDataClass.ChainData( SignalChain, ff, pol = 0, phase = 0 )
            signalchain.loss = self.Loss
            signalchain.farfield.scaleI0()
            self.element_array.add_signalchains( signalchain, self.SimWavelength, select = lambda el : el.noChains == 0 )


        if self.rxYpol :    # Y polarization
            # create pattern
            antenna_coordinate_list = self.element_array.createPattern(
                elements       = 1,
                limitingRadius = self.ArrayRadius,
                patternType    = self.ArrayGeo,
                offset         = ( 0,  0),
                rotation       = 45,
                new            = False,
                shuffleRadius  = 0,
                step           = self.Spacing
                )
            self.element_array.add_antennas( antenna_coordinate_list )

            signalchain      = ArrayDataClass.ChainData( SignalChain, ff, pol = 90, phase = 0 )
            signalchain.loss = self.Loss
            signalchain.farfield.scaleI0()
            self.element_array.add_signalchains( signalchain, self.SimWavelength, select = lambda el : el.noChains == 0 )

        # define matching
        if self.Matching < 0:
            self.Matching  = -1  # set the matching between antenna and LNA

        IDList, positionList, polarizationList = self.element_array.getArraySpecs()  # adds array specifications to h5 file

        self.dataWriter.setMetadata(self.Matching, self.Loss, IDList, positionList, polarizationList, self.ArrayGeo, self.ArrayRadius, self.Spacing, self.Elevation, self.TelescopeName)


    def runSimulation(self):
        """
        Simulates a PAF by executing the methods of the objects defined in setupSimulation().
        The full simulation (excluding deadelements and RFI) is performed within self.element_array.EM_Simulation.
        After the full simulation has concluded, a DeadElement-test and/or RFI-test are performed if their flags are set.
        """

        self.log.printOut( "Simulate array", self.log.USER_TYPE, False )
        FoVList, ArrayPlots = self.element_array.EM_Simulation  ( self.telescope, SourcePol = self.SourcePol, matching = self.Matching, resultFolder = self.Results )

        # select FoV-data class closest to simulation wavelength
        if self.TestRFI or self.TestDeadElement :
            minDiff = 1e99
            FoV     = FoVList[0]
            for fovIdx, fov in enumerate(FoVList) :
                diff = abs( self.SimWavelength - fov.SimWavelength )
                if diff < minDiff:
                    minDiff = diff
                    freqRFI = self.frequencyList[fovIdx]
                    FoV     = fov

        if self.TestRFI:
            FoV.RFI_test( matching = self.Matching, numberOfSimulations = 10, maxRFISources = -1, ResultPath = self.Results )    # do a RFI-tolerance check

        if self.TestDeadElement:
            FoV.deadElementTest( matching = self.Matching, numberOfSets = 10, maxLoops = -1, ResultPath = self.Results )         # do a dead element check



if __name__ == "__main__":      # if this is the main module being executed by the Python interpreter
    sim = SimulationRunnerPafFrontend()     # create an object of the simulator-class with default settings

    ##############################################
    ##############################################
    ##############################################
    ##############################################

    arg_pars = argparse.ArgumentParser(description='PAF frontend simulator (MPIfR)')

    # define arguments that may be passed to this module, which modify various settings for this simulation run:

    # simulation data:
    ##############################################
    arg_pars.add_argument('--TelescopeName',                           default = sim.TelescopeName,                  help = '(string), name of the telescope as defined in TelescopeDataClass (e.g. Effelsberg, MRO, APEX)' )
    arg_pars.add_argument('--Elevation',                               default = sim.Elevation,          type=float, help = '(float), elevation angle [deg] of the observation simulated (for e.g. ground pick-up)' )
    arg_pars.add_argument('--SourcePol',                               default = sim.SourcePol,          type=float, help = '(float), polarization angle [deg] of the used astronomical point source (0° = cross-elevation)' )
    arg_pars.add_argument('--SimWavelength',                           default = sim.SimWavelength,      type=float, help = '(float), wavelength [mm] used for e.g. RFI-testing or dead element test' )
    # PAF-data:
    ##############################################
    arg_pars.add_argument('--ArrayRadius',                             default = sim.ArrayRadius,        type=float, help = '(float), radius covered by the array elements [mm]')
    arg_pars.add_argument('--Spacing',                                 default = sim.Spacing,            type=float, help = '(float), lateral spacing of the array elements [mm]' )
    arg_pars.add_argument('--ArrayGeo',                                default = sim.ArrayGeo,                       help = '(string), array geometry specifier (e.g. squared, hexagonal, vogel)' )
    arg_pars.add_argument('--Matching',                                default = sim.Matching,           type=float, help = '(float), < 0: matching strategy between antenna and signal chain; > 0: matching impedance [Ohm]')
    arg_pars.add_argument('--Loss',                                    default = sim.Loss,               type=float, help = '(float), losses in percentil of the optics or e.g. RFI filters in front of the LNA' )
    arg_pars.add_argument('--UseSigChainFile',    action="store_true",                                               help = '(bool), experimental, using pre-defined LNA data e.g. from measurements' )
    arg_pars.add_argument('--UseFarfieldFile',    action="store_true",                                               help = '(bool), experimental, using e.g. simulated antenna pattern from CST' )
    arg_pars.add_argument('--MakeGaussianFF',     action="store_true",                                               help = '(bool), generate a Gaussian farfield instead of the default Dipole' )
    arg_pars.add_argument('--FrequencyListGauss', nargs='+',           default = sim.FrequencyListGauss, type=float, help = '(list of float), frequencies [GHz] for the Gaussian illumination (UseFarfieldfile == False)')
    # test-specifications:
    ##############################################
    arg_pars.add_argument('--TestDeadElement',    action="store_true", default = sim.TestDeadElement,                help = '(bool), preform a dead element test for the array' )
    arg_pars.add_argument('--TestRFI',            action="store_true", default = sim.TestRFI,                        help = '(bool), perform a RFI-mitigation capability test for the array' )
    # file input and output:
    ##############################################
    arg_pars.add_argument('--inputFolder',                             default = sim.inputFolder,                    help = '(string), name of the input folder' )
    arg_pars.add_argument('--outputFolder',                            default = sim.outputFolder,                   help = '(string), name of the output folder' )
    arg_pars.add_argument('--logVerbosity',                            default = sim.logVerbosity,                   help = '(string), verbosity of log printing to console' )

    ##############################################
    ##############################################
    ##############################################
    ##############################################

    args   = arg_pars.parse_args()  # parse all the arguments that were passed to this module
    params = vars(args)

    for k, v in params.items():     # go over each parameter that was passed
        if hasattr(sim, k) and (v != False):         # if the sim has such a variable
            setattr(sim, k, v)      # overwrite it with the passed parameter


    sim.prepareDataFolders()    # create and clean up the data folders for the simulation
    sim.setupSimulation()       # define objects of various library-classes for the simulation
    sim.runSimulation()         # run the simulation
