Git
===================================

This page seeks to give a basic overview of the collaborative workflow of the project using git.

If you wish to contribute, please create you own branch and commit your changes to it.
Don't forget to push your changes to the remote copy of your branch using `git push`.
Perform `git pull` in any branch to update it with its respective remote branch.
Once you feel like your feature or fix is complete, you may request to merge your branch back into the main branch.


Merge request (web interface):
	On the GitLab web interface go to Project (menu on the left) > Code > Merge requests.
	There click on the blue button labeled "New merge request".
	Select your branch in the "source branch" section on the left. The target branch should be preselected as "main".
	Press the button labeled "Compare branches and continue".
	Now you will write a fitting title and description for your merge request.
	Optionally adjust the other settings, for example "Delete source branch when merge request is accepted".
	Create the merge request with the button labeled "Create merge request".

	If there are conflicts:
		Press the button labeled "Resolve conflicts".

		You will be presented with all the conflicting code sections.
		In most cases one of the two conflicting code sections is obsolete.
		You can simply pick the more recent one by clicking "Use ours", for your code, or "Use theirs", for the code in the target branch.
		If the conflict is more complex, click "Edit inline".
		In this case the sections are surrounded by conflict markers.
		The first code section starting from `<<<<<<< (filename)` is your code.
		The second code section from `=======` to `>>>>>>> (filename)` is the target branches code.
		Modify these sections to reconcile the conflict.
		Don't forget to remove the conflict markers.

		BEWARE: There might be a bug in GitLab that flips the source and target branch in the commit message. Change it as to not cause confusion. You are merging YOUR-BRANCH into main.

		When all conflicts have been resolved, modify the commit message to describe what you have done and press "commit to source branch"

This merge request will then be reviewed by a maintainer of the project and hopefully approved.

If the main branch has received a merge from another branch, your branch will be out-of-date as a result.
If you haven't made any changes in your branch, it is easist to simply delete your branch and create a new one.
If you have made changes to your branch, you don't have to worry as any conflicts will be resolved during your merge request.
If are not ready to merge yet, but want update your branch with the changes from main, you may perform a rebase.

Rebase (local):
	A rebase applies the new commits from the main branch to your branch, while checking for conflicts.
	It is performed locally, so make sure the main branch of your local repository is up-to-date with the remote repository (`git pull`).

	To start the rebase process run `git rebase origin/main` in your branch.

	If there are conflicts, open the conflicting file and search for the conflict markers:

	`<<<<<<< HEAD`

	(code changes in the target branch)

	`=======`

	(code changes in your branch)

	`>>>>>>> YOUR-BRANCH or YOUR-COMMIT`

	See how you can reconcile the conflict by combining the two code sections.
	Usually one of the two code sections is obsolete and can be removed.
	Otherwise the two code sections are unrelated to each other and may coexist.
	In rare cases there is an actual conflict because the code sections serve the same purpose but produce different outcomes.
	Then you will have to determine what the best course of action is.
	You may prefer one code section over the other, or write new code that combines both solutions.

	When the conflict has been resolved, remove the conflict markers, as they don't belong in the code and commit the changes you have made.
	There is no need to push the commit as the rebase is performed locally for now.
	Continue the rebase with `git rebase --continue`.
	If there is another conflict resolve it the same way and continue until the rebase is successful.
	If you are satisfied with the result, you may `git push` your updated local branch to the remote branch.
