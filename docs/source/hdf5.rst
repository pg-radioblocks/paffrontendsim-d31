HDF5
===================================

HDF5 is a file format that allows data structures to be stored in an efficient way.
The simulator contains a module called h5writer.py to store the ACMs (array covariance matrices) it generates in said format.
This module uses an external Python library called h5py to create these files.
For detailed information on said library and the HDF5-format visit https://docs.h5py.org/en/stable/

The internal structure of the hdf5-file is shown below.
There are further attributes in each group and dataset to define units.
There are also source descriptions and names in the form of attributes within the group for each source.
The latter are identical to the group name.

File structure:

	/commit ``Attribute to denote the software version.``

	/creation_date_utc ``Attribute of the file creation timestamp in UTC.``

	/acms ``Group for the ACM datasets.``
		/noise ``Group containing the noise data.``
			/<id> ``Usually just ID 0.``
				/<noise source> ``For example LNA, atmosphere, telescope.``
					/frequencies ``List of frequencies simulated, in GHz.``

					/acm ``XYZ array. X is the frequency as in /frequencies. Y and Z is the ACM.``

		/rfi ``Group containing the RFI data.``
			/<id> ``One unique ID for each RFI source.``
				/frequencies ``List of frequencies simulated, in GHz.``

				/coordinates ``The phi, theta coordinate of the RFI source.``

				/acm ``XYZ array. X is the frequency as in /frequencies. Y and Z is the ACM.``

		/signal ``Group containing the astronomical signal data.``
			/<id> ``One unique ID per astro source.``
				/frequencies ``List of frequencies simulated, in GHz.``

				/ppa ``source polarization``

				/acm ``XYZ array. X is the frequency as in /frequencies. Y and Z is the ACM.``

	/array ``Group for the PAF geometry data.``
		/antenna_matching ``Impedance.``

		/antenna_return_loss ``Loss in front of LNA, for example RFI filter.``

		/element_ids ``List of IDs to identify the antennas.``

		/element_polarisations ``List of polarizations of each antenna.``

		/element_positions ``List of XY positions of every antenna on the plane of the PAF.``

		/geometry ``The geometry of the antenna array, for example square, honeycomb, etc.``

		/radius ``Maximum radius of the PAF plane. All antennas are within it.``

		/spacing ``Antenna spacing on the PAF plane.``

	/telescope ``Group for telescope information.``
		/elevation ``Altitude angle of telescope relative to the ground. Important for spill-over.``

		/name ``Name of the telescope, for example Effelsberg.``
