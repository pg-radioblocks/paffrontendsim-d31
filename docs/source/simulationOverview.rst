Simulation Overview & Sequence
===================================

The simulator consists of over a dozen modules, each representing a necessary (or optional) component of the simulation.

The following figure gives an overview of how the modules work together.


.. image:: modules.png


The generated pages of the modules can be found under `Libs`. They provide more detailed information.

A simulation is started using `PAFFrontendSim.py`.
This module is a reference implementation of an app that is able to call the simulator's modules and perform a full simulation with predefined parameters.

The app begins to declare object of the necessary modules and create a folder structure for the output data.

This includes the telescope structure and sky/spill-over via `TelescopeDataClass.py`, the PAF via `ArrayDataClass.py`, the signal chain(s) via `SignalChainDataClass.py` and the farfield data via `FarfieldDataClass.py`.

After the necessary components for the simulation have been defined, the app will call the method `EM_Simulation` of `ArrayDataClass.py` in order to perform the simulation.

If specified, the app may call the methods `RFI_test` and/or `deadElementTest` of the `FoVDataClass.py` module in order to repeat parts of the simulation with RFI sources and/or dead antennas respectively.
