# Configuration file for the Sphinx documentation builder.
#
# For the full list of built-in configuration values, see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information

import os
import sys

rootPath = os.path.abspath("../..")

libsPath = os.path.sep.join([rootPath, "src", "Libs"])
appsPath = os.path.sep.join([rootPath, "src", "Apps"])


sys.path.append(rootPath)
sys.path.append(libsPath)
sys.path.append(appsPath)

project = 'PAFFrontendSim'
copyright = '2024, Yanik Yiannakis'
author = 'Yanik Yiannakis'
release = '1.0'

# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration

extensions = [
    'sphinx.ext.autodoc',
    'sphinx.ext.viewcode',
    'sphinx.ext.napoleon'
]

autoclass_content = 'both'

templates_path = ['_templates']
exclude_patterns = []

napoleon_custom_sections = [('Returns', 'params_style')]

# -- Options for HTML output -------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#options-for-html-output

pygments_style = 'sphinx'
html_theme = 'sphinx_rtd_theme'
html_static_path = ['_static']
