Introduction
===================================

The PAF Design simulator (or PAF Frontend simulator) is a project released under the GNU GPLv3 licence.

This project seeks to offer a tool to assess the performance of a PAF under various conditions.
The conditions include the specifications of the PAF, the telescope its installed in, the signal chain, environmental conditions and more.
The simulator is only intended to represent the frontend and no backend components are included, though beams may be formed.

The vast majority of the code is written in Python.
The physical optics simulation (of the signal path from antenna array, over the dish structure and onto the sky/spill-over) is written in C.
This is due to the computational complexity.

A single physical optics simulation of the full signal path is valid for only one frequency and one antenna in the PAF.
Simulating the whole antenna array therefore requires one simulation per antenna.
This means there is a computational limitation for how large the PAF can be.
As mentioned, a simulation also only considers one frequency.
Therefore you need to rerun the simulation for each additional frequency.
The frequency resolution and width of your band is another computational limitation.

The total amount of full signal path simulations is N = noFrequencies * noAntennas.

The Python code is currently run on one thread, while the C code is parrallelized.
Your computation time mainly depends on the total processing power of your CPU.
The simulator does not require much disk space or memory (RAM).


Before committing to simulate a large PAF or a wide band, you should perform trial runs to determine if the total computation time is acceptable to you.
Making the frequency resolution a little too high or the PAF a little too large can cause the computation time to explode.
The timescale can range from a few minutes to multiple hours, or in extreme examples an entire weekend.

If you are unwilling to reduce the simulation setup, but need to save time, it might be viable to skip certain steps of the simulation or reduce their complexity.
Be aware that, for now, this is not recommended and considered a hack.
Your results could become inaccurate and your changes are likely not stored in their metadata.

Any optimizations are unlikely to be straightforward.
This is due to the fact that physical optics simulations have no shortcuts.
The C code is already set up in a way that takes maximum advantage of x86 processors and their instructions to speed up parallel processing (SIMD, etc.).
An alternative would be to use GPUs instead of CPUs. In this case the C code would have to be ported to something like CUDA.
Currently (July 2024) there have not been any plans, as there has not been a need for it.
