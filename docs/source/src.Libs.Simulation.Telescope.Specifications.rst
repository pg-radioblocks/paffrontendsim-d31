src.Libs.Simulation.Telescope.Specifications package
====================================================

Submodules
----------

src.Libs.Simulation.Telescope.Specifications.APEX module
--------------------------------------------------------

.. automodule:: src.Libs.Simulation.Telescope.Specifications.APEX
   :members:
   :undoc-members:
   :show-inheritance:

src.Libs.Simulation.Telescope.Specifications.Effelsberg module
--------------------------------------------------------------

.. automodule:: src.Libs.Simulation.Telescope.Specifications.Effelsberg
   :members:
   :undoc-members:
   :show-inheritance:

src.Libs.Simulation.Telescope.Specifications.EffelsbergSec module
-----------------------------------------------------------------

.. automodule:: src.Libs.Simulation.Telescope.Specifications.EffelsbergSec
   :members:
   :undoc-members:
   :show-inheritance:

src.Libs.Simulation.Telescope.Specifications.Lovell module
----------------------------------------------------------

.. automodule:: src.Libs.Simulation.Telescope.Specifications.Lovell
   :members:
   :undoc-members:
   :show-inheritance:

src.Libs.Simulation.Telescope.Specifications.MRO module
-------------------------------------------------------

.. automodule:: src.Libs.Simulation.Telescope.Specifications.MRO
   :members:
   :undoc-members:
   :show-inheritance:

src.Libs.Simulation.Telescope.Specifications.SRT\_F1 module
-----------------------------------------------------------

.. automodule:: src.Libs.Simulation.Telescope.Specifications.SRT_F1
   :members:
   :undoc-members:
   :show-inheritance:

src.Libs.Simulation.Telescope.Specifications.SRT\_F2 module
-----------------------------------------------------------

.. automodule:: src.Libs.Simulation.Telescope.Specifications.SRT_F2
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: src.Libs.Simulation.Telescope.Specifications
   :members:
   :undoc-members:
   :show-inheritance:
