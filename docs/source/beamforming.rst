Beamforming
===================================

This page explains beamforming in the context of PAFs.
The pages FoV and Optics should be read beforehand.

The synthezised beam of a PAF is based on the weighting of the innate beams of the PAF.
"Innate beams" in this context refers to the unweighted, natural beams that the individual antennas project into the farfield through the telescopes optics.

Before a beam is synthesized, the FoV is rastered (ideally Nyquist sampling) to determine all possible coordinates that a beam can point to.
The number of possible positions is not equal to the number of innate beams, nor do their coordinates on sky match.

The beam weighting may be based on any desired algorithms or factors.
They include steering, RFI supression, optimization for SnR, directivty and more.

The PAF Frontend simulator currently has three beamforming algorithms, each with a different figure of merit to optimize.
They are signal to noise (SNR), maximum directivity (MD) and conjugate field match (CFM).
They algorithm for SNR can optionally take RFI into account.
