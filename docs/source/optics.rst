Optics
===================================

This page explains the optics of radio telescopes and presumes an understanding of geometrical and physical optics.

The reflective dishes of a typical radio telescope gather and focus the incoming radio waves at a focal point.
When observing, the telescope is steered in order to align the optical axis (aka boresight) with the object of interest on sky.
The receiving antenna is located in the focal point.
In the case of multiple antennas/horns, they are distributed on the focal plane.
The focal plane is perpendicular to the optical axis.

Instead of an antenna, there may be an intermediate optical system at the focal point instead.
These optical systems typically consist of lenses, mirrors and filters.
The are able to split and distribute the signal among multiple antennas, which do not have to be aligned with the focal point.
