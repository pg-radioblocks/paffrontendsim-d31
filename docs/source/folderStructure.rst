Folder Structure for Input/Output Data
========================================

This page illustrates the default folder structure expected by PAFFrontendSim.

The simulator can read data from files to use in the simulation and write data to files containing the results it produces.

The setup of the folders is handled by the method PAFFrontendSim.prepareDataFolders.
See its documentation for more information.

The most important detail is that for every simulation, all existing data in the target output folder is overwritten.
**There is a risk of data loss!**

Always make sure that you specify a new name for your output folder (see the documentation for PAFFrontendsim for more information).
Only ever use the default folder for testing and debugging.

The following list structure reflects the hierarchy of the folders. Most of the names chosen here are arbitrary.

simData:
	InputFolder
		defaultInput ``If the input folder is not specified, this default is used.``
			Atmosphere_files ``Files for atmospheric transmittance data. Used by AtmosphereDataClass. View its readFile method for more information. Currently only supports ascii-format with 3 columns separated by spaces (frequency[GHz] transmission[/1] temperature[K]).``
				testAtm.txt
			CST_Files ``Farfield data of the antenna. Currently only supports CST farfield exports (.ffs) with one frequency per file.``
				2.5_GHz.ffs

				2.6_GHz.ffs
			LNA_Files ``Data for the signal chain. Native format (created by SignalChainDataClass) or Touchstone 2 port network files.``
				testLNA0.s2p

				testLNA1.s2p

		myCustomInput ``Custom input folders expect to have the same structure as the defaultInput folder.``

	OutputFolder
		defaultOutput ``If the output folder is not specified, this default is used. Be aware that this contents folder is deleted if a new simulations targets it.``
			Logs ``Log files of the console output. See FileDataClass for more information.``
				global.log

				PAFFrontendSim.log

				PAFFrontendSim.Telescope.log

			PAFFrontendSim.h5 ``ACM output file. See hdf5writer for more information.``

			primary.tclass ``Debugging data for physical optics simulator (UFO). See UFO page for more information.``

			telecopeMirror_0.curr ``Debugging data for physical optics simulator (UFO). See UFO page for more information.``

			telescope.png ``A plot to make data human readable. Many such plots are created. More information can be found in the documentation of the associated method.``

		myCustomOutput ``Custom output folders are created with the same structure as the defaultOutput folder.``

