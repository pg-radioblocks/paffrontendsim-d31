UFO
===================================

The UFO modules are a general purpose tool to perform physical optics simulations.

In the case of the PAF Frontend simulator, UFO is used to calculate the EM signal as it travels.
This travel path goes from the individual antennas, over the dish surfaces of the telescope and onto every point of the sky/spill-over.
Optionally it also includes an optical system of lenses and mirrors in front of the PAF (see Optics-page).
For every reflection, the incident and reflection angles are determined, as well as the electrical current on the respective surface.
This is done using Maxwells equations.
The physical optics simulation is computationally intensive and therefore implemented in C-code (`UFO_calc.c`).
The C-code is highly parallelized through the use of the OpenMP API (https://www.openmp.org/).
This ensures that the simulator takes advantage of the full potentional of the hardware (x86 CPUs).

The C-code is compiled by the module `UFO_CCode.py`.
As of the time of writing, it uses GCC with various optimization flags.
In the future, this module may be altered to allow for an alternative compiler to be used if GCC is not found.
It is also possible that the module is complemented by an alternative implementation for GPUs (using the CUDA API).

Optionally the data for the reflections at each surface can be stored in files (extensions .curr and .tclass).
These files are not used by the PAF Frontend simulator though they provide insight into the results of the UFO modules.
The names of these files indicate which surface the represent.

File structure of mirror .curr:
	A header gives basic information on the surface and simulation parameters.
	This is followed by the simulation data.
	Each line represents one point on the surface.
	A line consists of 20 float values representing the following information:

		- A position vector ``v`` for the surface (xyz),

		- a normal vector ``n`` for the surface (xyz),

		- a real current distribution vector ``Jr`` (xyz),

		- an imaginary current distribution vector ``Ji`` (xyz),

		- a real field vector ``Er`` (xyz),

		- an imaginary field vector ``Ei`` (xyz),

		- the intensity ``I`` (scalar),

		- the area of the surface ``df`` (scalar).

File structure of flat surface .tclass:
	Same as .curr but with only the surface data (8 columns)


When looking at the code, the first module to pay attention to is `UFODataClass.py`, as it represents the data structure.
The UFO modules have been used in various other projects and many of its features are unused by the PAF Frontend simulator.
Keep in mind that not everything in the UFO folder is relevant.
