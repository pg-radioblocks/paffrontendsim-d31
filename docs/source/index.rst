.. PAFFrontendSim documentation master file, created by
   sphinx-quickstart on Tue Dec 19 11:53:43 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to PAFFrontendSim's documentation!
==========================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   modules

.. toctree::
   :maxdepth: 10
   :caption: Additions:

   introduction.rst
   simulationOverview.rst
   folderStructure.rst
   hdf5.rst
   ufo.rst
   beamforming.rst
   optics.rst
   sphinx.rst
   git.rst

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
