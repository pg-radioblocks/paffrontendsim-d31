src.Libs.Simulation.UFO package
===============================

Submodules
----------

src.Libs.Simulation.UFO.OpticsDataClass module
----------------------------------------------

.. automodule:: src.Libs.Simulation.UFO.OpticsDataClass
   :members:
   :undoc-members:
   :show-inheritance:

src.Libs.Simulation.UFO.UFODataClass module
-------------------------------------------

.. automodule:: src.Libs.Simulation.UFO.UFODataClass
   :members:
   :undoc-members:
   :show-inheritance:

src.Libs.Simulation.UFO.UFOGaussCalc module
-------------------------------------------

.. automodule:: src.Libs.Simulation.UFO.UFOGaussCalc
   :members:
   :undoc-members:
   :show-inheritance:

src.Libs.Simulation.UFO.UFOTrace module
---------------------------------------

.. automodule:: src.Libs.Simulation.UFO.UFOTrace
   :members:
   :undoc-members:
   :show-inheritance:

src.Libs.Simulation.UFO.UFO\_CCode module
-----------------------------------------

.. automodule:: src.Libs.Simulation.UFO.UFO_CCode
   :members:
   :undoc-members:
   :show-inheritance:

src.Libs.Simulation.UFO.constants module
----------------------------------------

.. automodule:: src.Libs.Simulation.UFO.constants
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: src.Libs.Simulation.UFO
   :members:
   :undoc-members:
   :show-inheritance:
