Sphinx Documentation
===================================

Sphinx is a tool used to generate documentation for software projects, with a focus on Python.

This page will illustrate how Sphinx is used in this project and what contributors should pay attention to.

For more information on Sphinx, visit https://www.sphinx-doc.org/.

In this project, a script is run automatically on the remote repository after changes have been made to the main branch.
This script parses the code and regenerates the documentation tree.

The main page of the repository contains a link that points to the documentation as generated by Sphinx.
The documentation is made up of HTML-files.
On the left side of the HTML-pages you will find the search function, as well as a tree reflecting the hierarchy of the code, to help you navigate it quickly.


When you write new code, you must adhere to the format expected by Sphinx.
The basic requirement is that your Python code is free of syntax errors.
Additionally, we ask you to add a docstring to the top of every class, function and method in your code.
Within this docstring you should start with one sentence, summarizing what the particular section of the code does.
If the section of code is too complex to be understood with a short summary, then please add another block of text with a more in-depth description.

At the end of the docstring you should write down a list of the input parameters and return values.
Each entry should have a short sentence to illustrate its meaning.

You must be careful to adhere to the syntax that entries must follow.
The list of input parameters must be preceded by the line `Parameters:`.
The return value must be preceded by the line `Returns:`.
Each entry must be indented (4 spaces) and start with the following header:

`<name><1 space>(<data type>):`

After this header, you write your short description.
You can add as many spaces as you want between the header and the description (to vertically align the descriptions).
The return value can only contain one entry (for now. We might be able to change the syntax and allow for multiple entries).
If you return multiple values, they are seen as a tuple by Python and Sphinx.
In this case, your entry could look like this:

`tuple (tuple): okFlag (boolean, if result is valid), result (float, output value of the algorithm)`

Sphinx does include your code line by line through the `[source]` links, but you should keep in mind that this is not how Sphinx is intended to be used.
Your docstring should be enough to give the reader a reasonable understanding of what your code does.

The following docstring is an example.
Of course you can also look at the existing code to get an idea of how docstrings should be written.

.. code-block:: python

	"""
	Takes the number of beans and calculates their total nutritional value.

	The algorithm used here, multiplies the number of beans by 8.9 calories (the energy of a standardized bean).

	Parameters:
		noBeans (int):      The number of beans.

	Returns:
		nutriValue (float): Total nutritional value of all beans.
	"""
